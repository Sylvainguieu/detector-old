try:
    import pyfits as pf
except:
    from astropy.io import fits as pf
import numpy as np

from detector import config
if config.debug: print "fits.py"

from detector import bases,engine
if config.debug:
    reload(bases)
    
from datax import DataX, dataxconcat, isdatax



_class_dictionary_ = None
def _triphdu(name):
    if name[-3:] == "hdu":
        return name[0:-3]
    return name

getClass = engine.getClass_decorator( __name__, ["hdu", "hdulist", "fitslist"], engine.getClass)

#def getClass(classname,primary=False):
#    """ Return the right class given for a given name 
#    primary=True if the required class is a PrimaryHDU derived class 
#    """
#    p = ""
#    if primary:
#        p = "primary"
#        
#    for mod in [__name__, "__all__"]:
#        for pref in ["hdu", "hdulist", "fitslist"]:
#            cl = bases.getClass_in_module(classname+p+pref,mod, primary)
#            if cl is not None: return cl
#        
#        cl = bases.getClass_in_module(classname+p ,mod, primary)
#        if cl is not None: return cl
#    
#    return bases.getClass(classname, primary=primary)



def parseclass(obj, cls):
    obj.__class__ = cls
    if hasattr(obj, "_name") and ((not obj._name) or obj._name == "PRIMARY") and cls._name:
        obj.name = cls._name 
    
def dic2header(d):
    """
    dic2header(d)
    Return a pyfits Header object from a directory 
    """
    return pf.Header( cards=[ pf.Card(k,v) for k,v in d.iteritems()] )




def autoOpen(filename, opener=pf.open):
    """
    object = autoOpen(default_openeur(file_name))
    Automatlycaly assign the right obbject for a given pyfits hdulist 
    """
    fh =  opener(filename)
    
    if len(fh)>=5 and (fh[1].name == "M2" or fh[1].name=="SIGMA2"):
        if fh[1].header["NAXIS"]>2:
            return AllCombined(fh)
        return getClass("combined")(fh)
    
    if len(fh)==4 and fh[1].name == "SIGMA2":
        return Combined2(fh)
    if len(fh)==1 and fh[0].header['NAXIS'] >2:
        return CubeImage(fh)
    if "IMAGING_DATA" in  [hdu.name for hdu in fh]:
        return TableImage(fh)
    return Image(fh)
autoopen = autoOpen





def reform_key(k):
    k = " ".join(k.split("."))
    if " " in k and k[0:13]!="HIERARCH ESO ":
        k = "HIERARCH ESO "+k
    return k
    


  
class _HDU_(object):
    """
    Some additional function added to the normal pf HDUs
    """
    verbose = 0  # verbose level
    _getClass_ = staticmethod(getClass)
    @staticmethod
    def isList():
        return False
    @staticmethod
    def isListList():
        return False
    @staticmethod    
    def isEntity():
        return True
    
    def getKey(self, k):
        """
        Simple wrapped func for future more soffisticate object 
        getKey(k) return header[k]
        """
        #return bases.DataEntity.getKey(self,reform_key(k))
        return super( _HDU_, self).getKey( reform_key(k))
    
    def hasKey(self, k):
        #return bases.DataEntity.hasKey(self,reform_key(k))
        return super( _HDU_, self).hasKey( reform_key(k))

    def newHeader(self, *args, **kwargs):
        return pf.Header()
        return pf.Header(*args, **kwargs)
    def getHeader(self, kw=None, copy=False):
        if copy:
            header =  self.header.copy()
        else:
            header =  self.header
        if kw and len(kw):
            header.add_comment("COMMAND:%s"%(kw))
        return header
    
    def getKeyComment(self,k):
        #return bases.DataEntity.getKeyComment(self,reform_key(k))
        return super( _HDU_, self).getKeyComment(reform_key(k))
    
    def getShape(self):
        N = self.header["NAXIS"]
        return tuple(self.header["NAXIS%d"%(N-i)] for i in range(N) )
    
    def getFilePath(self):
        finfo = self.fileinfo()
        if finfo is None: return ""
        return finfo['file'].name        

def _get_data_axes( data):
    if isdatax(data): return np.asarray(data), data.axes
    return data, None

class PrimaryHDU(pf.PrimaryHDU,_HDU_, bases.DataEntity):
    def __init__(self,data, *args, **kwargs):
        data,axes = _get_data_axes(data)
        axes = kwargs.pop( data, axes)
        _name = self._name
        
        pf.PrimaryHDU.__init__(self, data, *args, **kwargs)
        
        if axes:
            self.axes = axes
        if self.name == "" or self.name=="PRIMARY" and _name:
            self.name = _name
        
class ImageHDU(pf.ImageHDU, _HDU_, bases.DataEntity):
    def __init__(self,data, *args, **kwargs):
        data,axes = _get_data_axes(data)
       
        axes = kwargs.pop(data, axes)
        _name = self._name
        pf.ImageHDU.__init__(self, data, *args, **kwargs)
        if axes:
            self.axes = axes
        self.name = self.name or _name


    
    
class BinTableHDU(pf.BinTableHDU, _HDU_, bases.DataEntity):
    def __init__(self,data, *args, **kwargs):        
        axes = kwargs.pop( "axes", None)
        _name = self._name
        pf.BinTableHDU.__init__(self, data, *args, **kwargs)
        if axes:
            self.axes = axes
        self.name = self.name or _name

class Image2dHDU(ImageHDU,bases.ImageEntity):
    axes = ["y","x"]
class Image2dPrimaryHDU(PrimaryHDU,bases.ImageEntity):
    axes = ["y","x"]


class SignalHDU(Image2dHDU, bases.SignalEntity):
    pass
class SignalPrimaryHDU(PrimaryHDU, bases.SignalEntity):
    pass
class SigmaHDU(Image2dHDU,  bases.SigmaEntity):
    pass
class Sigma2HDU(Image2dHDU, bases.Sigma2Entity):
    pass
class Sigma3HDU(Image2dHDU, bases.Sigma3Entity):
    pass
class Sigma4HDU(Image2dHDU, bases.Sigma4Entity):
    pass
class NumberHDU(Image2dHDU, bases.NumberEntity):
    pass

class CubePrimaryHDU(PrimaryHDU, bases.CubeEntity):
    pass

class CubeHDU(ImageHDU, bases.CubeEntity):
    pass
################################################################################
#
#  Single Images classes derived from pf.HDUList
#
################################################################################    
def list2fits(lst, cls=pf.HDUList, **kwargs):
    out = []
    if hasattr( lst[0], "toPrimaryHDU"):
        out.append( lst[0].toPrimaryHDU())
    else:
        out.append( lst[0].toHDU())
    for hdu in cls[1:]:
        out.append(hdu.toHDU())
    

def tofits_decorator(cls):
    def tmp_list2Fits(lst, **kwargs):
        cls = kwargs.pop("cls", cls)
        return list2fits(lst, cls=cls, **kwargs)
    return tmp_list2Fits



class HDUList(pf.HDUList, bases.Switch):                
    axes       = ["hdu"]  # axes name in the hdu direction
    _getClass_ = staticmethod(getClass)
    
    subclasses = {                   
        pf.PrimaryHDU: PrimaryHDU, 
        pf.ImageHDU:   ImageHDU, 
        #pf.TableHDU:   TableHDU,
        pf.BinTableHDU:BinTableHDU
    }
    @classmethod
    def open(cls, *args, **kwargs):
        return cls(pf.open(*args, **kwargs))
    @staticmethod
    def _subclass_entity(entity, subclasses, index, parseclass=parseclass, getClass=getClass):
        bases.Switch._subclass_entity(entity, subclasses, index, parseclass=parseclass,getClass=getClass)
        
    def __init__(self, lst=None, *args, **kwargs):
        #super(HDUList,self).__init__(lst,*args, **kwargs)
        axis = kwargs.pop("axis", None)
        pf.HDUList.__init__(self, lst,*args, **kwargs)
        self._subclass_list()
        if axis is not None: self.axes = [axis]

    @staticmethod        
    def isList():
        return True
    @staticmethod    
    def isListList():
        return False
    @staticmethod
    def isEntity():
        return False
    
class CombinedHDUList(HDUList, bases.CombinedSwitch):
    subclasses = {
        0:SignalHDU,
        1:Sigma2HDU,
        2:Sigma3HDU,
        3:Sigma4HDU,
        4:NumberHDU } 
    pass

class FitsList(bases.DataList):
    _getClass_ = staticmethod(getClass)
    pass


engine._update_class_dictionary_(__name__)

def __test__():
    global fls
    try:
        fls = bases.CubeList(fls)
        print "Subclassing"
        fls._subclass_all(Combined) 
    except:    
        from detector import fitsopener as opener
        fls = bases.CubeList(opener.open_glob("/Users/guieu/DETDATA/274-2014-PTC-P7100/PIONIER*_0.05*.fits", opener=opener.fits.pf.open))
    print "sigma2error", fls.getSigma2error().shape
    print "signalerror", fls.getSignalerror().shape
if __name__ is "__main__":
    __test__()
            
