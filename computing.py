import numpy as np

def moment(data, moments=2, meandata=None, axis=0):
    """
    moment(data, moment=2, meandata=None)
    return the moment of data on the axis 0 
    """    
    if meandata is None:
        meandata = data.mean(axis=axis)    
    
    diff =  (data-meandata)
    is_scalar = False
    if not hasattr(moments, "__iter__"):
        is_scalar = True
        moments = [moments]
    
    output = []
    for moment in moments:
        if moment == 2:
            diffpower = diff*diff
        elif moment == 3:
            diffpower = diff*diff*diff
        elif moment == 4:
            diffpower = diff*diff*diff*diff
        else:    
            diffpower = (diff)**moment    
        output.append(diffpower.mean(axis=axis))
    if is_scalar: return output[0]
    return output

def variance_error(m2,m4,N):
    """
    variance_error(m2,m4,N)
    return the variance error bar from the 2nd order moment, the 4th and number of points N
    """
    return np.sqrt( (m4 - m2**2*(N-3.)/(N-1.))/N)
    

def __test__():
    data = np.random.random( (5,100,100))
    print len(computeMoment( data, [2,3,4] ))
    print computeMoment( data, 2, axis=(0,1)).shape
if __name__ == "__main__":
    __test__()
