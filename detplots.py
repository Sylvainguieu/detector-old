from detector import baseplot
from detector.baseplot import DetPlot, DetImgPlot
#from detector import base

import numpy as np
import plots 
from plots import * 



class PtcFit(plots.Fit2d):
    def fit(self, signal, m2, signalErr=None, m2err=None, **kwargs):
        super(PtcFit, self).fit(signal, m2, signalErr, m2err, **kwargs)
        result = self.result        
        whereMin = signal.argmin()
        
        minM2    =  m2[whereMin]        
        minM2err =  m2err[whereMin] if hasattr(m2err, "__iter__") else None
        k = 1./result[0]
        
        floornoise = np.sqrt(minM2)*k
        floornoiseerror = 0.0 if minM2err is None else np.sqrt(minM2err)*k
        gain = k 
        floornoiseptc2 = result[1]/k**2
        self.result = {"floornoise":floornoise, 
                       "floornoiseerror": floornoiseerror, 
                       "gain":gain, 
                       "floornoiseptc2":floornoiseptc2, 
                       "coef":result
                   }
    def get_model(self,x):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")                
        return get_polynome_model(x, self.result["coef"])
    def get_text(self):        
        return "gain= {gain:5.3f} e-/ADU,\
$noise_{{fit}}^2$={floornoiseptc2:1.3E}=>{floornoiseptc:1.3E} e-,\
$noise_{{min}}$ = {floornoise:1.3E} e-+-{floornoiseerror:1.3E}".format(floornoiseptc=np.sqrt(self.result["floornoiseptc2"]), **self.result)

class LinearityFit(plots.Fit2d):
    dim = 1
    def get_text(self):
        coeff = self.result
        if self.dim==1:
             return "%5.2f ADU/ms %+5.2f %s"%( coeff[0]/1000. , coeff[1], self.kw["yrange"])
        return "%s , %s"%(coeff, self.kw["yrange"])     


class GainFit(plots.Fit2d):
    dim = 1
    def get_text(self):
        coeff = self.result
        
        return reduce( lambda t,ci: t+"%+4.2E.P^%d"%(ci[0],ci[1]), zip(coeff,range(len(coeff))[::-1]), "")
        
ptcfit       = PtcFit(xrange=(0,6000))
linearityfit = LinearityFit(yrange=(0,4000))
gainfit      = GainFit(dim=3)



class Signal(DetImgPlot):
    ckw_axes   = dict(xlabel="Signal [ADU]")
    def setImg(self, **kwargs):
        self.img = self.detector.getSignal(**kwargs)
baseplot.Combined._addPlot("signal", Signal)


class M2(DetImgPlot):
    ckw_axes   = dict(xlabel="Variance [ADU^2]")
    def setImg(self, **kwargs):
        self.img = self.detector.getSigma2(**kwargs)
baseplot.Combined._addPlot("m2", M2)
baseplot.Combined._addPlot("sigma2", M2)



class Std(DetImgPlot):
    def setImg(self, **kwargs):
        self.img = self.detector.getSigma(**kwargs)
baseplot.Combined._addPlot("std", Std)
baseplot.Combined._addPlot("sigma", Std)



class Gain(DetImgPlot):
    ckw_axes   = dict(xlabel="Gain Sys [e-/ADU]")
    def setImg(self, **kwargs):        
        self.img = self.detector.getGain(**kwargs)
baseplot.Ptc._addPlot("gain", Gain)

class Noise2(DetImgPlot):
    """ square of noise as the result of PTC fit [ADU^2 rms]"""
    ckw_axes   = dict(xlabel="Noise^2 [ADU^2 rms]")
    def setImg(self, **kwargs):        
        self.img = self.detector.getNoise2(**kwargs)
baseplot.Ptc._addPlot("noise2", Noise2)

class Noise(DetImgPlot):
    ckw_axes   = dict(xlabel="Noise [ADU rms]")
    def setImg(self, **kwargs):        
        self.img = self.detector.getNoise(**kwargs)
baseplot.Ptc._addPlot("noisemin", Noise)

class NoiseMin(DetImgPlot):
    ckw_axes   = dict(xlabel="Noise [ADU rms]")
    def setImg(self, **kwargs):        
        self.img = self.detector.getMinDitSigma(**kwargs)
baseplot.Images._addPlot("noisemin", NoiseMin)



class eNoiseMin(DetImgPlot):
    """ The min dit std image in [e-]"""
    ckw_axes   = dict(xlabel="Noise [e- rms]")
    def setImg(self, **kwargs):        
        self.img = self.detector.getENoise(**kwargs)
baseplot.Ptc._addPlot("enoisemin"   , eNoiseMin)



class Flux(DetImgPlot):
    """ Flux image in ADU """
    ckw_axes   = dict(xlabel="Flux [ADU/{unit}]")
    def setImg(self, **kwargs):        
        time_unit = kwargs.pop("time_unit", "ms")
        scale = {"ms":1000., "s":1.}[time_unit]
        self.kw_axes["xlabel"] = self.ckw_axes["xlabel"].format(unit=time_unit)
        self.img = self.detector.getFlux(**kwargs)/scale
baseplot.Linearity._addPlot("flux",Flux)


class eFlux(DetImgPlot):
    """ Flux image in e-"""
    ckw_axes   = dict(xlabel="Flux [e-/{unit}]")
    def setImg(self, **kwargs):
        time_unit = kwargs.pop("time_unit", "ms")
        scale = {"ms":1000., "s":1.}[time_unit]
        self.kw_axes["xlabel"] = self.ckw_axes["xlabel"].format(unit=time_unit)
        self.img = self.detector.getEFlux(**kwargs)/scale
baseplot.PtcLinearity._addPlot("eflux",eFlux)


class Bias(DetImgPlot):
    ckw_axes   = dict(xlabel="Bias [ADU]")
    def setImg(self, **kwargs):        
        self.img = self.detector.getBias(**kwargs)
baseplot.Linearity._addPlot("bias",Bias)


class Ptc(DetPlot):    
    ckw_axes   = dict(ylabel="$\sigma^2$ [$ADU^2$]", xlabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                      labelspacing=0.1
                  )
    ckw_fit    = dict(fitter=ptcfit)
    dataKeys = {   "x":("getMeanSignal",),        "y":("getMeanSigma2",),
                "xerr":("getMeanSignalerror",),"yerr":("getMeanSigma2error",)}
ptc = Ptc.newWithData
baseplot.Images._addPlot("ptc", Ptc)

class Linearity(DetPlot):
    ckw_axes   = dict(xlabel="DIT [s]", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                    labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=linearityfit)
    dataKeys = {"x":("getDit","nokw"),"y":("getMeanSignal",),"xerr":(None,),
                "yerr":("getMeanSignalerror",)}
linearity = Linearity.newWithData 
baseplot.Images._addPlot("linearity", Linearity)

class M2Dit(DetPlot):
    """ Variance function to dit """
    ckw_axes   = dict(xlabel="DIT [s]", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size":8}, frameon=False, 
                       labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=ptcfit)
    dataKeys = {"x":("getDit","nokw"),"y":("getMeanSigma2",),"xerr":(None,),
                "yerr":("getMeanSigma2error",)}
m2dit = M2Dit.newWithData 
baseplot.Images._addPlot("m2dit",M2Dit)
baseplot.Images._addPlot("sigma2dit",M2Dit)


class GainMPolar(DetPlot):
    """ Multiplicative Gain [no units] function to detector polarisation [mv]"""
    ckw_axes   = dict(xlabel="Polar [mv]", ylabel="Gain")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                    labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=gainfit)
    dataKeys = {"x":("getPolar","nokw"),"y":("getGainM",),"xerr":(None,),
                "yerr":(None,)}
gainmpolar = GainMPolar.newWithData 
baseplot.LinearityList._addPlot("gainmpolar", GainMPolar)

class GainPolar(DetPlot):
    """ Gain system [e-/ADU] function to detector polarisation [mv]"""
    ckw_axes   = dict(xlabel="Polar [mv]", ylabel="Gain System [e-/ADU]")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                    labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=gainfit)
    dataKeys = {"x":("getPolar","nokw"),"y":("getGain",),"xerr":(None,),
                "yerr":(None,)}
gainpolar = GainPolar.newWithData 
baseplot.PtcList._addPlot("gainpolar", GainPolar)

class NoiseMinPolar(DetPlot):
    """ Min dit noise (std) [ADU]  function to detector polarisation [mV]"""
    ckw_axes   = dict(xlabel="Polar [mv]", ylabel="Noise [ADU rms]")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                    labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=gainfit)
    dataKeys = {"x":("getPolarSet","nokw"),"y":("getMinDitSigma",),"xerr":(None,),
                "yerr":(None,)}
noiseminpolar = NoiseMinPolar.newWithData 
baseplot.Carac._addPlot("noiseminpolar", NoiseMinPolar)


class NoisePolar(DetPlot):
    """ Min dit noise (std) in ADU  function to polarisation """
    ckw_axes   = dict(xlabel="Polar [mv]", ylabel="Noise [ADU rms]")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                    labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=gainfit)
    dataKeys = {"x":("getPolar","nokw"),"y":("getNoise",),"xerr":(None,),
                "yerr":(None,)}
noisepolar = NoisePolar.newWithData 
baseplot.PtcList._addPlot("noisepolar", NoisePolar)


class eNoisePolar(DetPlot):
    """ Min dit noise (std) in e- function to polarisation """
    ckw_axes   = dict(xlabel="Polar [mv]", ylabel="Noise [e- rms]")
    ckw_legend = dict(loc="upper left", prop={"size":8}, frameon=False, 
                    labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=gainfit)
    dataKeys = {"x":("getPolar","nokw"),"y":("getEnoise",),"xerr":(None,),
                "yerr":(None,)}
noisepolar = NoisePolar.newWithData 
baseplot.PtcList._addPlot("enoisepolar", eNoisePolar)



class M2Temp(DetPlot):
    ckw_axes   = dict(xlabel="Temp [k]", ylabel="$\sigma^2$ [$ADU^2$]")
    ckw_legend = dict(loc="lower left", prop={"size":8}, frameon=False, 
                       labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=ptcfit)
    dataKeys = {"x":("getTemp","nokw"),"y":("getMeanSigma2",),"xerr":(None,),
                "yerr":("getMeanSigma2error",)}
m2temp = M2Temp.newWithData 
baseplot.Images._addPlot("m2temp",M2Temp)
baseplot.Images._addPlot("sigma2temp",M2Temp)

class SignalTemp(DetPlot):
    ckw_axes   = dict(xlabel="Temp [k]", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size":8}, frameon=False, 
                       labelspacing=0.1
                  )
    ckw_fit      = dict(fitter=ptcfit)
    dataKeys = {"x":("getTemp","nokw"),"y":("getMeanSignal",),"xerr":(None,),
                "yerr":("getMeanSignalerror",)}    
signaltemp = SignalTemp.newWithData 
baseplot.Images._addPlot("signaltemp",SignalTemp)

class Stability(DetPlot):
    ckw_axes   = dict(xlabel="MJD", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size":8}, frameon=False, 
                      labelspacing=0.1
    )
    ckw_fit      = dict(fitter=linearityfit)
    dataKeys = {"y":("getMeanSignal",), "yerr":("getMeanSignalerror",) }
    
    def setX(self, **kwargs):
        dets = self.detector
        mjd    = dets.getKey("MJD-OBS")
        minmjd = int(np.min(mjd))
        self.x = mjd-minmjd
        self.kw_axes["xlabel"] = "MJD -%d"%minmjd        

stability = Stability.newWithData 
baseplot.Images._addPlot("stability",Stability)

class Stability2(DetPlot):
    ckw_axes   = dict(xlabel="MJD", ylabel="<S> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size":8}, frameon=False, 
                      labelspacing=0.1
    )
    ckw_fit      = dict(fitter=linearityfit)
    dataKeys = {}
        
    def setX(self,**kwargs):
        det = self.detector
        mjd    = det.getMjd(**kwargs).transform( [("time","file","time")], True)
        minmjd = int(np.min(mjd))
        self.x = mjd-minmjd
        self.kw_axes["MJD"] = "MJD -%d"%minmjd
    def setY(self,**kwargs):        
        det = self.detector
        normalize = kwargs.pop("normalize", True)
        darksection = kwargs.pop("darksection", None)
        signal = det[0].getData(**kwargs).transform([("time","file","time"),("pix",None)],True)
        if normalize:
            signal = signal - signal.mean(axis="time")        
        if darksection:
            kwargs["section"] = darksection
            darks = det[0].getData(**kwargs).transform([("time","file","time"),("pix",None)],True).mean( axis="pix")
            signal = signal - darks            
        self.y = signal
    
    def setYerr(self,**kwargs):
        det = self.detector
        rms = det[1].getData(**kwargs).transform([("time","file","time"),("pix",None)],True)
        self.yerr = rms
baseplot.CubeImage._addPlot("stability", Stability2)
baseplot.Images._addPlot("stability2", Stability2)

class M2Stability(Stability):
    dataKeys = {"y":("getMeanSigma2",), "yerr":("getMeanSigma2error",) }
m2stability = M2Stability.newWithData 
baseplot.Images._addPlot("m2stability",M2Stability)
baseplot.Images._addPlot("sigma2stability",M2Stability)

class M2Stability2(Stability2):
    def setY(self,det,**kwargs):                
        rms = det[1].getData(**kwargs).transform([("time","file","time"),("pix",None)],True)
        self.y = rms
    def setYerr(self, *args, **kwargs):
        self.yerr = None
m2stability2 = M2Stability2.newWithData 
baseplot.CubeImage._addPlot("m2stability",M2Stability2)
baseplot.Images._addPlot("m2stability",M2Stability2)



class Fft(DetPlot):
    ckw_axes   = dict(xlabel="Frequency [Hz]", ylabel="FFT")
    def setData(self, detector=None, **kwargs):
        f, fft = detector.getFFT(**kwargs)
        self.x = f
        self.y = fft
fft = Fft.newWithData
baseplot.CubeImage._addPlot("fft",  Fft)


class Psd(DetPlot):
    ckw_axes   = dict(xlabel="Frequency [Hz]", ylabel="PSD [ADU/sqrt(Hz)]")
    def setData(self, detector=None, **kwargs):
        if detector:
            self.detector = detector
        
        f, psd = self.detector.getPSD(**kwargs)
        self.x = f
        self.y = psd
        self.xerr = None
        self.yerr = None
psd = Psd.newWithData
baseplot.CubeImage._addPlot("psd",  Psd)


class Jitter(DetPlot):
    def setData(self, cube_or_jitter, direction="x", space=False,  clipmin=None, clipmax=None, indexes=None, cut=True, fmin=None, fmax=None, **kwargs):
        if not isinstance( cube_or_jitter, det.JitterImage):
            j = cube_or_jitter.getJitter( clipmin=clipmin, clipmax=clipmax, indexes=indexes, **kwargs)
        else:
            j = cube_or_jitter
            
        
        if space is "fft":
            ky , kx = {"x": j.getXfft, "y": j.getYfft }, j.getFreq
            self.axessetup.set(xlabel="F [Hz]", ylabel="%s jitter fft"%direction)
            kw = dict(cut=cut, fmin=fmin, fmax=fmax)
        elif space is "psd":
            ky , kx = {"x": j.getXpsd, "y": j.getYpsd }, j.getFreq
            self.axessetup.set(xlabel="F [Hz]", ylabel="%s psd [pixel/sqrt(Hz)]"%direction)
            kw = dict(cut=cut, fmin=fmin, fmax=fmax)
        elif space=="direct" or space is False:
            ky , kx = {"x": j.getX, "y": j.getY }, j.getScanNum
            self.axessetup.set(xlabel="Frame #", ylabel="%s jitter [pixel]"%direction)
            kw = {}    
        else:
            raise KeyError("space should be 'direct', 'fft' or 'psd'")
        
        self.x = kx(**kw)
        self.y = ky[direction](**kw)
jitter = Jitter.newWithData   
baseplot._Jitter_._addPlot( "jitter", Jitter)
baseplot.CubeImage._addPlot( "jitter", Jitter)

class _plotCut(DetPlot):
    ymethod = "getSignal"
    def setData(self, detector=None, direction="x", **kwargs):
        det = detector
        if det:
            self.detector = det        
        y =  getattr( det , self.ymethod)(**kwargs).apply(file_reduce=kwargs.pop("file_reduce", np.mean)  ).flatten()
        if direction == "x":
            coord  = det.getX(**kwargs).flatten()
        elif direction == "y":
            coord  = det.getY(**kwargs).flatten()
        else:
            raise KeyError("direction should be \"x\" or \"y\", got %s"%direction)
        
        self.kw_axes["xlabel"] = direction
        self.x = coord
        self.y = y
        
class SignalCut(_plotCut):
    ckw_axes = {"ylabel":"Signal [ADU]"}
    ymethod = "getSignal"    
baseplot.Combined._addPlot("signalcut", SignalCut)

class M2Cut(_plotCut):
    ckw_axes = {"ylabel":"$\sigma^2$ [$ADU^2$]"}
    ymethod = "getSigma2"    
baseplot.Combined._addPlot("m2cut", M2Cut)


class GainCut(_plotCut):
    ckw_axes = {"ylabel":"Gain [$e^-/ADU$]"}
    ymethod = "getGain"    
baseplot.Ptc._addPlot("gaincut", GainCut)



