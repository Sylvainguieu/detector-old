import opener
from opener import *
from detector import fits 

default_openeur = fits.pf.open
autoOpen = fits.autoOpen


class ImgListOpeneur(opener.Opener):
    path = "*.fits"
    opener = staticmethod(autoOpen)
    def biases( self, *args, **kwargs):        
        return self.open( kwargs.pop("path", "*BIAS*.fits"), *args, **kwargs)
    def flats( self, directory="./", **kwargs):
        return self.open( kwargs.pop("path", "*FLAT*.fits"), *args, **kwargs)
    def darks( self, directory="./", **kwargs):
        return self.open( kwargs.pop("path", "*DARK*.fits"), *args, **kwargs)
     

    
