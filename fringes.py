from detector import fits 
import numpy as np
from det.mapping import *

ORIENTATION = 0

class FringesDataHDU(fits.ImagingDataHDU):
    mapping = mapABCD
    axes = ["output"]
    if ORIENTATION:
        winaxes = ["time", "sampix", "disp", "spacial"]
    else:
        winaxes = ["time", "sampix", "spacial", "disp"]
    
    def __init__(self, *args, **kwargs):
        self.mapping = kwargs.pop("mapping", self.mapping)
        return super(FringesDataHDU, self).__init__(*args, **kwargs)
    
    def getWinsData(self, **kwargs):        
        tel   = kwargs.pop("tel" , None)
        base  = kwargs.pop("base", None)
        wins  = kwargs.pop("wins", None)
                
        mp = self.mapping        
        if tel is not None:
            if mp is None:
                raise Exception("tel kw: No mapping")
            mp = mp.withTel(tel)
        if base is not None:
            if mp is None:
                raise Exception("base kw: No mapping")
            mp = mp.withBase(base)
        
        if wins is not None:
             if tel is not None or base is not None:
                  raise Exception("tel and base are not compatible with the wins keyword")
        else:
            if mp is None:
                wins = range(self.header["NREGION"])
            else:
                wins = [m.win for m in mp]        
        return fits.ImagingDataHDU.getWinsData(self, wins=wins, **kwargs)
    
class Fringes(fits.TableImage):    
    _data_hdu = "IMAGING_DATA"
    _primary_hdu = 0    
    axes = []  # all axis are actualy axes of ImagingData
    subclasses = {
        0:fits.PrimaryHDU, 
        fits.pf.TableHDU    :fits.TableHDU,
        fits.pf.BinTableHDU :fits.BinTableHDU, 
        _data_hdu           :FringesDataHDU
    }

fits._class_dictionary_["fringes"]   = Fringes
fits._class_dictionary_["fringesdata"] = FringesDataHDU
    
