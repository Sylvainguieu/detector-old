from detector import bases
from detector.bases import ImageEntity, Switch
from detector import engine


class FluxEntity(ImageEntity):
    """ The Flux image derived from a linear fit """
    _name = "FLUX"
FluxEntity._add_data_capability("flux",bases.getData, bases.getHeaderCopy,
                                clsname="flux", add_scalar=True)

class DarkEntity(ImageEntity):
    """ Dark image has computed by a linear fit """
    _name = "DARK"
DarkEntity._add_data_capability("dark",bases.getData, bases.getHeaderCopy,
                                clsname="dark", add_scalar=True)

class LinNumberEntity(ImageEntity):
    """ Image of the number of point used to compute the the linear fit """
    _name = "LINNUMBER"
LinNumberEntity._add_data_capability("number",bases.getData, bases.getHeaderCopy,
                                     clsname="linnumber", add_scalar=True)



####################################
# Must be at the end
engine._update_class_dictionary_(__name__)
