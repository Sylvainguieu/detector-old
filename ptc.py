import numpy as np

from detector import config
if config.debug: print "ptc.py"

from detector import bases, engine, linearfit, fits, baseplot
if config.debug:
    reload(fits) #fits reload bases bases reload engine
    reload(linearfit)    

class GainEntity(bases.ImageEntity):
    """ Image of system gain computed by ptc fit in [e-/ADU] """
    _name = "GAIN"
    def getPolar(self, **kwargs):
        return self.getKey("DET POLAR",**kwargs)
GainEntity._add_data_capability("gain",bases.getData, bases.getHeaderCopy,
                                clsname="gain", add_scalar=True)

class GainPrimaryHDU(fits.getClass("image2d",True),GainEntity):
    _name = "GAIN"
    
class GainHDU(fits.getClass("image2d"),GainEntity):
    _name = "GAIN"

class MGainHDU(fits.getClass("image2d")):
    """ """
    _name = "MGAIN"
MGainHDU._add_data_capability("mgain",bases.getData, bases.getHeaderCopy,
                                clsname="mgain", add_scalar=True)
class Noise2HDU(fits.getClass("image2d")):
    _name = "NOISE2"
    """ Image of the square of Noise computed from the PTC (can be negatif) [ADU] """
Noise2HDU._add_data_capability("noise2",bases.getData, bases.getHeaderCopy,
                                clsname="noise2", add_scalar=True)
class NoiseHDU(fits.getClass("image2d")):
    _name = "NOISE"
    """ Image of the detector noise in ADU, e.i. the standart deviation of a cube 
    it is taken from the cube/image with the minimum DIT and/or minimum flux in a sery 
    of exposure.
    """
NoiseHDU._add_data_capability("noise",bases.getData, bases.getHeaderCopy,
                                clsname="noise", add_scalar=True)

class ENoiseHDU(fits.getClass("image2d")):
    _name = "ENOISE"
    """
    Image of the detector Noise in e-, e.i. the standart deviation of a cube 
    it is taken from the cube/image with the minimum DIT and/or minimum flux in a sery 
    of exposure.
    """
ENoiseHDU._add_data_capability("noise",bases.getData, bases.getHeaderCopy,
                                clsname="enoise", add_scalar=True)


class PTCNumberHDU(fits.getClass("image2d")):
    """ Image of the number of point used to compute the PTC """
    _name = "PTCNUMBER"
    
PTCNumberHDU._add_data_capability("number",bases.getData, bases.getHeaderCopy,
                                clsname="ptcnumber", add_scalar=True)



class FluxEntity(bases.ImageEntity):
    _name = "FLUX"
    """
    Image of the incoming flux in ADU/s calculated from a linear fit of 
    signal = flux*dit + bias 
    """
    def getPolar(self, **kwargs):
        return self.getKey("DET POLAR",**kwargs)
    def getIllumination(self, **kwargs):
        return self.getKey("ILLUM",**kwargs)    

FluxEntity._add_data_capability("flux",bases.getData, bases.getHeaderCopy,
                             clsname="flux", add_scalar=True)

class FluxPrimaryHDU(fits.getClass("image2d",True),FluxEntity):
    _name = "FLUX"
    
class FluxHDU(fits.getClass("image2d"),FluxEntity):
    _name = "FLUX"

class EFluxHDU(fits.getClass("image2d")):
    _name = "EFLUX"
    """
    Image of the incoming flux in e-/s calculated from a linear fit of 
    signal = flux*dit + bias 
    """
EFluxHDU._add_data_capability("eflux",bases.getData, bases.getHeaderCopy,
                             clsname="eflux", add_scalar=True)


class BiasHDU(fits.getClass("image2d")):
    _name = "BIAS"
    """
    Image of the Bias  in ADU  calculated from a linear fit of 
    signal = flux*dit + bias 
    """
BiasHDU._add_data_capability("bias",bases.getData, bases.getHeaderCopy,
                             clsname="bias", add_scalar=True)

class EBiasHDU(fits.getClass("image2d")):
    _name = "EBIAS"
    """
    Image of the Bias  in ADU  calculated from a linear fit of 
    signal = flux*dit + bias 
    """
EBiasHDU._add_data_capability("ebias",bases.getData, bases.getHeaderCopy,
                             clsname="ebias", add_scalar=True)

class GainmHDU(fits.getClass("image2d")):
    _name = "GAINM"
    """
    Image of the multiplicative Gain calculated from the fraction of
    flux for different polars,  flux is obtained by the fit of 
      signal = flux*dit + bias 
    """
GainmHDU._add_data_capability("gainm",bases.getData, bases.getHeaderCopy,
                              clsname="gainm", add_scalar=True)


class LinNumberHDU(fits.getClass("image2d")):
    """ Image of the number of point used to compute the PTC """
    _name = "PTCNUMBER"
    
LinNumberHDU._add_data_capability("number",bases.getData, bases.getHeaderCopy,
                                  clsname="linnumber", add_scalar=True)

class PtcChi2HDU(fits.getClass("image2d")):
    """ Chi2 map of the ptc fit """
    _name = "PTCCHI2"
PtcChi2HDU._add_data_capability("ptcchi2",bases.getData, bases.getHeaderCopy,
                                clsname="ptcchi2", add_scalar=True)
    
class LinChi2HDU(fits.getClass("image2d")):
    """ Chi2 map of the linear flux fit"""
    _name = "LINCHI2"
LinChi2HDU._add_data_capability("linchi2",bases.getData, bases.getHeaderCopy,
                                clsname="linchi2", add_scalar=True)


    
class KeyPtcCapabilities(bases._KeyImageSwitchCapabilities_):
    pass

class DataPtcCapabilities(bases._DataImageSwitchCapabilities_):
    def getNumber(self,**kwargs):
        return self.getPtcnumber(**kwargs)
    def getNumberHeader(self, kw):
        return self.getPtcnumberHeader(kw)
    def getEnoise(self, **kwargs):
         return self.getNoise(**kwargs)*self.getGain(**kwargs)
    def getEnoiseHeader(self, kw):        
        h = self.getNoiseHeader(kw)
        h["unit"] = "e-"
        return h
    
class KeyLinearityCapabilities(bases._KeyImageSwitchCapabilities_):
    pass

class DataLinearityCapabilities(bases._DataImageSwitchCapabilities_):
    def getNumber(self,**kwargs):
        return self.getLinnumber(**kwargs)
    def getNumberHeader(self, kw):
        return self.getLinnumberHeader(kw)


   
class KeyPtcDataListCapabilities(bases._KeyImageDataListCapabilities_):
    pass

class DataPtcDataListCapabilities(bases._DataImageDataListCapabilities_):
    pass

class KeyLinearityDataListCapabilities(bases._KeyImageDataListCapabilities_):
    pass

class DataLinearityDataListCapabilities(bases._DataImageDataListCapabilities_):
    def getGainm(self, zero_polar=np.min, **kwargs):
        """ Return the multiplicative gain 
            zero_polar is the reference polar. by default take the minimun
        """
        polars = self.getPolar()

        zeros = self.withPolar(zero_polar)
        if not len(zeros):
            raise ValueError("Cannot find data for polar %s"%zero_polar)        
        return self.getFlux(**kwargs)/zeros[0].getFlux(**kwargs)
    def getGainmHeader(self,kw):
        return self.getFluxHeader(kw)




for k in ["gain","noise2","noise", "ptcnumber", "ptcchi2"]:
    DataPtcCapabilities._add_data_capability(k  , on=k   , clsname=k  ,add_scalar=True)    
    DataPtcDataListCapabilities._add_data_capability(k , clsname=k  ,add_scalar=True)
    
DataPtcDataListCapabilities._add_data_capability("number", clsname="ptcnumber"  ,add_scalar=True)


DataPtcCapabilities._add_data_capability("number" , inplace=True, clsname="ptcnumber", add_scalar=True)


DataPtcDataListCapabilities._add_data_capability("enoise", clsname="enoise"  ,add_scalar=True)
DataPtcCapabilities._add_data_capability("enoise" , inplace=True, clsname="enoise", add_scalar=True)

for k in ["flux","bias", "linnumber", "linchi2"]:
    DataLinearityCapabilities._add_data_capability(k  , on=k   , clsname=k  ,add_scalar=True)    
    DataLinearityDataListCapabilities._add_data_capability(k , clsname=k  ,add_scalar=True)
DataLinearityDataListCapabilities._add_data_capability("number", clsname="linnumber"  ,add_scalar=True)
DataLinearityDataListCapabilities._add_data_capability("gainm",  clsname="gainm", inplace=True, add_scalar=True)

DataLinearityCapabilities._add_data_capability("number", inplace=True   , clsname="linnumber", add_scalar=True)  
    
    

for k in ["polar"]:
    KeyPtcCapabilities._add_key_capability(k, on="gain" )
    KeyLinearityCapabilities._add_key_capability(k, on="flux" )    
    KeyPtcDataListCapabilities._add_key_capability(k)
    KeyLinearityDataListCapabilities._add_key_capability(k)
    
for k in ["illumination"]:
    KeyLinearityCapabilities._add_key_capability(k, on="flux" )    
    KeyLinearityDataListCapabilities._add_key_capability(k)

    
    
for k in ["dit","config","filepath"]:
    KeyPtcCapabilities._add_key_capability(k, on="keywords")
    KeyLinearityCapabilities._add_key_capability(k, on="keywords")    
KeyPtcCapabilities._add_key_capability("illumination", on="keywords")


class PtcHDUList(fits.getClass("hdulist"), KeyPtcCapabilities,DataPtcCapabilities,baseplot.Ptc):
    subclasses = {
        "GAIN"   :"gainprimary",
        "PRIMARY":"gainprimary",
        "NOISE2" :"noise2",
        "NOISE"  :"noise",
        "PTCNUMBER" :"ptcnumber",
        "PTCCHI2":"ptcchi2",
        "KEYWORDS":"keystable"
    }

class LinearityHDUList(fits.getClass("hdulist"),KeyLinearityCapabilities,DataLinearityCapabilities,  baseplot.Linearity):    
    subclasses = {
        "FLUX":"fluxprimary",
        "BIAS":"bias",
        "LINCHI2":"linchi2",
        "KEYWORDS":"keystable"
    }

class PtcDataList(fits.getClass("fitslist"), KeyPtcDataListCapabilities,DataPtcDataListCapabilities,baseplot.PtcList):
    pass

class LinearityDataList(fits.getClass("fitslist"), KeyLinearityDataListCapabilities,DataLinearityDataListCapabilities, baseplot.LinearityList):
    pass




class PtcLinearityHDUList(LinearityHDUList,PtcHDUList, baseplot.PtcLinearity):
     subclasses = {
        "GAIN"   :"gainprimary",
        "PRIMARY":"gainprimary",
        "NOISE2" :"noise2",
        "NOISE"  :"noise",
        "NUMBER" :"ptcnumber",
        "PTCCHI2":"ptcchi2",
        "LINCHI2":"linchi2",
        "FLUX":"flux",
        "BIAS":"bias",
        "KEYWORDS":"keystable"        
     }
     def getNumber(self,**kwargs):
         return self.getPtcnumber(**kwargs)     
     def getEflux(self, **kwargs):
         return self.getFlux(**kwargs)*self.getGain(**kwargs)
     def getEfluxHeader(self,kw):
         h = self.getFluxHeader(kw)
         h["unit"] = "e-"
         return h
     def getEbias(self, **kwargs):
         return self.getBias(**kwargs)*self.getGain(**kwargs)
     def getEbiasHeader(self, kw):        
         h = self.getBiasHeader(kw)
         h["unit"] = "e-"
         return h

class PtcLinearityDataList(PtcDataList,LinearityDataList, baseplot.PtcLinearityList):
    pass

for k in ["eflux","ebias"]:
    PtcLinearityHDUList._add_data_capability(k, inplace=True, clsname=k  ,add_scalar=True)
    PtcLinearityDataList._add_data_capability(k , clsname=k  ,add_scalar=True)
    
if config.debug:
    try:
        ptc_buffer
    except:
        ptc_buffer      = []
else:
    ptc_buffer      = []

ptc_buffer_size = 50

class _PtcCapabilities_(linearfit._LinearFitCapabilities_):
    def getMinDitSigma(self, **kwargs):
        return self.withDit(np.min).getSigma(**kwargs)
    def MinDitSigma(self, **kwargs):
        Sigma = self.withDit(np.min)[0].Sigma(**kwargs)
        Sigma.name = "NOISE"
        return Sigma
    def _hash_ptc(self, kwargs):
        return hash( ((kwargs.get("signalrange",None), kwargs.get("sigma2range",None),
                       kwargs.get("chi2_signalrange",None), kwargs.get("chi2_sigma2range",None)
                   ),
                      tuple( self.getFilePath())) )
                     
        
    def _look_in_buffer_ptc(self, **kwargs):
        h = self._hash_ptc(kwargs)
        buff = dict(ptc_buffer)
        return h, buff.get( h, None)
    def _add_to_buffer_ptc(self, h, ptc):
        global ptc_buffer, ptc_buffer_size
        if len(ptc_buffer)>ptc_buffer_size:
            ptc_buffer.pop(0)
        ptc_buffer.append( (h, ptc) )
    
    def getPtc(self, signalrange=None, sigma2range=None,
               chi2_signalrange=None, chi2_sigma2range=None,
               force=False, nokeytable=False):
        """
        Return a fits PTC HDUList containing result of photon transfer curve (ptc) fit : 
        sigma2 = signal * alpha + beta 
        
        - Gain system  (e-/ADU from slope of ptc fit:  1/alpha)
        - Noise2       (ADU^2 square of noise form the intercept value of ptc fit: beta*gain^2)
        - Noise        (ADU This is the rms of the minimum dit image: the BIAS)
        - PTCNumber    (a map of the Number of points used for the fit)
        - PTCCHI2      (a chi2 map, result of the ptc fit)
        
        KEYWORDS: 
           signalrange : a tuple of (min,max) value for signal to compute the fit
           sigma2range : a tuple of (min,max) value for sigma2 to compute the fit 

           chi2_signalrange : a tuple of (min,max) value for signal to compute chi2
           chi2_sigma2range : a tuple of (min,max) value for sigma2 to compute chi2 
           if chi2_signalrange or chi2_sigma2range, default is signalrange and
           sigma2range respectively

        """
        ptc = None
        
        h, ptc = self._look_in_buffer_ptc( signalrange=signalrange, sigma2range=sigma2range,
                                           chi2_signalrange=chi2_signalrange,
                                           chi2_sigma2range=chi2_sigma2range
                                       )
        if force or (ptc is None):
            ptc = self.compute_ptc(signalrange=signalrange, sigma2range=sigma2range,
                                    chi2_signalrange=chi2_signalrange,
                                   chi2_sigma2range=chi2_sigma2range
            )           
            self._add_to_buffer_ptc(h, ptc)
        
        if nokeytable:
             self._getClass_("ptchdulist")(ptc).pop([f.name for f in ptc].index("KEYWORDS"))
        return ptc
    
    def compute_ptc(self, signalrange=None, sigma2range=None, chi2_signalrange=None, chi2_sigma2range=None, **kwargs):
        xrange = signalrange
        yrange = sigma2range
        chi2_xrange = chi2_signalrange
        chi2_yrange = chi2_sigma2range
        
        medbox = kwargs.pop("medbox", None)

        polar = 0
        try:
            polars = self.getPolar()
            if len(set(polars))>1:
                self.say("Found more than one detector polar in this list: %s"%(set(polars)), vtype=config.WARNING)
            polar = max(polars)
        except:
            self.say("Cannot determine detector polar", vtype=config.WARNING)
        
        self.say("Computing full frame PTC with signalrange=%s, sigma2range=%s for polar %d"%(signalrange, sigma2range, polar), 1)
        
        coeff = self.linearfit( "getSignal", "getSigma2", variance="getSigma2variance", compute_chi2=True,chi2_xrange=chi2_xrange, chi2_yrange=chi2_yrange, **kwargs)
        noisemin = self.MinDitSigma(**kwargs)
        k     = 1./coeff[0]
        eta2  = (coeff[1]*k**2)
        N = coeff[2]
        chi2 = coeff[3]
        
        if xrange is None:
            xrange = (None, None)
        if yrange is None:
            yrange = (None, None)  

        
            
        k, eta2, N, chi2 = self._getClass_("gain",True)(k), self._getClass_("noise2")(eta2), self._getClass_("ptcnumber")(N), self._getClass_("ptcchi2")(chi2)

        
        
        k.setKey("XFITMIN", xrange[0] or "None" )
        k.setKey("XFITMAX", xrange[1] or "None" ) 
        k.setKey("YFITMIN", yrange[0] or "None" )
        k.setKey("YFITMAX", yrange[1] or "None" )
        k.setKey("HIERARCH ESO DET POLAR", polar)
        
        if hasattr( self, "make_keys_table"):
            return self._getClass_("ptc")( [k, eta2, noisemin, N, chi2,self.make_keys_table()])
        else:
            return self._getClass_("ptc")( [k, eta2, noisemin, N, chi2])

if config.debug:
    try:
        lin_buffer
    except:
        lin_buffer      = []
else:
    lin_buffer      = []
lin_buffer_size = 50
        
class _LinearityCapabilities_(linearfit._LinearFitCapabilities_):
    def _hash_lin(self, kwargs):
        return hash( ((kwargs.get("signalrange",None), kwargs.get("ditrange",None), 
                       kwargs.get("chi2_signalrange",None), kwargs.get("chi2_ditrange",None)
                   ),
                      
                      tuple( self.getFilePath())) )
    
    def _look_in_buffer_lin(self, **kwargs):
        h = self._hash_lin(kwargs)
        buff = dict(lin_buffer)
        return h, buff.get( h, None)
    def _add_to_buffer_lin(self, h, ptc):
        global lin_buffer, lin_buffer_size
        if len(lin_buffer)>lin_buffer_size:
            lin_buffer.pop(0)
        lin_buffer.append( (h, ptc) )
        
    def getLinearity(self, ditrange=None, signalrange=None,
                     chi2_ditrange=None, chi2_signalrange=None, 
                     force=False, nokeytable=False):
        """
        Return a Linearuty HDU list result of linear fit:
         signal = flux*dit + bias  
        
        - Flux  (ADU/s the slope of the fit)
        - Bias  (ADU  the intercept value of the fit signal = flux*dit+bias)
        - LINNUMBER   (a map of the Number of points used for the fit)
        - LINCHI2     (a chi2 map, result of the linear fit)
        

         KEYWORDS: 
           signalrange : a tuple of (min,max) value for signal to compute the fit
           ditrange : a tuple of (min,max) value for dit to compute the fit 

           chi2_signalrange : a tuple of (min,max) value for signal to compute chi2
           chi2_ditrange : a tuple of (min,max) value for dit to compute chi2 
           if chi2_signalrange or chi2_ditrange, default is signalrange and
           ditrange respectively
        """

        
        lin = None
        
        h, lin = self._look_in_buffer_lin( signalrange=signalrange,ditrange=ditrange,
                                           chi2_ditrange=chi2_ditrange,
                                           chi2_signalrange = chi2_signalrange                                           
                                       )
        if force or (lin is None):
            lin = self.compute_linearity(signalrange=signalrange, ditrange=ditrange,
                                         chi2_ditrange    = chi2_ditrange,
                                         chi2_signalrange = chi2_signalrange  
                                     )           
            self._add_to_buffer_lin(h, lin)
        if nokeytable:
            self._getClass_("linearityhdulist")(lin).pop([f.name for f in lin].index("KEYWORDS"))
        return lin
    
    def compute_linearity(self, ditrange=None, signalrange=None,
                          chi2_ditrange=None, chi2_signalrange=None,  **kwargs):        
        yrange = signalrange
        xrange = ditrange
        chi2_yrange = chi2_signalrange
        chi2_xrange = chi2_ditrange
        
        
        
        if xrange is None:
            xrange = (None, None)
        if yrange is None:
            yrange = (None, None)  

        polar = 0
        illumination = 0
        try:
            polars = self.getPolar()
            
            if len(set(polars))>1:
                self.say("Found more than one detector polar in this list: %s"%(set(polars)), vtype=WARNING)
            polar = max(polars)
            
        except:
            self.say("Cannot determine detector polar", vtype=config.WARNING)
        try:
            illuminations = self.getIllumination()
            if len(set(illuminations))>1:
                self.say("Found more than one detector illumination  in this list: %s"%(set(illuminations)), vtype=config.WARNING)
            illumination  = max(illuminations)
        except:
            self.say("Cannot determine detector illumination", vtype=config.WARNING)
            
        self.say("Computing full frame Linearity with ditrange=%s, signalrange=%s for polar %d"%(ditrange, signalrange, polar), 1)
            
        coeff = self.linearfit( "getDit2data", "getSignal", variance="getSignalvariance",
                                compute_chi2=True, 
                                xrange=xrange, yrange=yrange,
                                chi2_xrange=chi2_xrange,
                                chi2_yrange=chi2_yrange,
                                **kwargs)    
        
       
        
        flux = self._getClass_("flux", True)(coeff[0])
        bias = self._getClass_("bias")(coeff[1])
        N    = self._getClass_("linnumber")(coeff[2])
        chi2 = self._getClass_("linchi2")(coeff[3])
        
        flux.setKey("XFITMIN", xrange[0] or "None" )
        flux.setKey("XFITMAX", xrange[1] or "None" ) 
        flux.setKey("YFITMIN", yrange[0] or "None" )
        flux.setKey("YFITMAX", yrange[1] or "None" )
        flux.setKey("HIERARCH ESO DET POLAR", polar)
        flux.setKey("ILLUM", illumination)
        
        if hasattr( self, "make_keys_table"):
            return self._getClass_("linearity")([flux,bias,N,chi2,self.make_keys_table()])
        else:
            return self._getClass_("linearity")([flux,bias,N, chi2])
        
class _PtcLinearityCapabilities_(_PtcCapabilities_,_LinearityCapabilities_):
    def getDetCarac(self, signalrange=None, ditrange=None, sigma2range=None,
                    chi2_signalrange=None, chi2_sigma2range=None,
                    chi2_ditrange=None,
                    signalrange_ptc=None, signalrange_lin=None,
                    chi2_signalrange_ptc=None, chi2_signalrange_lin=None,
                    force=False):
        """ Return a fits Carac HDUList containing detector carac : 
        - Gain system  (e-/ADU from slope of ptc fit)
        - Noise2       (ADU^2 square of noise as the intercept value of ptc fit)
        - Noise        (ADU This is the rms of the minimum dit image: the BIAS)
        - PTCNumber    (a map of the Number of points used for the fit)
        - PTCCHI2      (a chi2 map, result of the ptc fit)

        - Flux  (ADU/s the slope of the fit  signal = flux*dit+bias)
        - Bias  (ADU  the intercept value of the fit signal = flux*dit+bias)
        - LINNUMBER   (a map of the Number of points used for the fit)
        - LINCHI2     (a chi2 map, result of the linear fit)
        
        KEYWORDS:
           see getPtc and getLinearity function.
           - signalrange_ptc, chi2_signalrange_ptc overwrite signalrange, chi2_signalrange 
                 for ptc fit
           - signalrange_lin, chi2_signalrange_lin overwrite signalrange, chi2_signalrange 
                 for linearity fit
                           
        """
        signalrange_ptc = signalrange_ptc or signalrange
        chi2_signalrange_ptc = chi2_signalrange_ptc or chi2_signalrange

        signalrange_lin      = signalrange_lin or signalrange
        chi2_signalrange_lin = chi2_signalrange_lin or chi2_signalrange
        
        return self._getClass_("ptclinearityhdulist")(
            self.getPtc( signalrange=signalrange_ptc, sigma2range=sigma2range,
                         chi2_signalrange=chi2_signalrange_ptc,
                         chi2_sigma2range=chi2_sigma2range,
                         force=force, nokeytable=True)+
            self.getLinearity( signalrange=signalrange_lin, ditrange=ditrange,
                               chi2_signalrange=chi2_signalrange_lin,
                               chi2_ditrange=chi2_ditrange,
                               
                               force=force)
        )
            
    
        
####################################
# Must be at the end
engine._update_class_dictionary_(__name__)
