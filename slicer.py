import numpy as np
from defaults import Default
from datax import DataX

################################################################################
#
#  Detector Slicer functions 
#
################################################################################

def outputlist2datax( lst):
    x = []
    y = []
    for sec in lst:
        yg, xg = np.mgrid[sec]
        x.append(xg)
        y.append(yg)
    axes = ["output", "y", "x"]
    return DataX(y,axes)  , DataX(x,axes)            

class DetectorSlicer(object):
    pass

class DetectorOutput(DetectorSlicer):
    size = (128,128)
    noutput = 8
    params = Default({})
    def __init__(self, size, noutput, wrapper=None, axis=1):
        self.size = size
        self.imgdim = size #for compatibility
        self.noutput = noutput
        if wrapper is None:
            self.wrapper = outputlist2datax
        self.axis = axis
        
    

    def new(self, **kwargs):
        for k in ["size","noutput","axis"]:
            kwargs.setdefault( k, self.__getattribute__(k))
        return self.__class__( **kwargs ) 
    


    def __call__(self, outputnum=None, size=None, pos=(0,0), noerror=False,  
                 ref="center",
                 **kwargs):
        detector_size = self.size
        noutput = self.noutput
        """
         Return the detector section of given output
         output( outputnum=None, size=None, pos=(0,0), axis=0)
         
         outputnum = output number from 0 to 7 for rapid (None is all the detector)
         size =  size of the box 
         pos  = center position of the box related to the center of the output 
         axis = the axis direction of outputs
         
         s = output( outputnum=2)
         subimage = data[s]
        
        Additional kwargs (anything) are set in self.params, can be use to retrieve 
        additional information for e.g. plots         
        
        """
        if issubclass(type(outputnum), (list,tuple)):
             return self.wrapper([ self(num, size, pos) for num in outputnum])
        
        if outputnum is None:
            section_size = detector_size
            offset = [0,0]
        elif self.axis == 0:
            section_size = [detector_size[0]/noutput, detector_size[1]]
            offset =   ( (outputnum)*section_size[0] , 0)
        else: 
            section_size = [detector_size[0], detector_size[1]/noutput]
            offset = (0 ,  (outputnum)*section_size[1])

        if size is None:
            size = section_size
        elif issubclass(type(size),(list,tuple)):
             if size[0] is None and size[1] is None:
                 size = list(section_size)
             elif size[0] is None:
                 size = [section_size[0],size[1]]
             elif size[1] is None:
                 size = [size[0], section_size[1]]
        else:           
            size = [size,size]
        pos = list(pos)
        
        offsetref = [0,0]
        if ref in ["bl", "bottomleft"]:
            pass            
        elif ref in ["br", "bottomright"]:
            offsetref[1] += section_size[1]-size[1]
            pos[1] *= -1
            
        elif ref in ["tl", "topleft"]:
            offsetref[0] += section_size[0]-size[0]
            pos[0] *= -1
        elif ref in ["tr", "topright"]:
            offsetref[1] += section_size[1]-size[1]
            offsetref[0] += section_size[0]-size[0]
            pos[1] *= -1
            pos[0] *= -1
            
        elif ref in ["tc", "topcenter"]:
            offsetref[0] += section_size[0]-size[0]
            offsetref[1] += section_size[1]/2-size[1]/2
            pos[0] *= -1            
        elif ref in ["bc", "bottomcenter"]:
            offsetref[1] += section_size[1]/2-size[1]/2

        elif ref in ["cl", "centerleft"]:
            offsetref[0] += section_size[0]/2-size[0]/2

        elif ref in ["cr", "centerright"]:
            offsetref[0] += section_size[0]/2-size[0]/2
            offsetref[1] += section_size[1]-size[1]
            pos[1] *= -1
        
        elif ref in ["c", "center", "cc", "centercenter"]:
            offsetref[0] += section_size[0]/2-size[0]/2
            offsetref[1] += section_size[1]/2-size[1]/2
            
        elif isinstance(ref, (tuple,list)) and len(ref)==2:
            offsetref[0] += ref[0]
            offsetref[1] += ref[1]
        else:
            raise KeyError("ref should be on of %s or a 2 element list or tuple"%(["bl", "bottomleft"]+["br", "bottomright"]+["tl", "topleft"]+["tr", "topright"]+["c", "center"]+["tc", "topcenter"]+["bc", "topcenter"]+["cl", "centerleft"]+["cr", "centerright"]))
        
        
        xy =[ (pos[i] + offsetref[i],
               pos[i] + offsetref[i]+size[i]) for i in [0,1]]
        
        
        #xy = [ (section_size[i]/2 - size[i]/2+pos[i]    +offsetref[i], 
        #        section_size[i]/2 + size[i]/2+size[i]%2 +pos[i]+offsetref[i]) for i in [0,1] ]
        
        if not noerror:
            if any( [ (xy[i][0])<0 or (xy[i][1])>section_size[i]  for i in[0,1]]):
                raise Exception("Window is larger or outside output")
        
        s = [ slice( xy[i][0]+offset[i] ,  xy[i][1]+offset[i] ) for i in [0,1] ]
        
        kwargs.update(self.params, outputnum=outputnum, size=size, pos=pos)
        
        out =  DetectorIndexOutput( tuple(s) )
        out.params = out.params.derive( kwargs)
        out.constructor = self
        return out
    #__call__ = fw.fw(__call__, fw.isiterable2)

    
class DetectorIntegratedOpticOutput(DetectorSlicer):
    
    def __init__(self, outputs = 24 , dispersion =10,  wollastons =2, 
                 pos= (0,0),  disp_ref = (0,0), woll_ref = (0,0),
                 dwollastons   = (10,0) , doutputs = 8, 
                 imgdim=(256,320), noutput=8, axis=1, 
                 wrapper = list
                 ):
        # disp_ref is the position of the left bottom pixel of the figure 
        # compared to the same pixel without dispersion 
        # woll_ref is the samething for wollaston 
        self.pos         = pos 
        self.wollastons  = wollastons
        self.dispersion  = dispersion
        self.disp_ref    = disp_ref
        self.woll_ref    = woll_ref        
        self.dwollastons = dwollastons
        self.doutputs    = doutputs
        self.outputs     = outputs
        self.imgdim      = imgdim 
        self.wrapper     = wrapper
        self.noutput     = noutput
        self.axis        = axis

    def new(self, **kwargs):
        for k in ["outputs", "dispersion", "wollastons", "pos", "disp_ref", 
                  "woll_ref", "dwollastons", "doutputs", 
                  "imgdim", "noutput", "axis", "wrapper"]:
            kwargs.setdefault( k, self.__getattribute__(k))
        return self.__class__( **kwargs ) 
        
        
    def _default(self, kwargs, key):
        if key in kwargs:
            return kwargs[key]
        return self.__getattr__(key)
        
    def __call__(self, pos=None, **kwargs):
        if pos is None:
            pos = self.pos
        
        if issubclass( type(pos) , list):
             return self.wrapper( [self(p) for p in pos])
        
        #coordinate of one output 
        dim = (self.wollastons,self.outputs,self.dispersion)
        
        x = np.tile( np.arange(self.dispersion), self.outputs*self.wollastons ).reshape(dim)
        y = np.tile( np.outer(  np.arange(self.outputs), np.ones( (self.dispersion), dtype=int ) ).flatten(), self.wollastons).reshape(dim)
                
        y = y*self.doutputs
        
        for i in range(self.wollastons):
            x[i] = x[i]+( self.dwollastons[0]*i +self.woll_ref[0])
            y[i] = y[i]+( self.dwollastons[1]*i +self.woll_ref[1])
            
        y += pos[0]+self.disp_ref[1] 
        x += pos[1]+self.disp_ref[0] - self.dispersion/2  # Adjust offsetX according to the number of read spectral channels a 7-channel window is [-3,-2,-1,0,1,2,3]
        
        #patch transpose to have dims as [Noutput, Nwollaston , Ndispersion]
        #out = DetectorIndexDispersor( ( y.transpose( (1,0,2) ) ,x.transpose((1,0,2)) ) )
        ## patch forget the wollaston dimention to be more compatible with the IMAGINGDATA format
        ## TODO CHECK in wich way the windows are defined
        out = DetectorIndexDispersor( ( y.reshape((-1,self.dispersion)),
                                        x.reshape((-1,self.dispersion))) )
        
        out.params = out.params.derive(kwargs, pos=pos)
        out.constructor = self
        return out 
    
        
class DetectorIndex(tuple):
    params = Default({})
    constructor = None
    axes  = ["test"]
    def __new__(cls, args):
        if cls.axes:
            args = list(args)
            for i,a in enumerate(args):
                if isinstance(a, np.ndarray ):
                    args[i] = DataX(a, cls.axes)
        return tuple.__new__(cls, args)
    
    def __call__(self, *args, **kwargs):
        if self.constructor is None:
             raise Exception("constructor is not defined for: %s"%self)        
        return self.constructor(*args, **kwargs)
    
    def plot(self, axes=None, **kwargs):
        [kwargs.setdefault(k, self.params.get(k)) for k in ["color", "fill"] if k in self.params]
        kwargs.setdefault( "fill", False)
        
        import matplotlib.pyplot as plt
        r = self.rectangle(offset=kwargs.pop("offset", [0,0]))        
        if axes is None:
            return [ plt.Rectangle(*p, **kwargs) for p in r ]
        return [ axes.add_patch( plt.Rectangle(*p, **kwargs) ) for p in r ]
    
    def img(self, imgdim=None):
        if imgdim is None:
            imgdim = self.constructor.imgdim
        y, x = self        
        img = np.zeros(imgdim, dtype=int)
        img[y,x] =1
        return img     

    def geometry(self, relative=False, offset=0):
        r = self.rectangle()
        N = len(r)
        cmd =  [("DET.SUBWINS",N+offset)]
        cmd += [("DET.SUBWIN.COORDINATES",  "RELATIVE" if relative else "ABSOLUTE")]
        if relative:
            dy, dx = self.params.get('pos', (0,0))
        else:
            dy, dx = 0,0
        
        
        cmd += [ ("DET.SUBWIN%d.GEOMETRY"%(i+1+offset) ,  "%dx%d+%d+%d"%(r[i][1],r[i][2],r[i][0][0]-dx,r[i][0][1]-dy)) for i in range(N) ] 
        return cmd
        
    def move(self, xstep=0, ystep=0):
        pos = list(self.params.get("pos", (0,0)))
        pos[1] += xstep
        pos[0] += ystep
        return self.constructor( **self.params.derive(pos=tuple(pos)) )
    def changeOutput( self, step):
        output = self.params.get("outputnum", self.constructor.noutput/2)
        if output is None:
            return self.move( step*(self.constructor.imgdim[self.constructor.axis] / self.constructor.noutput))
        output += step
  
        return self.constructor( **self.params.derive(outputnum=output) )
        
    
    def moveLeft( self, step=1):
        return self.move( -step,0)
    def moveRight( self, step=1):
        return self.move( step,0)
    def moveUp( self, step=1):
        return self.move( 0,step)
    def moveDown( self, step=1):
        return self.move( 0,-step)
    def nextOutput(self):
        return self.changeOutput(1)
    def prevOutput(self):
        return self.changeOutput(-1)
        
    
class DetectorIndexOutput(DetectorIndex):
    def rectangle(self, offset=[0,0]):
        xmin = self[1].start
        xmax = self[1].stop
        ymin = self[0].start
        ymax = self[0].stop        
        return [[ (xmin+offset[1], ymin+offset[0]), xmax-xmin, ymax-ymin ]]
    @property
    def size(self):
        xmin = self[1].start
        xmax = self[1].stop
        ymin = self[0].start
        ymax = self[0].stop
        return (xmax-xmin)*(ymax-ymin)
    
class DetectorIndexDispersor(DetectorIndex):
    # path now forget the woll axes 
    # axes = ["output","woll","disp"]
    axes = ["output","disp"]
    def rectangle( self, scale=[1,1], offset=[0,0]):
        y,x = self
        dim = x.shape

        if len(dim)>2:
                # reshape to be compatible with geometry
            x = x.reshape( (dim[0]*dim[1], dim[2]))        
            y = y.reshape( (dim[0]*dim[1], dim[2]))        
            size = dim[0]*dim[1]
        else:
            size = dim[0]
        
        return [   [ (x[i].min()+offset[1] , y[i].min()+offset[0]) , x[i].max()-x[i].min()+scale[0] , y[i].max()-y[i].min()+scale[1]  ]   for i in np.arange(size) ]
    
    def changeOutput( self, step):
        return self.move( step*(self.constructor.imgdim[self.constructor.axis]/self.constructor.noutput))
    @property
    def size(self):
        return self[0].size
        
    def envelop(self, margin=0, noerror=True):
        if issubclass(type(margin), tuple):
            my, mx  = margin
        else:
            my, mx = margin, margin
            
        y,x = self
        dy = y.max()-y.min() + my*2
        y = int( (y.max()+y.min())/2) - self.constructor.imgdim[0]/2 -my
        dx = x.max()-x.min() +mx*2
        x = int((x.max()+x.min())/2) - self.constructor.imgdim[1]/2 -mx
        
        return DetectorOutput( self.constructor.imgdim, self.constructor.noutput, axis=self.constructor.axis)(None,(dy,dx),(y,x), noerror=noerror) 

