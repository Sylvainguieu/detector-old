from detector import config
if config.debug: print "engine.py"
import numpy as np
import os
from datax import DataX, dataxconcat, size,size_of_shape, isdatax



def getClass_in_module(classname,module, primary=False):
    """ Return the right class given for a given name 
    primary keyword argument as no effect here, just for Fits compatibility
    """
    if config.class_dictionary is None:
        raise Exception("config.class_dictionary is empty" )        
    if classname in config.class_dictionary[module]:
        return config.class_dictionary[module][classname]
    return None

def _end_getClass(classname,primary=False):
    """ In engine.py this is a dead end """
    raise ValueError ("cannot fint class associated to '%s'"%classname)

def getClass_decorator(module_name, prefixes, endfunc):
    def tmp_getClass(classname,primary=False):
        """ Return the right class given for a given name 
        primary keyword argument as no effect here, just for Fits compatibility
        """
        if config.class_dictionary is None:
            raise Exception("config.class_dictionary is empty" )
        for mod in [module_name, "__all__"]:
            for pref in prefixes:
                cl = getClass_in_module(classname+pref ,mod, primary)
                if cl is not None: return cl

            cl = getClass_in_module(classname ,mod, primary)
            if cl is not None: return cl        
        return endfunc(classname,primary=primary)
    return tmp_getClass

getClass = getClass_decorator(__name__,["entity","switch","datalist"], _end_getClass)
    
def _rename_class(clsname):
    return clsname.lower()
  
def _update_class_dictionary_(module=__name__):
    import sys
    nd = {_rename_class(k):v for k,v in sys.modules[module].__dict__.iteritems() if type(v) is type and issubclass(v,(DataEntity,Switch,DataList)) }
    if not module in config.class_dictionary:
        config.class_dictionary[module] = nd
    else:
        config.class_dictionary[module].update(nd)
    config.class_dictionary["__all__"].update(nd)
    
def parseclass(obj, cls):
    obj.__class__ = cls

    
def say(msg, level=1, vtype=config.NOTICE, indent=True):    
    if config.verbose>= level:
        _print_msg(msg, vtype, indent=indent)

def objsay(obj,msg, level=1, vtype=config.NOTICE, indent=True):
    """ say(obj, msg ) 
    function to verbose a message. obj should be an object with the .verbose 
    if obj.verbose>= level: print msg
    attribute
    
    """
    if not hasattr(obj, "verbose"): return 
    if obj.verbose>=level:
        return _print_msg(msg, vtype, indent=indent)

def _get_key_name(name):
    return "get"+name.capitalize()
def _get_key_to_data_name(name):
    return "get"+name.capitalize()+"2data"
def _get_data_name(name):
    return "get"+name.capitalize()
def _encaps_name(name):
    return name.capitalize()
def _get_scalar_data_name(name,fname):
    return "get"+fname.capitalize()+name.capitalize()
def _header_name(name):
    return "get"+name.capitalize()+"Header"
def _with_scalar_name(name, fname):
    return "with"+fname.capitalize()+name.capitalize()
def _with_name(name):
    return "with"+name.capitalize()
def _set_name(name):
    return "get"+name.capitalize()+"Set"
def _set_scalar_name(name, fname):
    return "get"+fname.capitalize()+name.capitalize()+"Set"
def _iter_name(name):
    return "iter"+name.capitalize()



def _print_msg(msg, vtype, indent=True):
    if indent:
        print "%s : %s"%(vtype, msg)
    else:
        print "%s : %s"%(vtype, msg), 

def _list_decorator_prepare(obj, onsection, kwargs, lenfunc=len, inaxis=None,
                            on_default_attr="_on_default"):                
        section = None
        freduce = None        
        axes = None

        
        if onsection is False:
            if hasattr(obj,on_default_attr):
                onsection = getattr(obj,on_default_attr)
            else:
                onsection = None
        
        
        if inaxis is not None:
            axes = [inaxis]
        elif hasattr(obj ,"axes"):
            axes =  obj.axes
            
        if axes is not None:
            if axes and len(axes):
                if axes[0] in config.idxkeys: # check is the short/lazy keyword is accepted
                    section = kwargs.pop(axes[0], None)                    
                if str(axes[0])+"_idx" in kwargs:
                    if section is not None:
                        say("conflict between '%s_section' and '%s' keyword, '%s' is retained"%(axes[0], axes[0], axes[0])) 
                    section = kwargs.pop(str(axes[0])+"_idx", None)

            ######
            # If section is anything else than a slice, a int None or array 
            # convert it to an array 
            if (section is not None) and (not isinstance(section,slice)) and (not isinstance(section,(int,long))) and (not isinstance(section,np.ndarray)):
                section = np.array(section)
            ####
            # if section is boolean convert it to an array of index
            if isinstance(section, np.ndarray) and section.dtype == np.bool:
                section = np.where(section.flat)[0]
            ###
            # if section is a datax takes its axes 
            if isdatax(section):
                axes = section.axes
            else:
                axes = axes[0:1]
            
            freduce = { ax+"_reduce":kwargs.pop(ax+"_reduce") for ax in axes if ax+"_reduce" in kwargs}        
        if isinstance(onsection, slice):
            onsection = np.r_[0:lenfunc(obj)][onsection]
        
        # ## ## ## ## ##
        # if onsection is a scalar, section and freduce should be None !
        # ## ## ## ## ##
        if onsection is not None and not hasattr(onsection, "__len__"):
            if section is not None:
                raise IndexError( " %s_idx is %s on an index with no lens "%(axes[0],section))
            #if freduce is not None:
            #    raise IndexError( " %s is %s on an index with no lens "%(axes[0]+"_reduce",freduce))
        
        if onsection is None and section is not None:                       
            index = np.r_[0:lenfunc(obj)][section]
        elif section is not None:                
            index = np.r_[0:len(onsection)][section]
        else:
            index = onsection
            
        if index is None:
            index = range(lenfunc(obj))
        if isinstance(index, np.ndarray): index = index.flat
        return index, section, freduce

def _return_list_array_decorator(A, section, freduce):
    ##
    # At this point A should be flat
    # we need to return A to reflect the section attribute if this one
    # was a N>2 array dimention.
    if isinstance( A, np.ndarray):
        if isinstance( section,  np.ndarray):
            index = np.arange(section.size).reshape(section.shape)
            if isdatax( section):                
                index = DataX( index, section.axes )
            print A, index
            A = A[index]
    
    if isdatax( A) and freduce is not None and len(freduce):
        A = A.apply( **freduce) 
        
    return A


def _get_item_exec_method(obj, funcname, item , args, kwargs):
    return getattr(obj[item], funcname)(*args, **kwargs)
def _get_item_exec_func(obj, func, item , args, kwargs):
    return func(obj[item],*args, **kwargs)
_len_func = len

def _list_decorator(funcname, on=False, inarray=True, len_func=len,
                    exec_func=_get_item_exec_func, inaxis=None, on_default_attr="_on_default"):
    """
    A decorator that loop over the list and apply the right method
    if defaulindex is not None use that index as default insteed of looping 
    if on  is not None the onis always used and default is ignored 
    """
    def tmp_decorator_list(self,*args, **kwargs):
        """
        A list looper for the attribute %s
        """%funcname          
        index, section, freduce = _list_decorator_prepare(
            self, on , kwargs, len_func, inaxis=inaxis, on_default_attr=on_default_attr
        )                
        if  hasattr(index,"__iter__"):
            A = self.finalise([ exec_func(self,funcname,i, args, kwargs) for i in index ], inarray=inarray, inaxis=inaxis)      
        else:
            return exec_func(self,funcname,index, args, kwargs)
        
        return _return_list_array_decorator( A, section, freduce)    
    return tmp_decorator_list


def list_decorator(func_or_methodname, on=False, inarray=True, inaxis=None):
    if isinstance(func_or_methodname, basestring):
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_method, inaxis=inaxis)
    else:
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_func, inaxis=inaxis)


    

def data_decorator(func_or_methodname, on=False, inarray=True, inaxis=None):
    if isinstance(func_or_methodname, basestring):
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_method, inaxis=inaxis,
                               on_default_attr="_on_data_default"
                           )
    else:
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_func, inaxis=inaxis,
                               on_default_attr="_on_data_default"
                           )

def key_decorator(func_or_methodname, on=False, inarray=True, inaxis=None):
    if isinstance(func_or_methodname, basestring):
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_method, inaxis=inaxis,
                               on_default_attr="_on_key_default"
                           )
    else:
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_func, inaxis=inaxis,
                               on_default_attr="_on_key_default"
                           )

def reduce_decorator(getname,func):    
    def tmp_get(self, *args, **kwargs):
        return func( getattr(self,getname)(*args, **kwargs)) 
    return tmp_get

def encaps_decorator(classname,getname=None, getheadername=None):
    if getname is None:
        getname = _encaps_name(classname)
    if getheadername is None:
        getheadername = _header_name(classname)
    def tmp_encaps_get(self, *args, **kwargs):                
        return self._getClass_(classname)( getattr(self,getname)(*args, **kwargs),
                                           getattr(self,getheadername)(kwargs))
    return tmp_encaps_get

def key_to_data_decorator(methodname):
    def tmp_key_to_data_decorator(self, *args, **kwargs):
        value = getattr(self,methodname)(*args)
        if (isinstance(value,np.ndarray) and value.size>1) or hasattr( value,"__iter__"):
            raise ValueError( "%s() didn't returned a scalar"%methodname )
        output = np.ndarray(self.getDataShape(), dtype=np.dtype(type(value)))
        output[...] = value
        return self._return_array_data( output, kwargs)
    return tmp_key_to_data_decorator

def _polar_len(obj):    
    return len(obj.getPolarSet())

def _get_polar_exec_method(obj, funcname, item , args, kwargs):
    return getattr(obj.withPolar(obj.getPolarSet()[item]),funcname)( *args, **kwargs)

def polar_decorator(func_or_methodname, on=None, inarray=True, inaxis=None):
    return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=_polar_len,
                           exec_func=_get_polar_exec_method,  inaxis=inaxis)

def _dummy_(val):
    return val

def _exec_with_range(methodname, ranges, wrap, args, kwargs):
    minval, maxval = ranges
    def _tmp_exec_with_range(obj):
        if minval is None and maxval is None: return True        
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        if maxval is None:
            return value>=minval
        if minval is None:
            return value<=maxval
        return (value>=minval) & (value<=maxval)
    return _tmp_exec_with_range

def _exec_with_val(methodname, valtest, wrap, args, kwargs):
    def _tmp_exec_with_val(obj):
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        return valtest==value
    return _tmp_exec_with_val

def _exec_with_iterable(methodname, lst, wrap, args, kwargs):
    def _tmp_exec_with_iterable(obj):
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        return value in lst
    return _tmp_exec_with_iterable

def _exec_with_array(methodname, A, wrap, args, kwargs):
    def _tmp_exec_with_array(obj):
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        return any(value == A)
    return _tmp_exec_with_array
                     
def _with_decorator_method(methodname, wrap):
    def tmp_with_method(self,values, *args, **kwargs):

        if not isinstance( values, np.ndarray) and hasattr( values, "__call__"):
            values = values([wrap(getattr(obj,methodname)(*args,**kwargs)) for obj in self])
        
        if isinstance( values, np.ndarray):
            ffunc = _exec_with_array(methodname, values, wrap,args, kwargs)
        elif isinstance( values, tuple):            
            ffunc = _exec_with_range(methodname, values, wrap, args, kwargs)
        elif hasattr( values, "__iter__"):
            ffunc = _exec_with_iterable(methodname, values,wrap,  args, kwargs)
        else:
            ffunc = _exec_with_val(methodname, values, wrap, args, kwargs)
        
        return self.filter(ffunc)
    return tmp_with_method
            
         
def with_decorator(func_or_methodname, wrap=_dummy_):
    return _with_decorator_method(func_or_methodname, wrap)

def set_decorator(methodname, wrap=_dummy_, inaxis=None):
    def tmp_set_decorator(self,*args, **kwargs):
        values = list(set([wrap(getattr(obj,methodname)(*args,**kwargs)) for obj in self]))
        values.sort()
        if inaxis is not None:
            return DataX(np.array(values), [inaxis])
        return values
    return tmp_set_decorator


class IterKey(object):
    _index = -1
    color_cycle  = config.color_cycle
    marker_cycle = config.marker_cycle
    
    def __init__(self, obj, key="", _values=None, **kwargs):
        self._key    = key
        if key:
            self._method = getattr(obj, _with_name(key))
        else:            
            self._method = lambda index: obj[index]
        
        if _values is None:
            if not key:
                self._values = range(len(obj))
            else:
                self._values = getattr(obj, _set_name(key))()
        else:
            self._values = _values
        self._size = len(self._values)
        self.kwargs = kwargs
    
    def __iter__(self):
        self._index = -1
        return self
    def __len__(self):
        return self._size
    
    def __getitem__(self, item):
        return self._method(self._values[item])
    
    def __getattr__(self, attr):
        if attr in self.kwargs:
            return self.get_kw(attr)
        raise AttributeError("this Iterator does not have attribute '%s' "%attr)
    
    def next(self):
        self._index += 1
        if self._index>=self._size:
            raise StopIteration()
        return self._method(self._values[self._index])
    @property
    def value(self):
        if self._index<0: return self._values[0]
        return self._values[self._index]
    @property
    def legend(self):
        if "legend" in self.kwargs: return self.get_kw("legend")
        
        return self.getlegend()
    _legend = "{0._key} = {0.value:}"
    def getlegend(self):
        return self._legend.format(self)
    @property
    def color(self):
        if "color" in self.kwargs: return self.get_kw("color")
        return self.color_cycle[self._index%len(self.color_cycle)]
    @property
    def marker(self):
        if "marker" in self.kwargs: return self.get_kw("marker")
        return self.marker_cycle[self._index%len(self.marker_cycle)]
    @property
    def axes(self):
        if "axes" in self.kwargs: return self.get_kw("axes")
        return (self._size,self._index+1)
    @property
    def figure(self):
        if "figure" in self.kwargs: return self.get_kw("figure")
        return self._index+1
    def get_kw(self, key):
        v = self.kwargs[key]
        if hasattr(v,"__iter__") and not isinstance(v,tuple):
            return v[self._index%len(v)]
        return v
    @property
    def kw(self):
        return {k:self.get_kw(k) for k in self.kwargs}

class IterKwargs(IterKey):
    def __init__(self,obj, *args, **kwargs):
        self.kwargs = dict(*args, **kwargs)
        self._size = 0
        self._obj = obj 
        for k,v in kwargs.iteritems():
            if hasattr(v,"__iter__") and not isinstance(v,tuple):
                self._size = max( self._size, len(v))
    def next(self):
        self._index += 1
        if self._index>=self._size:
            raise StopIteration()
        return self._obj
    
    def __getitem__(self, item):
        return self._obj
    @property
    def value(self):
        return None

def iter_decorator(key):
    def tmp_iter_decorator(self, *args, **kwargs):
        return IterKey(self, key, *args, **kwargs)
    return tmp_iter_decorator



    


def header_decorator(getfunc, on=0):
    def tmp_header_decorator(self, *args, **kwargs):
        getf   = getattr(self[on], getfunc)
        #kwargs.setdefault("copy", True)
        header = getf(*args, **kwargs)
        header.set("HIERARCH ESO NFILE", len(self))
        for i,name in enumerate(self.getFileName(),1):
            k = "HIERARCH ESO FILE%d"%i
            # truncate the file name
            kn = len(k)
            attemp = 0
            while kn+len(name)>76:
                if attemp == 0:
                    # try to remove the extention
                    name = os.path.splitext(name)[0]
                else:
                    name = name[:72-kn]+"..."
                attemp += 1 
            header.set(k, name)
        #header.set("COMMENT",":FILES:"+",".join(self.getFileName())+":FILES:")
        return header
    return tmp_header_decorator

def newHeader(obj, *args, **kwargs):
    return obj.newHeader( *args, **kwargs) 

def _map( func, iterable):
    """ same as map but return in a datax if iterble is a datax """ 
    if not hasattr( iterable, "__iter__"):
        return func(iterable)    
    output = map( func, iterable)
    if isdatax(iterable):
        return DataX( output, iterable.axes)
    return type(iterable)(output)

def _get_directory(dirName, rootdir=None):
    """ _get_directory( dirName, rootdir)
    return the directory without the rootdir
    """
    if rootdir:
        rootdirs = rootdir.rstrip("/").split("/")
        dirs = dirName.split("/")
        if rootdirs == dirs[0:len(rootdirs)]:
            return "/".join(dirs[len(rootdirs):])
    return dirName



  
def add_key_capability_as_entity(cls,name,func, add_key_data=config.add_key_data):
    getname  = _get_key_name(name)
    setattr(cls, getname, func)
    if add_key_data:
        cls._add_key_to_data_capability(name)

def add_key_to_data_capability_as_entity(cls,name):
    getkeydataname = _get_key_to_data_name(name)
    getname  = _get_key_name(name)
    setattr(cls, getkeydataname, key_to_data_decorator(getname))



class _KeyEntityCapabilities_(object):
    """
    Provide function to add capability to the _Entity_ class easely 
    
    _add_key_capability(name,func)    : add the function with the name getName 
    _add_key_to_data_capability(name) : make a getName2Data method which will return 
                                        the scalar key value in a data shape
   
    """
    _add_key_capability = classmethod(add_key_capability_as_entity)
    _add_key_to_data_capability = classmethod(add_key_to_data_capability_as_entity)

    ###############################
#
# Consern mostly keys function 
#
################################

    def getFilePath(self):
        raise NotImplementedError()
    def getFileDirectory(self):
        """ Return the directory where file has been open """
        path = self.getFilePath()
        return _map( lambda p:os.path.split(p)[0], path )
    
    def getFileName(self):
        """ Return the file name (without directory) """
        path = self.getFilePath()
        return _map( lambda p: os.path.split(p)[1], path)

    def getDirectory(self, rootdir=None):
        """ Same has getFileDirectory but substract a rootdir to the path"""
        dirs = self.getFileDirectory()
        return _map( lambda d: _get_directory(d,rootdir=rootdir), dirs)

    
    def getKey(self,k, default=None):
        """
        obj.getKey(key, default=None)
        Return the value of key if key is present else deturn default
        """
        kc = self.header.get(k, default) 
        if isinstance(kc , tuple):
            kc = kc[0]        
        return kc
    def setKey(self,k, v):
        """
        obj.setKey(key,val)
        Set the key/value pair  
        """
        self.header[k] = v
        
    def hasKey(self,k):
        """
        obj.hasKey()
        Return True is the object has key False otherwise 
        """
        return k in self.header
    
    def getKeyComment(self,k):
        """
        obj.getKeyComment(k) 
        return the eventual comment of the leyword k or "" if no comment
        """
        kc = self.header.get(k) 
        if isinstance(kc , tuple):
            return kc[1]
        return ""
      
    
    def getAttr(self, attr):
        """ obj.getAttr("attr") ->  obj.attr       
        """
        return self.__getattribute__(attr)
    
    def callAttr(self, attr, *args, **kwargs):
        """
        obj.callAttr( "attr", *args, **kwargs) ->  obj.attr(*args, **kwargs)
        """
        return self.getAttr(attr)( *args, **kwargs)



def add_data_capability_as_entity(cls,name, datafunc, headerfunc=newHeader,
                                  clsname=None, add_scalar=config.add_scalar,
                                  inplace=config.inplace
):
    getname    = _get_data_name(name)
    if not (inplace and hasattr(cls, getname)):
        setattr(cls, getname, datafunc)        
    if clsname:
        cls._add_header_capability(name, headerfunc, inplace=inplace)
        cls._add_encaps_capability(name, clsname, inplace=inplace)

    if add_scalar:
        cls._add_scalar_data_capability(name, inplace=inplace)

def add_header_capability_as_entity(cls, name, headerfunc, inplace=config.inplace):
    headername =  _header_name(name)
    if not (inplace and hasattr(cls, headername)):
        setattr(cls, headername, headerfunc)

def add_encaps_capability_as_entity(cls,name, clsname, inplace=config.inplace):
    headername =  _header_name(name)
    getname    = _get_data_name(name)
    encapsname    = _encaps_name(name)
    if not (inplace and hasattr(cls, encapsname)):
        setattr(cls, encapsname, encaps_decorator(clsname,getname,headername)) 

def add_scalar_data_capability_as_entity(cls, name, reduces=config.reduce_to_scalar, inplace=config.inplace):
    getname   = _get_data_name(name)
    for fname,func in reduces.iteritems():
        getrname  = _get_scalar_data_name(name, fname)        
        if not (inplace and hasattr(cls, getrname)):
            setattr(cls, getrname , reduce_decorator(getname,func))

###
# Add the getKey2data method 
_KeyEntityCapabilities_._add_key_to_data_capability("key")
    
class _DataEntityCapabilities_(object):
    """
     _add_data_capability(name,func,headerfunc=newHeader, clsname=None, 
                         add_scalar=config.add_scalar): 
          * Add the getNameData function
          * if cls name is not None add method to return an other Entity data (like fits HDU)
            In this case the headerfunc function is used to create a  getNameHeader function
            wich return a header
          * add_scalar (default in config.add_scalar) will run the _add_scalar_data_capability

    _add_scalar_data_capability(name, reduces=)
          Will add automaticaly the functions aimed to reduce the data to scalar
          reduces is a dictionary with key as the reduce name and values as reduce function 
          defaults are in config.REDUCE_TO_SCALAR
          e.i:  _add_scalar_data_capability( "img", reduces={"min":np.min,"max":np.ma})
                will add the  .getMinImg and getMaxImg methods
                They are mostly usefull when encapsuled in lists 
    _add_encaps_capability(name, clsname)
    
    """
    _add_data_capability        = classmethod(add_data_capability_as_entity)
    _add_header_capability      = classmethod(add_header_capability_as_entity)
    _add_encaps_capability      = classmethod(add_encaps_capability_as_entity)
    _add_scalar_data_capability = classmethod(add_scalar_data_capability_as_entity)

    data_params = None
    data = None
    @staticmethod
    def _updateKwargs(kwargs):
        return 
    
    def _return_array_data_and_scale(self, data, kwargs, axes=None):
        """
        Return the data  but offset it and/or scale it if needed 
        """
        noscale  = kwargs.pop("noscale", False)
        nooffset = kwargs.pop("nooffset", False)
        data =  self._return_array_data(data, kwargs, axes=axes)
        if not nooffset:
            data =  self.offsetData(data, **kwargs)
        if not noscale:
            data =  self.scaleData(data , **kwargs)
        return data
    
    def _return_array_data(self, data, kwargs, axes=None):
        
        axes = self.axes if (axes is None) and hasattr(self,"axes") else axes if isinstance(data, np.ndarray) else []
        if axes:
            self._updateKwargs(kwargs)            
            if len(kwargs): self.say("reducing the data with %s"%kwargs)            
            data = DataX(data, axes).apply( **kwargs)
        elif len(kwargs):
            ###
            # Decide or not to put an error here
            self.say( "entity has no axes name, data is not a DataX. reduce and section kwargs will be ignored ", vtype=config.WARNING)
        
        return data        

        
    def getData(self, copy=False, **kwargs):
        """
        obj.getData( **kwargs)
 
        Return the data inside a DataX object if obj as the axes attribute else in a nparray.
         - Keywords argument are eventually applied to the data before returned.
         - The data is offsetted and/or scale if obj has offset array (or scalar) or 
           scale array 

        Example: 
           if obj contain a data of dimention  N*Ny*Nx and axes ["time", "y", "x"]
        > obj.getData( time_reduce=np.mean) 
          will return a Ny*Nx image where the time axis has been averaged
        > obj.getData(  x_idx=slice(10,21), y_idx=slice(30,41) )
          will return a N*10*10 cube 
        > obj.getData(  x_idx=slice(10,21), y_idx=slice(30,41), x_reduce=np.mean, y_reduce=np.mean )
          is   obj.getData()[:,10:21,30:41].mean( axis=["x","y"])   
        
        """
        
        if copy:
            return self._return_array_data_and_scale(self.data.copy(), kwargs)
        else:
            return self._return_array_data_and_scale(self.data   , kwargs)

    def getSize(self, axis=None):
        """ Return the data size along the given axis 
        if axis is None return the full data size
        axis can be a list of axis.
        """
        return size_of_shape(self.getShape(), self.getAxes(), axis=axis)
    getDataSize = getSize
    
    def getShape(self):
        """
        Return the shape of data has for a nparray
        """
        return self.data.shape    
    getDataShape = getShape
    
    def getAxes(self):
        return list(self.axes)
    getDataAxes = getAxes
    
    def offsetData(self, data, offset=None,**kwargs):
        """
        Substract an offset to the reduced data
        if keyword offset is present use it otherwhise look at 
        the hdu.offset attribute. 
        If all of them are None data is reutrned as it is silently
        """
        data_params = self._getDataParams()
        if data_params.get("offset",None) is None and offset is None: return data
        
        self.say( "Substract offset to the data") 
        return data + self.getOffset(offset=offset,**kwargs)
    def _getDataParams(self):
        if self.data_params is None:
            self.data_params  = {}
        return self.data_params

    def getOffset(self, offset=None, **kwargs):
        """
        Return the offset array or value if any, else return 0.0
        """
        data_params = self._getDataParams()
        offset = data_params.get("offset",None) if offset is None else offset    
        
        if offset is None: return 0.0
        if isdatax(offset):
            return offset.apply(**kwargs)        
        elif len(kwargs):
            ###
            # Decide or not to put an error here
            if isinstance(offset, np.ndarray) and offset.size:
                self.say( "offset is not a DataX. reduce and\
                section kwargs will be ignored", vtype=config.WARNING)
        return offset

    def getScale(self, scale=None, **kwargs):
        """
        Return the scale data or value if any else return 1.0 
        """
        data_params = self._getDataParams()
        scale = data_params.get("scale",None) if scale is None else scale
        if scale is None: return 1.0
        if isdatax(scale):
            return scale.apply(**kwargs)
        elif len(kwargs):
            if isinstance(scale, np.ndarray) and scale.size:
                self.say( "scale is not a DataX. reduce and\
                section kwargs will be ignored", vtype=config.WARNING)
                
        return scale
    
    def scaleData(self, data, scale=None, **kwargs):
        """
        Multiply by a scale factor the data
        if keyword scale is present use it otherwhise look at 
        the hdu.scale attribute. 
        If all of them are None data is reutrned as it is silently
        """
        data_params = self._getDataParams()
        if data_params.get("scale",None) is None and scale is None: return data
        self.say( "Rescaling the data ") 
        return data*self.getScale(scale=scale,**kwargs)

    def setOffset(self, offset):
        """ 
        Set the offset to the HDU 
        offset must be compatible with the data and can be a DataX object 
        """
        data_params = self._getDataParams()
        data_params['offset'] = offset
        
    def setScale(self, scale):
        """ 
        Set the scale to the HDU 
        scale must be compatible with the data and can be a DataX object 
        """
        data_params = self._getDataParams()        
        data_params['scale'] = scale
    
    def delOffset(self):
        """
        obj.delOffset()
        Delete the current offset if any 
        """
        data_params = self._getDataParams()  
        data_params.pop("offset", None)
    
    def delScale(self):
        """
        obj.delScale()
        Delete the current scale if any         
        """
        data_params = self._getDataParams()  
        data_params.pop("scale", None)
        
    def getHeader(self, kw=None, copy=False):
        """ 
        obj.getHeader( )
        Return the header of the curent Data         
        """
        if copy:
            header =  self.header.copy()
        else:
            header =  self.header
        if  kw and len(kw):
            comment = "COMMAND:kw"%(kw)
            if not "COMMENT" in header:
                header["COMMENT"] = comment
            else:
                header["COMMENT"] +="\n"+comment
        return header
    
    def newHeader(self, *args, **kwargs):
        """
        obj.newHeader( )
        obj.newHeader( dictionary , key1=val1, key2=val2, ....)
        Return a new empty Header filled with the key/vals pairs 
        """
        return dict(*args,**kwargs)


    
###
# Add the getMinData, getMaxData, getMeanData, getStdData methods 
_DataEntityCapabilities_._add_scalar_data_capability("data")

class _EntityCapabilities_(_DataEntityCapabilities_,_KeyEntityCapabilities_):
    pass
    
class DataEntity(_EntityCapabilities_):
    """
    A very simple data older 
    """
    _name = ''
    _getClass_ = staticmethod(getClass)
    say = objsay
    data = None

    def __init__(self,data=None, header=None, axes=None, name="", **kwargs):
        header = header or {}
        header.update(kwargs)
        self.header = header
        self.data   = data
        self.name   = name
        if axes is not None: self.axes = list(axes)
        else:
            if isdatax(data): self.axes = list(data.axes)
    
    @property
    def name(self): return self._name
    @name.setter
    def name(self, value): self._name = str(value.upper())
    @name.deleter
    def name(self): self._name = ""
    
    def isName(self, name):
        return self.name == name
    
class _KeyTableDataCapabilities_(_KeyEntityCapabilities_):
    pass
    
class _DataTableDataEntityCapabilities_(_EntityCapabilities_):
    """ This is a data entity where the data is a recarray 
    the offset and scale have effect only if the method 
    isOffsetable(name) return True
    """
    def getData(self, name=None, copy=False, **kwargs):
        """  
        obj.getData(name, **kwargs)
 
        Return the data of name from a recarray inside a DataX object (if obj as the
        axes attribute else in a nparray.)
         - Keywords argument are eventually applied to the data before returned.
         - The data is offsetted and/or scale if obj has offset array (or scalar) or 
           scale array and if the method isOffsetable(name) return True (default is false)
        """
        axes = self.getDataAxes(name)
        
        if name is None:
            if copy:                
                return self._return_array_data( self.data.copy(), kwargs, axes=axes)
            else:
                return self._return_array_data( self.data, kwargs, axes=axes)
            
        if self.isOffsetable(name):
            if copy:                
                return self._return_array_data_and_scale( self.data[name].copy(),kwargs, axes=axes)
            else:
                return self._return_array_data_and_scale( self.data[name], kwargs, axes=axes)
        
        if copy:                
            return self._return_array_data( self.data[name].copy(), kwargs, axes=axes)
        else:
            return self._return_array_data( self.data[name], kwargs, axes=axes)
        
    def getDataAxes(self, name=None):
        """ Return the axes list according to the wanted data 
        taken in the recarray 
        """
        if name is None: return self.axes
        return self.axes
        
    def getShape(self, name=None):
        """ return the data shape, a optional argument is the name of data 
        inside the recarray
        """
        if name is None:
            return self.data.shape
        
        return self.data[name].shape
        
    def isOffsetable(self,name):
        """
        isOffsetable(name)
        Return True if the data with name NAME inside the recarray is offsetable (and scalable)
        """
        return False


class TableDataEntity(_DataTableDataEntityCapabilities_, _KeyTableDataCapabilities_, DataEntity):
    pass


    
######
#  Add the Scalar meethod for Data

def add_derived_key_capability_as_list(cls, name,  inaxis=None, inplace=config.inplace):
    getname  = _get_key_name(name)
    withname = _with_name(name)
    setname  = _set_name(name)
    itername = _iter_name(name)
    inaxis = inaxis or name
    
    if not (inplace and hasattr(cls, withname)):
        setattr(cls, withname, with_decorator(getname))
    if not (inplace and hasattr(cls, setname)):
        setattr(cls, setname , set_decorator(getname, inaxis=inaxis))
    if not (inplace and hasattr(cls, itername)):
        setattr(cls, itername , iter_decorator(name)) 
        
    
def add_key_capability_as_switch(cls,name, on=False, inarray=True, inaxis=None, add_key_data=config.add_key_data, inplace=config.inplace):
    if on is False:
        on = cls._on_key_default
    getname  = _get_key_name(name)
    if add_key_data:
         add_key_to_data_capability_as_switch(cls,name, on=on, inarray=inarray, inplace=inplace)                 
    if not (inplace and hasattr(cls, getname)):
        setattr(cls, getname, key_decorator(getname, on=on, inarray=inarray))

def add_key_capability_as_list(cls,name, on=False, inarray=True, inaxis=None, add_key_data=config.add_key_data, inplace=config.inplace):
    if on is False:
        on = cls._on_key_default
    getname  = _get_key_name(name)
    if add_key_data:
         add_key_to_data_capability_as_list(cls,name, on=on, inarray=inarray, inplace=inplace)          
    add_derived_key_capability_as_list(cls, name, inaxis=inaxis, inplace=inplace)
        
    if not (inplace and hasattr(cls, getname)):
        setattr(cls, getname, key_decorator(getname, on=on, inarray=inarray))
                
        
def add_key_to_data_capability_as_switch(cls, name, on=False, inarray=True,  inplace=config.inplace):
    if on is False:
         on = cls._on_key_default
    getkeydataname = _get_key_to_data_name(name)
    if not (inplace and hasattr(cls, getkeydataname)):
        setattr(cls, getkeydataname, data_decorator(getkeydataname, on=on, inarray=inarray))
    
add_key_to_data_capability_as_list = add_key_to_data_capability_as_switch 


def add_data_capability_as_switch(cls, name, on=False, inplace=config.inplace,
                                  clsname=None, add_scalar=config.add_scalar):
    if on is False:
        on = cls._on_data_default            
    getname    = _get_data_name(name)
    headername = _header_name(name)
    
    if not (inplace and hasattr(cls, getname)):
        setattr(cls, getname   ,  data_decorator(getname   ,   on=on) )
    #elif not hasattr(cls, getname):
    #    print "WARNING inplace=True but method %s is not defined"%getname
        
    if clsname:
        ###
        # if a class name is present means it has a method to encapsulate
        # in a new DataEntity (e.g. HDU or other)
        encapsname    = _encaps_name(name)
        if not (inplace and hasattr(cls, getname)):
            setattr(cls, encapsname, encaps_decorator(clsname, getname, headername))

        cls._add_headerfunc(headername, on = on, inplace=inplace)        
    if add_scalar:
        cls._add_scalar_data_capability(name, on=on, inplace=inplace)
        
add_data_capability_as_list = add_data_capability_as_switch
       
def add_scalar_data_capability_as_switch(cls, name, inplace=config.inplace,
                                         reduces=config.reduce_to_scalar, on=False, inaxis=None):
    
    if on is False:
        on = cls._on_data_default  
    for fname in reduces:
        getname  = _get_scalar_data_name(name, fname)        
        if not (inplace and hasattr(cls, getname)):
            setattr(cls, getname , data_decorator(getname, on=on))
        
def add_scalar_data_capability_as_list(cls, name, inplace=config.inplace,
                                       reduces=config.reduce_to_scalar, on=False, inaxis=None):    
    if on is False:
        on = cls._on_data_default
    
    for fname in reduces:
        getname  = _get_scalar_data_name(name, fname)
        
        if not (inplace and hasattr(cls, getname)):
            setattr(cls, getname , data_decorator(getname, on=on))

        withname = _with_scalar_name(name, fname)
        setname  = _set_scalar_name(name, fname)
                
        if not (inplace and hasattr(cls, withname)):
            setattr(cls, withname, with_decorator(getname))
        if not (inplace and hasattr(cls, setname)):
            setattr(cls, setname , set_decorator(getname, inaxis=inaxis))



def add_headerfunc_as_switch(cls, headername, on=False, inplace=config.inplace):
    if on is False:
        on = cls._on_data_default
    if not (inplace and hasattr(cls, headername)):
         setattr(cls, headername,  key_decorator(headername,   on=on) )
def add_headerfunc_as_list(cls, headername, on=False, inplace=config.inplace):
    if on is False:
        on = cls._on_data_default
    if not (inplace and hasattr(cls, headername)):
        setattr(cls, headername,  header_decorator(headername))



        

class _KeySwitchCapabilities_(object):
    _on_key_default   = 0
    _add_key_capability         = classmethod(add_key_capability_as_switch)
    _add_key_to_data_capability = classmethod(add_key_to_data_capability_as_switch)
    getKey        = key_decorator("getKey")
    getKeyComment = key_decorator("getKeyComment")
        
    getFilePath       =  key_decorator("getFilePath")
    getFileName       =  key_decorator("getFileName")
    getFileDirectory  =  key_decorator("getFileDirectory")
    getDirectory      =  key_decorator("getDirectory")

_KeySwitchCapabilities_._add_key_capability("key")

def getAxis(self, axis=None):
    if axis is not None:
        return [axis]
    return self.axes

def finalise(self, data, inarray=True, inaxis=None):
    """ Key function to finalise a list of returned elements 
    if inarray is true return in a DataX if list has axes or in np.array otherwhise
    if inarray is a string look at the coresponging class with the _getClass_ method 
    and return the object in this class 
    inaxis keyword force force the returned data to have the inaxis axis for firs axis
    """
    if inarray is True:
        return self.inarray(data, axes=self._getAxis(inaxis))
    if isinstance(inarray, basestring):
        return self._getClass_(inarray)(data)    
    return data

def inarray(data, axes=None):        
    if axes and len(axes):       
        return dataxconcat(data, axes[0])        
    return np.asarray(data)


def isarray(data):
    return isinstance( data, np.ndarray)



    
class _DataSwitchCapabilities_(object):
    _on_data_default  = 0
    _add_data_capability        = classmethod(add_data_capability_as_switch)
    _add_scalar_data_capability = classmethod(add_scalar_data_capability_as_switch)
    _add_headerfunc             = classmethod(add_headerfunc_as_switch)

    _getAxis = getAxis
    
    finalise = finalise
    inarray = staticmethod(inarray)
    isarray = staticmethod(isarray)
    def getDataShape(self,*idx):
        """
        Return the DataShape -> obj.getData().shape 
        """
        if not len(self): return tuple()        
        return self[self._on_data_default].getDataShape(*idx) #idx for recarray 
    
    def getDataSize(self, *idx, **kwargs):
        """
        Return the DataSize on given axis -> obj.getDataSize(axis=)
        """
        if len(idx)>1:
            if "axis" in kwargs:
                raise KeyError("duplicate value for keyword axis")
            kwargs["axis"] = idx
            idx.pop(1)
        
        axis = kwargs.pop("axis", None)
        if len(kwargs):
            raise KeyError("getDataSize accept only one keyword argument axis=")
        # idx is for recarrays, will return an error if not
        return size_of_shape( self.getDataShape(*idx), self.getDataAxes(), axis=axis)
    
    def getDataAxes( self):
        if hasattr(self, "data_axes"): return self.data_axes
        if not len(self): return []
        if hasattr(self, "_on_data_default") and self._on_data_default is not None:
            return self[self._on_data_default].getDataAxes()        
        return self.axes+self[0].getDataAxes()
    
    newHeader     = key_decorator("newHeader", 0 , inarray=False)
    getAttr       = key_decorator("getAttr"  ,     inarray=False)
    callAttr      = key_decorator("callAttr" ,     inarray=False)   
    getHeader     = key_decorator("getHeader", inarray=False)
        
    getShape     = data_decorator("getShape")
    getData      = data_decorator("getData")
    getOffset    = data_decorator("getOffset")
    setOffset    = data_decorator("setOffset", inarray=False)
    delOffset    = data_decorator("delOffset", inarray=False)

    getScale     = data_decorator("getScale")
    setScale     = data_decorator("setScale", inarray=False)
    delScale     = data_decorator("delScale", inarray=False)

_DataSwitchCapabilities_._add_scalar_data_capability("data")


class _SwitchCapabilities_(_DataSwitchCapabilities_, _KeySwitchCapabilities_):
    pass

class Switch(list, _SwitchCapabilities_):
    """
    A Switch is a list of DataEntity (or other)
    it aims is to dispatch command to the right DataEntity. 
    For instance :  s.getImage() will call s[1].getImage() while 
                    s.getSpectrum() will call s[2].getSpectrum()
                    s.getKey("DIT")  call s[0].getKey("DIT") etc .... 
    
    """
    _on_default = 0    
    def __init__(self, lst, *args, **kwargs):
        axis = kwargs.pop("axis", None)
        list.__init__(self, lst,*args, **kwargs)
        self._subclass_list()
        if axis is not None: self.axes = [axis]
    
    def __getitem__(self,item):
        if isinstance(item, basestring):
            names  = [subs.name.upper() for subs in self]
            try:
                item = names.index(item.upper())
            except ValueError:
                raise ValueError("data entity '%s' not found"%item)
        return list.__getitem__( self, item)
    
    say = objsay    
    _getClass_ = staticmethod(getClass)
    
    @staticmethod
    def _subclass_entity(entity, subclasses, index=-1, parseclass=parseclass, getClass=getClass):
        subkey = entity.name if entity.name in subclasses else index if index in subclasses else entity.__class__ if entity.__class__ in subclasses else None
        if subkey in subclasses:
            subclass = subclasses[subkey]
            if isinstance(subclass, basestring):
                subclass = getClass(subclass)
            parseclass( entity, subclass)
            
    def _subclass_list(self):
        """ a Switch object can have rules to reclass the childs 
        This function is aimed to reclass the init list according 
        to the subclasses attribute
        """
        if not hasattr(self,"subclasses"): return 
        for index,entity in enumerate(self):
            self._subclass_entity(entity,  self.subclasses, index, getClass=self._getClass_)
    


class _KeyDataListCapabilities_(_KeySwitchCapabilities_):
    _on_key_default = None
    _add_key_capability         = classmethod(add_key_capability_as_list)
    _add_key_to_data_capability = classmethod(add_key_to_data_capability_as_list)
    _add_derived_key_capability = classmethod(add_derived_key_capability_as_list)
    getKey        = key_decorator("getKey")
    getKeyComment = key_decorator("getKeyComment")
    
class _DataDataListCapabilities_(_DataSwitchCapabilities_):
    _on_data_default  = None
    _add_data_capability        = classmethod(add_data_capability_as_list)
    _add_scalar_data_capability = classmethod(add_scalar_data_capability_as_list)
    _add_headerfunc             = classmethod(add_headerfunc_as_list)
    
    def finalise(self, data, inarray=True, inaxis=None):
        if inarray is True:
            return self.inarray( data, axes=self._getAxis(inaxis))
        if isinstance(inarray, basestring):
            return self._getClass_(inarray)(data)        
        return self.__class__(data)
    
    def getDataShape( self, *idx):
        if not len(self): return tuple()
        shapes = self.getShape(*idx)
        if not isinstance(shapes, np.ndarray):
            return shapes
        dshape = self[0].getDataShape(*idx)
        if isinstance( shapes, tuple):
            return shapes
        if (dshape != shapes).sum():
            raise Exception("Seems that individual data have not the same shape")
        return tuple( [len(self)]+list(dshape) )
    
        
    def getDataAxes( self):
        if hasattr(self, "data_axes"): return self.data_axes
        if not len(self): return self.axes
        return self.axes+self[0].getDataAxes()

    def order(self, method, *args, **kwargs):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        values = method(*args, **kwargs)
        indexes = np.argsort(values)
        return self.__class__ (  [self[i] for i in indexes] )
    
    def filter(self, method):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        return self.__class__ ( [fts for fts in self if method(fts)] )
    
    newHeader     = key_decorator("newHeader", 0, inarray=False)
    getAttr       = key_decorator("getAttr", inarray=False)
    callAttr      = key_decorator("callAttr", inarray=False)
    
  
    getHeader     = key_decorator("getHeader", inarray=False)

    getFilePath  =  key_decorator("getFilePath")
    getFileName  =  key_decorator("getFileName")
    getFileDirectory  =  key_decorator("getFileDirectory")
    getDirectory  =  key_decorator("getDirectory")
    
    getShape     = data_decorator("getShape")

    getData      = data_decorator("getData")
    
    getOffset    = data_decorator("getOffset")
    setOffset    = data_decorator("setOffset", inarray=False)
    delOffset    = data_decorator("delOffset", inarray=False)

    getScale     = data_decorator("getScale")
    setScale     = data_decorator("setScale", inarray=False)
    delScale     = data_decorator("delScale", inarray=False)

class _DataListCapabilities_(_DataDataListCapabilities_,_KeyDataListCapabilities_):
    _on_default       = None
    
    
class DataListMethods(_DataListCapabilities_):
    """
    High level Function for list of fitsfile cube or images
    """
    _on_default      = None
    _on_data_default = None
    _on_key_default  = None
    
    verbose = 1
    say = objsay
    _getClass_ = staticmethod(getClass)
    axes = ["list"]
        
    def idx(self, items):
        """
        getitem( lst, item)
        mimic fancy numpy index for detector lists 
        """
        if not isinstance( items, slice) and not hasattr( items, "__iter__"):
            return list.__getitem__(self, items)
         
        if isinstance( items, np.ndarray):
            items = np.arange( len(self))[ items].flat
        else:
            items = np.arange( len(self))[ np.r_[items] ].flat
        return self.__class__( [self[i] for i in items] )

    def iter(self, *args, **kwargs):
        return IterKey( self, *args, **kwargs)
    def iterkw(self, *args, **kwargs):
        return IterKwargs( self, *args, **kwargs)
    
#####
#   Add the key with and set function and the scalar Data
DataListMethods._add_scalar_data_capability("data")
DataListMethods._add_key_capability("key")

class DataList(list,DataListMethods):
    def __init__(self, lst, *args, **kwargs):
        axis     = kwargs.pop("axis", None)
        subclass = kwargs.pop("subclass", None)
        
        list.__init__(self, lst,*args, **kwargs)       
        if axis is not None: self.axes = [axis]
        if subclass:
            self._subclass_all(subclass)
    def _subclass_with(self, subclassfunc):
        for obj in self:
            subclassfunc(obj)
    def _subclass_all(self, subclass):
        for obj in self:
            obj.__class__ = subclass
            if hasattr(obj, "_subclass_list"):
                obj._subclass_list() 
    def __getitem__(self,items):
        return self.idx(items)



_update_class_dictionary_(__name__)

def __test__():
    global e,l,t
    e = DataEntity( np.random.random((20,100,120)), axes=["time","y","x"] )

    print "(100, 120) ->", e.getData( time_reduce=np.mean ).shape
    print "(3, 100,) ->", e.getData( time_idx=slice(0,3), x_idx=4).shape
    e.data[0,20,10] = 2
    kw = dict( x_idx=10, y_idx=20, time_idx=0 )
    print "2.0 -> ",e.getData(**kw)    
    e.setScale( 10)
    print "20.0 -> ",e.getData( **kw)
    e.setOffset( -5)
    print "-30.0 -> ",e.getData( **kw)
    e.delOffset()
    print "20.0 -> ",e.getData( **kw)
    
    e._add_data_capability( "test", lambda obj,*args,**kwargs: obj.getData(*args, **kwargs)*100, add_scalar=True)
    print "2000 ->", e.getMeanTest( **kw)
    print e.getMinTest( )

    e.setKey("KTEST", (30, "This is a test"))
    print "Key", e.getKey("KTEST"), e.getKeyComment("KTEST")
    e._add_key_capability( "toto", lambda obj,*args, **kwargs: str(obj.getKey("KTEST"))+" + ")
    print "'30 + ' -> ", e.getToto()
    
    print "##################################################"
    s = Switch( [e,e.__class__(e.data, axes=["time","y","x"])], axis="hdu")
    s._add_key_capability( "toto")
    print "max", s.getMaxData()
    print "men", s.getMeanData()
    print "(100,) -> ", s.getData( time_reduce=np.mean, x_idx=30).shape
    print "10 -> ", s.getScale()
    s.setScale(100)
    print "200.0 ->", s.getData(**kw), ", 2.0 ->", s[1].getData(**kw)
    s.delScale()    
    print "2.0 ->", s.getData(**kw)
    s.setScale(100)
    s.add_data_capability( "titi", lambda obj,*args, **kwargs: obj.getData(*args, **kwargs), on=1)
    print "200.0 ->", s.getData(**kw), ", 2.0 ->", s.getTiti(**kw)
    
    print "##################################################"

    l = DataList([ s, s, Switch(s, axis="hdu"), Switch(s, axis="hdu")], axis="expo")
    print DataX([200.]*4, ["expo"]), l.getData(**kw)
    print [30],  l.getKeySet("KTEST")
    l._add_key_capability( "toto")
    print DataX(["30 +"]*4, ["expo"]),l.getToto()
    print l.withKey( 30, "KTEST").getKey("KTEST")
    print l.withMinData( (0,400), **kw).getMinData(**kw)
    print l.withMinData( (0,199), **kw).getMinData(**kw)
    print l.withMinData( lambda obj:(0,400), **kw).getMinData(**kw)

    print "##################################################"
    t = TableDataEntity(np.array([(1.0, 2), (3.0, 4)], dtype=[('x', float), ('y', int)]), axes=["time"])
    setattr(t,"isOffsetable", lambda name: name=="x")
    print t.getData("x")
    print t.getShape("x")
    t.setOffset(10)
    print t.getData("x"), t.getData("y")
    
if __name__=="__main__":
    __test__()
