import re
import numpy as np
import os
from detector import config
if config.debug: print "carap.py"
from detector import detplots as plots
from detector import engine, bases,fits, ptc, keystable, baseplot

if config.debug:
    reload(plots)
    reload(keystable)
    reload(fits)
    reload(ptc)

getClass = engine.getClass_decorator( __name__, ["hdu", "hdulist", "fitslist"], fits.getClass)


##
# TODO  DEBUG
reload(bases)
reload(ptc)

######
# Misc function 
def getDirectory(dirName, rootdir=None):    
    if rootdir:
        rootdirs = rootdir.rstrip("/").split("/")
        dirs = dirName.split("/")
        if rootdirs == dirs[0:len(rootdirs)]:
            return "/".join(dirs[len(rootdirs):])
    return dirName


def autoOpen(filename, opener=fits.pf.open):
    """
    object = autoOpen(default_openeur(file_name))
    Automatlycaly assign the right obbject for a given pyfits hdulist 
    """
    fh =  opener(filename)
    
    if len(fh)>=5 and (fh[1].name == "M2" or fh[1].name=="SIGMA2"):
        if fh[1].header["NAXIS"]>2:
            return AllCombined(fh)
        return getClass("combined")(fh)
    
    if len(fh)==4 and fh[1].name == "SIGMA2":
        return getClass("combined2")(fh)
    if len(fh)==1 and fh[0].header['NAXIS'] >2:
        return getClass("cubeimage")(fh)
    if "IMAGING_DATA" in  [hdu.name for hdu in fh]:
        return TableImage(fh)
    return Image(fh)




class KeyWords(bases._KeyCubeCapabilities_):
    """ a set of function to return key keywords """
    def getDit(self):
        """ Return the DIT  [HIERRARH ESO DET DIT] """
        return self.getKey("DET DIT")
    
    def getPolar(self):
        """ return [HIERRARH ESO DET POLAR] if found 
        else try to gess with the file name (for old BETI files) 
        """ 
        if self.hasKey("DET POLAR"):
            return self.getKey("DET POLAR")
        # if no polar is found try to gess it with file name
        # (this is for old files on BETI)
        fname = self.getFileName()
        tc = re.compile( "P([0-9][0-9][0-9][0-9])[^0-9]" )
        f = tc.findall( fname)
        if not len (f):
            raise Exception("cannot find polar information in file name")
        return int(f[0])
    def getIllumination(self):
        """ If any return the illumination value """
        if self.hasKey("ILLUMIN"): return self.getKey("ILLUMIN")
        return 0.0
    def getMjdObs(self):
        """ return the MjdObs day of observation """
        return self.getKey("MJD-OBS")
    def getConfig(self):
        """ return a list containing pairs of key/value for instrument config """ 
        return ([("DIT",  self.getDit()  ),
                ("POLAR",self.getPolar()),
                ("DISP" ,self.getDisp() )         
            ])
    def getDisp(self, **kwargs):
        """ Return the dispersion name INS OPTI2 NAME"""
        return self.getKey("INS OPTI2 NAME ")
    
    
 
    
keywords_keys = [k[3:].lower() for k in  KeyWords.__dict__ if k[0:3]=="get"]
class KeyWordsSwitch(bases._KeyCubeSwitchCapabilities_):
    pass
class KeyWordsList(bases._KeyCubeListCapabilities_):
    pass

for k in keywords_keys:
    if config.add_key_data:
        KeyWords._add_key_to_data_capability(k)
        KeyWordsSwitch._add_key_to_data_capability(k)
        KeyWordsList._add_key_to_data_capability(k)
    
    KeyWordsSwitch._add_key_capability(k)
    KeyWordsList._add_key_capability(k)


def withOutput(obj, output):
    out =  obj.__class__( obj.getData( section=np.s_[:,output*40:(output+1)*40]),
                          obj.getHeader().copy()
                      )
    out.setKey("OUTPUT", output)
    return out
def getOutputSet(obj):
    return range(8)
def iterOutput(obj):
    return engine.IterKey(self, "output")
    
    
class CombinedSwitchCapabilities(bases._DataCombinedSwitchCapabilities_):
    def getCubePath(self, directory=None):
        """ Return the path to the cube file if any 
        The file name is taken from the header from the CUBENAME keyword.
        The optional directory is the directory default is the same than self
        """
        cube_name = self.getKey("CUBENAME")
        
        if not cube_name:
            raise NotEnoughData("There is not cube attached to this combined file")
        directory = directory or self.getKey("CUBEDIR") or self.getDirectory()
        
        path = directory+"/"+cube_name if len(directory) else cube_name
        return path
    def Cube(self, directory=None):
        """ Return the Cube fits file opened if any linked """
        path = self.getCubePath(directory)
        if not os.path.exists(path):
            raise ValueError("Cannot find file '%s'"%path)
        return Cube.open(path)
    def getCube(self, **kwargs):
        """ Return the data Cube if any """
        return self.Cube().getData(**kwargs)
    def getNumber(self, **kwargs):
        """ Return a array of number of combined images 
        look first at hdu "NUMBER" than "N" and if not 
        built it from the keyword DET NDIT 
        """
        names = [t.name for t in self]
        if "NUMBER" in names:
            return self["NUMBER"].getData(**kwargs)
        if "N" in names:
            return self["N"].getData(**kwargs)
        
        return self.getKey2data( "DET NDIT", **kwargs)

    
class CubeSwitchCapabilities(bases._DataCubeSwitchCapabilities_):
  def Combine(self, **kwargs):
      signaldata = self.getSignal(**kwargs)
      signal =  self._getClass_("signalprimary")( signaldata, self.getSignalHeader())
      
      moments = self.getMoment([2,3,4], meandata=signaldata, **kwargs)
      mhdu = [ self._getClass_("sigma2")(moments[0],self.getSigma2Header()),
               self._getClass_("sigma3")(moments[1],self.getSigma3Header()),
               self._getClass_("sigma4")(moments[2],self.getSigma4Header())
           ]
      
      signal.setKey("CUBENAME", self.getFileName())
      signal.setKey("CUBEDIR",  self.getFileDirectory())
      return Combined(  [signal]+mhdu)
        
    
class ImagesCapabilities(bases._DataCubeListCapabilities_,ptc._PtcLinearityCapabilities_):    
    def make_keys_table(self, rules=config.allcombined_keys_as_table, axes=None, name="KEYWORDS"):
        if axes is None and hasattr(self,"axes"): axes = self.axes
        return KeysTable.make(self, rules, axes=axes, name=name)
    def make_combined_table(self, kwargs=None, args=None, datarules=config.combined_data_as_table,
                            keyrules=config.allcombined_keys_as_table,
                            axes=None, name="COMBINED"):
        rules = list(keyrules)
        kwargs = kwargs or {}
        args = args or []
        for name,func in datarules:
            rules.append( {"name":name,"array":(func,args,kwargs)})
        
        return CombinedTable.make( self, rules, axes=axes, name=name)

class Signal(fits.getClass("signal"), KeyWords):
    _getClass_ = staticmethod(getClass)
    
class SignalPrimary(fits.getClass("signalprimary", True), KeyWords):
    _getClass_ = staticmethod(getClass)
    

    
class CubeHDU(fits.getClass("cube"), KeyWords):
    _getClass_ = staticmethod(getClass)
class CubePrimaryHDU(fits.getClass("cube", True), KeyWords):
    _getClass_ = staticmethod(getClass)
    
    
class KeysTable(keystable.KeysTableHDU, KeyWords):
    _getClass_ = staticmethod(getClass)
    def getConfig(self):
        """ return a list containing pairs of key/value for instrument config """
        d  = self.getDit()
        p  = self.getPolar()
        di = self.getDisp()
        return self._return_array_data([ [("DIT",d[i]),("POLAR",p[i]),("DISP",di[i])] for i in range(len(d))], {})
    def getFilePath(self):
        return self.getKey("FILEPATH")
    

def intable_decorator(data_name):
    def tmp_decorator(self, *args,**kwargs):
        return self.getData(data_name, *args,**kwargs) 
    return tmp_decorator

class TableRecord(bases.DataEntity, CombinedSwitchCapabilities, KeyWordsSwitch):
    subclasses = {
        "signal":Signal,        
        "sigma2":fits.Sigma2HDU,
        "sigma3":fits.Sigma3HDU,
        "sigma4":fits.Sigma4HDU,        
        "number":fits.NumberHDU,
    }
    def __getitem__(self, item):
        if isinstance( item , int):
            item = ["signal", "sigma2", "sigma3", "sigma4", "number"][item]
        item  = item.lower()
        
        out = self.subclasses[item](self.data[item])            
        if item == "signal":
            h = out.getHeader()
            for ktable,kheader in config.table_to_header:
                try:
                    h[kheader] = self.data[ktable]
                except:
                    pass
        return out
    def toCombined(self):
        items = ["signal", "sigma2", "sigma3", "sigma4", "number"]
        return self._getClass_("combined")( [self[item] for item in items] )
    
class CombinedTable(keystable.KeysTableHDU,
                    ImagesCapabilities,engine.DataListMethods, KeyWordsList):    
    _getClass_ = staticmethod(getClass)
    def toImages(self):        
        return self._getClass_("images")( [c.toCombined() for c in self] )
    def getFilePath(self):
        return self.getKey("FILEPATH")
    getSignal = intable_decorator("signal")
    getSigma1 = intable_decorator("sigma1")
    getSigma2 = intable_decorator("sigma2")
    getSigma3 = intable_decorator("sigma3")
    getNumber = intable_decorator("number")
    def getDataAxes(self, name=None):
        if name is None: return self.axes
        if name in ["signal", "sigma1", "sigma2", "sigma3", "sigma4", "number"]:
            return self.axes+["y","x"]
        return self.axes
    
    def isOffsetable(self, name):
        return name.lower()=="signal"
    def __len__(self):
        return self.header["NAXIS2"]
    
    def __getitem__(self, items):        
        if not isinstance( items, slice) and not hasattr( items, "__iter__"):
            return TableRecord(self.data[items])
        
        return self.__class__( self.data[items], self.header.copy())
    
        if not isinstance( items, slice) and not hasattr( items, "__iter__"):
            return list.__getitem__(self, items)
         
        if isinstance( items, np.ndarray):
            items = np.arange( len(self))[ items].flat
        else:
            items = np.arange( len(self))[ np.r_[items] ].flat
        return self.__class__( [self[i] for i in items] )

    
    def order(self, method, *args, **kwargs):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        values = method(*args, **kwargs)
        indexes = np.argsort(values)
        self.__class__ ( self.data[indexes], self.header.copy())
        return self.__class__ (  [self[i] for i in indexes] )
    
    def filter(self, method):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        return self._getClass_("images") ( [fts for fts in self if method(fts)] )

    
class Combined(fits.getClass("hdulist"), KeyWordsSwitch, CombinedSwitchCapabilities):
    _getClass_ = staticmethod(getClass)
    subclasses = {
        "SIGNAL":SignalPrimary,
        0:SignalPrimary, 
        "SIGMA2":fits.Sigma2HDU,
        "SIGMA3":fits.Sigma3HDU,
        "SIGMA4":fits.Sigma4HDU,        
        "NUMBER":fits.NumberHDU,
        "M2":fits.Sigma2HDU, # legacy of old hdu names 
        "M3":fits.Sigma3HDU,
        "M4":fits.Sigma4HDU,        
        "N":fits.NumberHDU
    }

class Cube(fits.getClass("hdulist"), KeyWordsSwitch, CubeSwitchCapabilities):
    _getClass_ = staticmethod(getClass)
    subclasses = {
        0:CubePrimaryHDU
    }
    
class Images(bases.getClass("cubelist"), ImagesCapabilities, KeyWordsList, baseplot.Images):
    _getClass_ = staticmethod(getClass)
    axes = ["expo"]
    

def set_decorator(classname, method_name, getset_name):
    def tmp_polar_decorator(self,**kwargs):
        return self._getClass_(classname)(
            [getattr(self.withPolar(p), method_name)(**kwargs) for p in getattr(self,getset_name)()]
        )
    return tmp_polar_decorator


class Carac(Images, baseplot.Carac):
    def withPolar(self, polar):
        lst = Images.withPolar(self, polar)
        if len(lst.getPolarSet())<2:
            return Images(lst)
        return lst
    def getPtc(self, **kwargs):
        """ Return the ptcHDUList for each polar of the set """
        return self._getClass_("ptcdatalist")(
            [self.withPolar(p).getPtc(**kwargs) for p in self.getPolarSet()],
            axis="polar"
        )
    def getLinearity(self, **kwargs):
        """ Return the ptcHDUList for each polar of the set """
        return self._getClass_("linearitydatalist")(
            [self.withPolar(p).getLinearity(**kwargs) for p in self.getPolarSet()],
            axis="polar"
        )
    def getDetCarac(self, **kwargs):
        return self._getClass_("ptclinearitydatalist")(
            [self.withPolar(p).getDetCarac(**kwargs) for p in self.getPolarSet()],
            axis="polar"
        )
    def getMinDitSigma(self, **kwargs):
        out = []
        for p in self.getPolarSet():
            out += self.withPolar(p).withDit(np.min)
        
        return self._getClass_("carac")(
            out,
            axis="polar"
        ).getSigma(**kwargs)
        

engine._update_class_dictionary_(__name__)


def __test__():
    global fls
    try:
        fls = Images(fls)
        fls._subclass_all(Combined)
    except:
        from detector import fitsopener as opener
        fls = Images(opener.open_glob("/Users/guieu/DETDATA/274-2014-PTC-P7100/PIONIER*_0.05*.fits", opener=fits.pf.open))
        fls._subclass_all(Combined)
    #print fls.getPtc().getGain().shape

if __name__ is "__main__":
    __test__()
    
