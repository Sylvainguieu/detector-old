import numpy as np
from detector import fits
from detector import base
from datax import DataX, dataxconcat
import os
pf = fits.pf

filelist_decorator = base.list_decorator


##
# a list of keys that will be converted to a binary table 
allcombined_keys_as_table = fits.allcombined_keys_as_table


def dic2header(d):
    """
    dic2header(d)
    Return a pyfits Header object from a directory 
    """
    return pf.Header( cards=[ pf.Card(k,v) for k,v in d.iteritems()] )



class FitsList(list):
    axes = ["file"]  #well only the first one count     
    def __getslice__(self,*args):
        return self.__class__(list.__getslice__( self, *args))
    
    def __getitem__(self, items):
        return base.getitem(self, items)

    @classmethod
    def new(cls, *args, **kwargs):
        return cls( *args, **kwargs)
    
_makeHDUDecorator = fits._makeHDUDecorator

def _makeHDUDecoratorList(get_function, cls, header_name, cp_header=False):
    def tmpMakeHDU(self, *args, **kwargs):
        getf  = getattr(self, get_function)
        primary = kwargs.pop("primary",False)
        if cp_header and isinstance(self, (pf.ImageHDU, pf.PrimaryHDU)):
            hdu = cls[primary]( getf(**kwargs), self.header.copy() )
        else:
            hdu = cls[primary]( getf(**kwargs) )
        hdu.name = header_name
        hdu.header["COMMENT"] = ":FILES:"+",".join(self.getFileName())+":FILES:"
        return hdu
    return tmpMakeHDU


class _FileListFunction_(base._ListList_):
    
    def toAllCombined(self, keys_as_table= allcombined_keys_as_table,sign=None, **kwargs):                 
        hlist = [
            pf.Card("dir", self.getDirectory(), comment="root directory of combined file")
        ]
        ####
        ## Add the key in the first HDU but replace value by 'seetable'
        ## 
        default_comment = "look at the table hdu for this value"
        default_header  = self[0].getHeader()
        for k in keys_as_table:
            if len(k)==2:
                comment = default_header.comments[k[0]] if k[0] in default_header else default_comment
            else: comment = default_comment
            hlist.append(pf.Card(k[0], "seetable", comment=comment))
                                
            
        h = pf.Header(hlist+[pf.Card(k[0], "seetable", comment="look at the table hdu for this value") for k in keys_as_table ] )

        columns = []
        for k in keys_as_table:
            if len(k)==2:
                # This a keyword use the getKey to getit
                a = self.getKey(k[0])
            else:
                # The array is returned from an atribute name
                a = getattr( self, k[2])()
            columns.append(
                pf.Column(name=k[0], array=a, format=k[1] )
            )        
        cd = pf.ColDefs(columns)
        fh = pf.HDUList([fits.SignalCubePrimaryHDU(self.getSignalData(scale=sign, **kwargs), h), 
                         fits.M2CubeHDU( self.getM2Data(  **kwargs), name="M2"), 
                         fits.M3CubeHDU( self.getM3Data(scale=sign,  **kwargs), name="M3"), 
                         fits.M4CubeHDU(self.getM4Data(  **kwargs), name="M4"), 
                         fits.NCubeHDU(self.getNData( **kwargs), name="N") ,
                         pf.new_table(cd)                           
                     ])     
        fh[0].update_ext_name("signal")
        fh[-1].update_ext_name("keywords")
        fh[-1].__class__ = fits.KeysBinTableHDU
        
        return fits.AllCombinedImages(fh)
    
    getGainHDU    = _makeHDUDecoratorList("getGain",   [fits.GainHDU,   fits.GainPrimaryHDU], "gain")
    getNoise2HDU  = _makeHDUDecoratorList("getNoise2", [fits.Noise2HDU, fits.PrimaryHDU], "noise2")
    getNoiseHDU   = _makeHDUDecoratorList("getNoise",  [fits.NoiseHDU,  fits.PrimaryHDU], "noise")
    getENoiseHDU  = _makeHDUDecoratorList("getENoise", [fits.NoiseHDU,  fits.PrimaryHDU], "enoise")
        
    getFluxHDU = _makeHDUDecoratorList("getFlux", [fits.FluxHDU, fits.FluxPrimaryHDU], "flux")
    getBiasHDU = _makeHDUDecoratorList("getBias", [fits.BiasHDU, fits.PrimaryHDU], "bias")



class ImageDerived(object):
    pass

    
class Images(FitsList, ImageDerived,  base._Images_, _FileListFunction_):    
    pass

class CombinedImages(FitsList,ImageDerived, base._CombinedImages_, _FileListFunction_):
    axes = base._CombinedImages_.axes
    def getDirectory(self, rootdir=None):
         return self[0].getDirectory(rootdir=None)
    def getMinDitM2(self, withheader=False, **kwargs):
        dit = self.getDit() 
        minditimg = self[np.argmin(dit)]        
        data = minditimg.getM2(**kwargs)                
        if withheader:            
              return data, minditimg.header.copy()
        return data
    
class CombinedLongDark(Images, base._CombinedImages_):   
    getDirectory = CombinedImages.getDirectory
    getMinDitM2  = CombinedImages.getMinDitM2
