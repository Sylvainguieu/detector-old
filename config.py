# verbose level 
verbose = 1
# debug mode will all reload modules automaticaly 
debug = True

# The linear fit method are vectorialized (fast) or not (very slow)
vectorialized_fit = True

# the class dictionary will contain a dictionary of DataEntity Switch and DataList sub-classes
try:
    class_dictionary
except:
    class_dictionary = {"__all__":{}}
    
# if True automaticaly add the scalar viewn of data: e.g. getMinSignal, getMeanSignal, etc
add_scalar = True
# to automaticaly 
add_key_data  = True
# inplace keywords is True do not overwrite already existing methods
inplace = False
# list of reduce function for automatic add_scalar option 
import numpy as np
reduce_to_scalar = {"min":np.min,"max":np.max,"mean":np.mean,"median":np.median,"std":np.std}




NOTICE     = "NOTICE"
WARNING    = "WARNING"

WIN  = "win"
OUTPUT = "output"
EXPO = "expo"
TIME = "time"
TELESCOPE = "telescope"
WINDOW = "window"
X    = "x"
Y    = "y"
DISP = "disp"
SAMPIX = "sampix"
FILE = "file"
HDU  = "hdu"
POLAR = "polar"
DIT = "dit"
ILLUMINATION = "illumination"
CONFIG       = "config"

##
# axes considered has pixels like
# used for the freduce keyword 
pixelaxes = [X,Y, WIN, DISP, SAMPIX,OUTPUT]
timeaxes  = [TIME]

##
# All these keys as considered as axes
# so for instance expo will be a short cut for expo_idx
idxkeys = pixelaxes+timeaxes+[FILE,HDU,EXPO,SAMPIX,POLAR]

###
# Detector related 
#
noutput    = 8 # number of output
outputaxis = "x" # axis of the output slicing
outputsize = 40 # size of each outputs

####
# Iterator object can cycle over color,marker, ...
#
color_cycle  = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
marker_cycle = [',', '+', '-', '.', 'o', '*']

####
# Some constants specific of the Manip
# 
NSHUTTERS = 4
DARKSHUTTERS  = 0b0000
OPENSHUTTERS  = 0b1111
KAPA1SHUTTERS = 0b1000 
KAPA2SHUTTERS = 0b0100 
KAPA3SHUTTERS = 0b0010 
KAPA4SHUTTERS = 0b0001



##
# a list of keys that will be converted into a binary table
# should be a tuple of 5 arguments:
#  - the key name in the table
#  - its format
#  - a 3 len tuple containing the method string, a list of argument, a dict of kwargs
#    
## 
def _tkey(name=None, array=None, **kwargs):
    if array is None:
        if name is None:
            raise ValueError("array and name are None")
        array = ("getKey",[name], {})
    return dict (dict(name=name,array=array), **kwargs)
##
#
allcombined_keys_as_table = [_tkey("DATE-OBS"),
                             _tkey("MJD-OBS" ),
                             _tkey("DET DIT",  "getDit"),
                             _tkey("DET NDIT" ),
                             _tkey("DET NDITSKIP"),
                             _tkey("DET POLAR","getPolar"),
                             _tkey("ILLUMINATION",  "getIllumination"),
                             _tkey("INS SENS6 STAT"),
                             _tkey("INS SENS8 STAT"),
                             _tkey("FILEPATH","getFilePath"),
                             _tkey("INS SHUT1 ST" ),
                             _tkey("INS SHUT2 ST" ),
                             _tkey("INS SHUT3 ST" ),
                             _tkey("INS SHUT4 ST" ),
                             _tkey("INS OPTI2 NAME")
                         ]
HESO= "HIERARCH ESO "
table_to_header = [["DATE-OBS"]*2, ["MJD-OBS"]*2,
                   ("DET DIT",HESO+"DET DIT"),
                   ("DET DIT",HESO+"DET POLAR"),
                   ("ILLUMINATION","ILLUMINATION"),
                   ("INS OPTI2 NAME",HESO+"INS OPTI2 NAME"),
                   ("INS SHUT1 ST",HESO+"INS SHUT1 ST"),
                   ("INS SHUT2 ST",HESO+"INS SHUT2 ST"),
                   ("INS SHUT3 ST",HESO+"INS SHUT3 ST"),
                   ("INS SHUT4 ST",HESO+"INS SHUT4 ST")
               ]
combined_data_as_table = [ ("signal","getSignal"),
                           ("sigma2","getSigma2"),
                           ("sigma3","getSigma3"),
                           ("sigma4","getSigma4"),
                           ("number","getNumber") ]
