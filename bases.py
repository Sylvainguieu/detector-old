from detector import config
if config.debug : print "bases.py"

import numpy as np
from datax import DataX, dataxconcat, size, isdatax
import os
import scipy.ndimage as ndim
import re

from detector import baseplot,detplots
from detector import computing, engine

if config.debug:
    #from detector import execute
    reload(computing)
    reload(engine)
    #reload(execute)
    
from detector.engine import DataEntity,TableDataEntity,Switch,DataList,data_decorator,header_decorator,reduce_decorator, key_decorator,encaps_decorator, _KeyEntityCapabilities_, _DataEntityCapabilities_,_DataSwitchCapabilities_, _KeySwitchCapabilities_,  _KeyDataListCapabilities_, _DataDataListCapabilities_, _update_class_dictionary_, getClass,getClass_in_module, say
from detector.execute import _ExecuteCapabilities_



PIXELAXESALIAS = ["freduce"]
REDUCESUFIX = ""
MEANKWARGS   = {k+REDUCESUFIX:np.mean   for k in PIXELAXESALIAS}
MEDIANKWARGS = {k+REDUCESUFIX:np.median for k in PIXELAXESALIAS}
STDKWARGS    = {k+REDUCESUFIX:np.std    for k in PIXELAXESALIAS}


        
def defaultMeanTime(kwargs,say):
    for k in config.timeaxes:
        if "%s_reduce"%k in TIMEAXES and kwargs["%s_reduce"%k]!=np.mean:
            say( "%s_reduce replaced by np.mean", vtype=config.NOTICE)
        kwargs["%s_reduce"%k] = np.mean
                 
def defaultStdTime(kwargs,say):
    for k in config.timeaxes:
        if "%s_reduce"%k in TIMEAXES and kwargs["%s_reduce"%k]!=np.std:
            say( "%s_reduce replaced by np.std", vtype=config.NOTICE)
        kwargs["%s_reduce"%k] = np.std

def _updateReduceKwargs(kwargs):
    #
    #  for backward compatibility 
    #  freduce is equivalent to xreduce+yreduce
    if "freduce" in  kwargs:
        freduce = kwargs.pop("freduce")
        for k in config.pixelaxes:
            kwargs.setdefault(k+"_reduce",freduce)
            

def _updateSectionKwargs(kwargs):
    KEYWORDCONFLICT = "conflict between '%s' and '%s' keyword, '%s' is retained"
    
    for k in config.idxkeys:
        if k in kwargs:
            kr = k+"_idx"
            if kr in kwargs:
                say(config.KEYWORDCONFLICT%(k,kr,k), vtype=config.WARNING)
            kwargs[kr] = kwargs.pop(k)
    
    if not "section" in kwargs: return              
    section = kwargs.pop("section")
    if section is None: return
    if isinstance( section, slice):
        if config.Y+"_idx" in kwargs:
                say(KEYWORDCONFLICT%("section",config.Y+"_idx","section"), vtype=config.WARNING)
        kwargs[config.Y+"_idx"] = section
        return 
    else:
        y_idx, x_idx = section
        if config.X+"_idx" in kwargs:
            say(KEYWORDCONFLICT%("section",config.X+"_idx","section[1]"), vtype=config.WARNING) 
        kwargs[config.X+"_idx"] = x_idx
        
        if config.Y+"_idx" in kwargs:
            say(KEYWORDCONFLICT%("section",config.Y+"_idx","section[0]"), vtype=config.WARNING)
        kwargs[config.Y+"_idx"] = y_idx

def _updateKwargs(kwargs):
    _updateReduceKwargs (kwargs)
    _updateSectionKwargs(kwargs)

#############################
# Apply the _updateKwargs and getClass to DataEntity as
DataEntity._updateKwargs = staticmethod(_updateKwargs)
DataEntity._getClass_ = staticmethod(getClass)
#############################
class NotEnoughData(Exception):
    """ When cannot found enough data to fit, plot, .... """
    pass


########################################################
#
# Generic functions
#
########################################################
def getData(self,**kwargs):
    return self.getData(**kwargs)
def getHeader(self, kw=None):
    return self.getHeader(kw)
def getHeaderCopy(self, kw=None):
    return self.getHeader(kw, copy=True)
def newHeader(self, kw=None):
    return self.newHeader()
########################################################
#
# Function for 2d Images 
#
########################################################
def getYx(self, physical=True, **kwargs):
    """ 
    return a grid of pixel coordinates 
    axes "y" and "x" must be present in the DataEntity
    """
    shapes = self.getDataShape()
    if config.X in self.axes and config.Y in self.axes:
        xi = self.axes.index(config.X)
        yi = self.axes.index(config.Y)
    elif len(shapes)==2:
        yi,xi = 0,1
    else:
        raise ValueError("Need the axes name 'y' and 'x' if the data is not an image")
    
    if yi<xi:
        y,x = np.mgrid[0:shapes[yi],0:shapes[xi]]
    else:
        x,y = np.mgrid[0:shapes[yi],0:shapes[xi]]

    
    output= self.getOutput()
    if output and physical:
        if config.outputaxis == config.X:
            x += output*config.outputsize
        elif config.outputaxis == config.Y:
            y += output*config.outputsize
    
    y = self._return_array_data( y, kwargs, axes=[config.Y,config.X] )
    x = self._return_array_data( x, kwargs, axes=[config.Y,config.X] )
    return y,x 

def getX(self, **kwargs):
    return self.getYx(**kwargs)[1]
def getY(self, **kwargs):
    return self.getYx(**kwargs)[0]
def getImage(self, **kwargs):
    shape = self.getShape()
    if len(shape)!=2:
        raise Exception("Not an image")                                      
    return self.getData(**kwargs)

######################################################################
#
#  A set of function to compute, image, std, moments from a cube 
#
######################################################################


def getSignal_cube(self,**kwargs):
    """ Average the first axis of the cube and return data """
    return self.getData(**kwargs).mean(axis=self.axes[0])
def getMoment_cube(self, moment, meandata=None, **kwargs):
        """ Compute the moment on the last dimention of the cube """
        return computing.moment(
            self.getData(**kwargs), moment,
            meandata=meandata, 
            axis=self.axes[0]
        )
def getSignalerror_cube(self, **kwargs):
    """ Return the Signal Error bar of a cube of  images 
        std / sqrt(N) 
        std is the standart deviation and N the size of the cube first dimension
    """
    shapes = self.getDataShape()
    return self.getData(**kwargs).std(axis=self.axes[0]) / np.sqrt(shapes[0])


def getSigma_cube(self,**kwargs):
    return self.getData(**kwargs).std(axis=self.axes[0])

def getNumber_cube(self, **kwargs):
    """ Return an image with a unique number: the axis len of the first axis dimension """ 
    shape = self.getDataShape()
    N    =  np.zeros(shape[1:], dtype=int)
    N[:] = shape[0]
    return self._return_array_data(N, kwargs, axes=self.axes[1:])   

def _remove_time_reduce(kwargs):
    """ Remove all the {key}_reduce keyword in kwargs with key inside config.timeaxes """
    for k in config.timeaxes:
        kwargs.pop(k,None)
        
def _get_time_axes(axes):
    """ Return the list of time axes contained in axes 
    raise a key error if none are found 
    """
    timeaxes = [ax for ax in axes if ax in config.timeaxes]
    if not len(timeaxes):
        raise KeyError("Cannot find time axis, looking for %s in %s"%(config.timeaxes, axes))
    return timeaxes

def getSignal_time(self,**kwargs):
    """ Average the time axes of the cube and return data """
    _remove_time_reduce(kwargs)
    timeaxes = _get_time_axes(self.axes)
    return self.getData(**kwargs).mean(axis=timeaxes)

def getSignalerror_time(self, **kwargs):
    """ Return the Signal Error bar of a cube on the "time" axes  
        std / sqrt(N) 
        std is the standart deviation and the number of points on time axes
    """
    _remove_time_reduce(kwargs)
    timeaxes = _get_time_axes(self.axes)
    return self.getData(**kwargs).std(axis=timeaxes)/ np.sqrt(self.getSize(axis=timeaxes))

def getSignalvariance(self, **kwargs):
    return self.getSignalerror(**kwargs)**2

def getNumber_time(self, **kwargs):
    """ Return an image with a unique number: the axis len of the first axis dimension """
    _remove_time_reduce(kwargs)
    timeaxes = _get_time_axes(self.axes)
    
    shape = self.getShape()
    axes  = self.getDataAxes()
    shape = self.getDataShape()
    notime_shape = [ sh for ax,sh in zip(axes, shape ) if ax not in timeaxes]
    notime_axes  = [ ax for ax,sh in zip(axes, shape ) if ax not in timeaxes]
    
    N    = np.zeros(notime_shape , dtype=int)
    N[:] = self.getDataSize(axis=timeaxes)
    
    return self._return_array_data(N, kwargs, axes=notime_axes)  

def getMoment_time(self, moment, **kwargs):
        """ Compute the moment on the last dimention of the cube """
        _remove_time_reduce(kwargs)
        timeaxes = _get_time_axes(self.axes)
        return computing.moment(
            self.getData(**kwargs), moment,
            axis=timeaxes
        )
def getSigma_time(self,**kwargs):
    """ Return the std on the first axis """
    _remove_time_reduce(kwargs)
    timeaxes = _get_time_axes(self.axes)
    return self.getData(**kwargs).std(axis=timeaxes)

def getSigma2(self, **kwargs):
    """ return sigma2, the moment of order 2 """
    return self.getMoment(2, **kwargs)

def getSigma3(self, **kwargs):
    """ return sigma2, the moment of order 2 """
    return self.getMoment(3, **kwargs)

def getSigma4(self, **kwargs):
    """ return sigma2, the moment of order 2 """
    return self.getMoment(4, **kwargs)

def _update_number_kwargs_for_error(Nkwargs):
    for k in Nkwargs:
        if k[-6:]=="reduce":
            if Nkwargs[k] in [np.mean, np.median]:
                Nkwargs[k] = np.sum
            else: raise ValueError("computing error: wrong reduce function in %k expecting mean or median to comvert to sum got %s"%(k,Nkwargs[k]))        

    
def getSigma2error(self, **kwargs):
    """ Return the error on the moment of order 2 (variance)
    to function correctly the object must have the following method:
    getSigma2 -> Moment of order 2
    getSigma4 -> Moment of order 4
    getNumber -> Number of points used to compute moment            
    """
    Nkwargs = kwargs.copy()
    _update_number_kwargs_for_error(Nkwargs)
    return computing.variance_error( self.getSigma2(**kwargs),
                                     self.getSigma4(**kwargs),
                                     self.getNumber(**Nkwargs)
                                 )
def getSigma2variance(self, **kwargs):
    return self.getSigma2error(**kwargs)**2

def getSigma2_fromsigma(self, **kwargs):
    """ return the moment of order two as square of sigma """
    sigma = self.getSigma(**kwargs)
    return sigma*sigma
def getSigma_fromsigma2(self, **kwargs):
    """ return sigma from the moment of order 2 """
    sigma2 = self.getSigma2(**kwargs)
    return np.sqrt(sigma2)
    
def withOutput(obj, output):
    sls = list(np.s_[:,:])
    if hasattr(obj,"axes") and obj.axes:
        sls[obj.axes.index(config.outputaxis)] = slice(output*config.outputsize,
                                                       (output+1)*config.outputsize)
    else:
        sls[config.outputaxis] = slice(output*config.outputsize,
                                       (output+1)*config.outputsize)
    sls = tuple(sls)
    out =  obj.__class__( obj.getData( section=sls),
                          obj.getHeader().copy()
                      )
    out.setKey("OUTPUT", output)
    return out
def getOutputSet(obj):
    return range(config.noutput)

def iterOutput(obj, *args, **kwargs):
    return engine.IterKey(self,"output",*args, **kwargs)


class _KeyImageCapabilities_(_KeyEntityCapabilities_):
    pass

class _DataImageCapabilities_(_DataEntityCapabilities_, _ExecuteCapabilities_):
    getYx     = getYx
    getX      = getX
    getY      = getY
    getOutputSet = getOutputSet
    withOutput   = withOutput
    iterOutput   = iterOutput
    def getOutput(self):
        return self.getKey("OUTPUT")
    
    def getData(self, **kwargs):
        pixel = kwargs.pop("pixel",None)
        if pixel is not None:
            y, x = self.getYx(physical=False, **kwargs)
            kwargs["y_idx"] = y.flat[pixel]
            kwargs["x_idx"] = x.flat[pixel]
            # remove any section keywords if needed
            kwargs.pop("section", None)
        return _DataEntityCapabilities_.getData(self, **kwargs)
            
class _DataDerivedCubeCapabilities_(_DataEntityCapabilities_):
    getSigma2 = getSigma2
    getSigma2Header = newHeader
    getSigma3 = getSigma3
    getSigma3Header = newHeader
    getSigma4 = getSigma4
    getSigma4Header = newHeader
    getSigma2error = getSigma2error
    getSigma2variance = getSigma2variance
    getImage  = getImage
    
image_data_keys = ["image","x","y","yx", "output"]
cube_data_keys  = ["signal","moment","sigma","sigma2", "sigma3", "sigma4", "number",
                   "signalerror", "sigma2error", "signalvariance", "sigma2variance"
               ]

####
# For method wich possed their class add them here
# class_data_names[method_name] = class_name
class_data_names = {k:k for k in cube_data_keys+["image"]}
class_data_names.pop("moment",None)

header_functions = {k:getHeaderCopy for k in class_data_names}

for k in  image_data_keys:
    _DataImageCapabilities_._add_scalar_data_capability(k)
for k in  cube_data_keys:
    _DataDerivedCubeCapabilities_._add_scalar_data_capability(k)

class _KeyCubeCapabilities_(_KeyEntityCapabilities_):
    pass
    
class _DataCubeCapabilities_(_DataDerivedCubeCapabilities_):
    getSignal = getSignal_cube
    getSignalHeader = getHeaderCopy
    getImage  = getSignal_cube
    getMoment = getMoment_cube
    getSigma  = getSigma_cube
    getSignalerror = getSignalerror_cube
    getSignalvariance = getSignalvariance    
    getNumber = getNumber_cube
    getNumberHeader = newHeader
    
class _DataCubeTimeCapabilities_(_DataDerivedCubeCapabilities_):   
    getSignal       = getSignal_time
    getSignalHeader = getHeaderCopy
    getImage  = getSignal_time
    getMoment = getMoment_time
    getSigma  = getSigma_time
    getSignalerror = getSignalerror_time
    getSignalvariance = getSignalvariance
    getNumber = getNumber_time
    getNumberHeader = newHeader
    
class CubeEntity(DataEntity,_DataCubeCapabilities_, _KeyCubeCapabilities_, _DataImageCapabilities_, baseplot.CubeImage):
    axes = ["time","y","x"]
    
class CubeTimeEntity(DataEntity,_DataCubeTimeCapabilities_, _KeyCubeCapabilities_, _DataImageCapabilities_, baseplot.CubeImage):
    axes = ["time","y","x"]
          
class ImageEntity(DataEntity,_DataImageCapabilities_,_KeyImageCapabilities_,baseplot.Image):
    axes = ["y","x"]
    
class SignalEntity(ImageEntity):
    """ Image of the averaged signal """
    _name = "SIGNAL"
SignalEntity._add_data_capability("signal",getData,getHeaderCopy,
                            clsname=class_data_names.get("signal",None),
                            add_scalar=True)

class SigmaEntity(ImageEntity):
    """ Standart deviation Image of a cube """
    _name = "SIGMA"
SigmaEntity._add_data_capability("sigma" , getData,getHeaderCopy,clsname=class_data_names.get("sigma",None),add_scalar=True)
SigmaEntity._add_data_capability("sigma2", getSigma2_fromsigma, getHeaderCopy,
                           clsname=class_data_names.get("sigma2",None),add_scalar=True)

class Sigma2Entity(ImageEntity):
    """ Moment of order 2 Image of a cube """
    _name = "SIGMA2"
Sigma2Entity._add_data_capability("sigma2",getData,getHeaderCopy,
                            clsname=class_data_names.get("sigma2",None),add_scalar=True)
Sigma2Entity._add_data_capability("sigma", getSigma_fromsigma2, getHeaderCopy,
                            clsname=class_data_names.get("sigma",None), add_scalar=True)

class Sigma3Entity(ImageEntity):
    """ Moment of order 3 Image of a cube """
    _name = "SIGMA3"
Sigma3Entity._add_data_capability("sigma3",getData,getHeaderCopy,
                            clsname=class_data_names.get("sigma3",None),add_scalar=True)

class Sigma4Entity(ImageEntity):
    """  Moment of order 4 Image of a cube """
    _name = "SIGMA4"
Sigma4Entity._add_data_capability("sigma4",getData,getHeaderCopy,
                            clsname=class_data_names.get("sigma4",None),add_scalar=True)    

class NumberEntity(ImageEntity):
    """Image of the number of point used to combine cube """
    _name = "NUMBER"
NumberEntity._add_data_capability("number",getData,getHeaderCopy,
                            clsname=class_data_names.get("number",None),add_scalar=True)    

########################################################################
# 
# Function for switches
#
#######################################################################
def getSignalerror(self, **kwargs):
    """ Return the Signal Error bar of a previously combined images 
        std / sqrt(N) 
        std is the standart deviation and the number of combined points
    """
    # Replace silently all the mean by sum
    Nkwargs = kwargs.copy()
    _update_number_kwargs_for_error(Nkwargs)    
    return self.getSigma(**kwargs) / np.sqrt(self.getNumber(**Nkwargs))


def getMoment_fromsigmas(self, moment, **kwargs):
    """
    Return the moment previously combined images, extention M{moment} or SIGMA{moment}
    where {moment} is the moment order must be present
    """
    try: 
        s = self["SIGMA%d"%moment]
    except KeyError:
        # legacy combined data where SIGMA2 was M2
        try:
            s = self["M%d"%moment]
        except KeyError:
            raise NotEnoughData("Cannot find extention for moment {moment}, SIGMA{moment} or M{moment} not found".format(moment=moment))
    return s.getData(**kwargs)



class _KeyImageSwitchCapabilities_(_KeySwitchCapabilities_):
    pass
class _DataImageSwitchCapabilities_(_DataSwitchCapabilities_,_ExecuteCapabilities_):
    def withOutput(self, output):
        return self.__class__( [e.withOutput(output) for e in self if hasattr(e, "withOutput")] )
    iterOutput = iterOutput
    getOutputSet = getOutputSet

for k in image_data_keys: 
    _DataImageSwitchCapabilities_._add_data_capability(k, on=0, clsname=k, add_scalar=True)

    
class _KeyCubeSwitchCapabilities_(_KeyImageSwitchCapabilities_):
    pass
class _DataCubeSwitchCapabilities_(_DataImageSwitchCapabilities_):
    pass



    
class _KeyCombinedSwitchCapabilities_(_KeyCubeSwitchCapabilities_):
    pass    
class _DataCombinedSwitchCapabilities_(_DataCubeSwitchCapabilities_):
    getMoment = getMoment_fromsigmas
    getSignalerror    = getSignalerror
    getSignalvariance = getSignalvariance
    getSigma2error    = getSigma2error
    getSigma2variance = getSigma2variance



for i,k in enumerate(["signal","sigma2", "sigma3", "sigma4", "number"]):    
    _DataCombinedSwitchCapabilities_._add_data_capability(k, on=i, clsname=k, add_scalar=True)
# sigma depend on sigma2 for combined 
_DataCombinedSwitchCapabilities_._add_data_capability("sigma",on=1, clsname="sigma", add_scalar=True)

for k in ["signal","sigma","sigma2", "sigma3", "sigma4", "number", "signalerror", "sigma2error",
          "signalvariance", "sigma2variance", "moment"]:
    _DataCubeSwitchCapabilities_._add_data_capability(k,on=0, clsname=k, add_scalar=True)
for k in ["signalerror", "sigma2error", "signalvariance", "sigma2variance"]:
    engine.add_scalar_data_capability_as_entity(_DataCombinedSwitchCapabilities_, k)


class CubeSwitch(Switch,_DataCubeSwitchCapabilities_,_KeyCubeSwitchCapabilities_, baseplot.CubeImage):
    """ A switch (=HDUList) with only one data entity  (HDU) with a cube """
    subclasses = {
        0:CubeEntity
    }

class CombinedSwitch(Switch,_DataCombinedSwitchCapabilities_,_KeyCubeSwitchCapabilities_,baseplot.Combined):
    """A switch (=HDUList) 5 data entity  (HDU) representing:
    - the averaged cube
    - moment of order 2
    - moment of order 3
    - moment of order 4
    - The number of pixel (int the time direction) used to compute these values
    """
    subclasses = {
        0:SignalEntity,
        1:Sigma2Entity,
        2:Sigma3Entity,
        3:Sigma4Entity,
        4:NumberEntity
    }
    
###
# Polpulate CubeSwitch and CombinedSwitch
# 
    
###################################################################
# Now the list 
#
class _KeyImageDataListCapabilities_(_KeyDataListCapabilities_):
    pass
class _DataImageDataListCapabilities_(_DataDataListCapabilities_,_ExecuteCapabilities_):
    pass
    def withOutput(self, output):
        return self.__class__( [e.withOutput(output) for e in self if hasattr(e, "withOutput")] )
    iterOutput   = iterOutput    
    getOutputSet = getOutputSet
    
for k in image_data_keys:
    _DataImageDataListCapabilities_._add_data_capability(k, add_scalar=True, clsname=class_data_names.get(k,None))
    

class _KeyCubeListCapabilities_(_KeyImageDataListCapabilities_):
    pass

class _DataCubeListCapabilities_(_DataImageDataListCapabilities_):
    pass

for k in cube_data_keys:
    _DataCubeListCapabilities_._add_data_capability(k, add_scalar=True, clsname=class_data_names.get(k,None))

    
class CubeListDataList(DataList, _DataCubeListCapabilities_, _KeyDataListCapabilities_):
    pass
    
######################################
# Must be at the end 
#
_update_class_dictionary_(__name__)

def __test__():
    global e,fls
    e = CubeTimeEntity( DataX( np.random.random((100,255,320)), ["time","y","x"]) )
    kw = dict( x_idx = 10, y_idx = 11)
    e.data[:,11,10] = np.random.normal( scale=2, size=(100,))
    
    print getSignal_cube(e).shape, getSignal_time(e).shape
    print "SIGMAS",e.getSigma(**kw), e.getSigma2(**kw), e.getSigma3(**kw), e.getSigma4(**kw)
    print "min,max,median,mean,std", e.getMinSignal(),  e.getMaxSignal(),  e.getMedianSignal(), e.getMeanSignal(), e.getStdSignal()
    print "10 11 ->", e.getX(**kw),  e.getY(**kw)
    print e.getSignal().shape, e.getSigma2().shape, e.getSignalerror().shape, e.getSigma2error().shape
            
if __name__ is "__main__":
    __test__()
                
    
