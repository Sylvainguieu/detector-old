import numpy as np
from datax import DataX, dataxconcat, size, isdatax
import os
import scipy.ndimage as ndim
import re
from detector import baseplot

vectorialized = True
_verbose = True

from detector.config import * 

################################################################################
#
#  This 3 little function are for history compatibility 
#  freduce=mean means x_reduce=mean, y_reduce=mean   section=sec means 
#   x_section=sec, y_section=sec
#
################################################################################    

PIXELAXESALIAS = ["freduce"]
REDUCESUFIX = ""
MEANKWARGS   = {k+REDUCESUFIX:np.mean   for k in PIXELAXESALIAS}
MEDIANKWARGS = {k+REDUCESUFIX:np.median for k in PIXELAXESALIAS}
STDKWARGS    = {k+REDUCESUFIX:np.std    for k in PIXELAXESALIAS}

REDUCE_TO_SCALAR = {"min":np.min,"max":np.max,"mean":np.mean,"median":np.median,"std":np.std}
ADD_SCALAR       = True
class DEFAULT:
    pass

def defaultKwargs(default, kwargs):    
    for k,v in default.iteritems():
        kwargs.setdefault(k,v)
def defaultMeanKwargs(kwargs):
    defaultKwargs(MEANKWARGS, kwargs)
def defaultMedianKwargs(kwargs):
    defaultKwargs(MEDIANKWARGS, kwargs)
def defaultStdKwargs(kwargs):
    defaultKwargs(STDKWARGS, kwargs)


def defaultMeanTime(kwargs):
    for k in TIMEAXES:
        kwargs[k+"_reduce"] = np.mean

def defaultStdTime(kwargs):
    for k in TIMEAXES:
        kwargs[k+"_reduce"] = np.std
        
def _updateReduceKwargs(kwargs):
    #
    #  for backward compatibility 
    #  freduce is equivalent to xreduce+yreduce
    if "freduce" in  kwargs:
        freduce = kwargs.pop("freduce")
        for k in PIXELAXES:
            kwargs.setdefault(k+"_reduce",freduce)
            

def _updateSectionKwargs(kwargs):
    KEYWORDCONFLICT = "conflict between '%s' and '%s' keyword, '%s' is retained"
    
    for k in IDXKEYS:

        if k in kwargs:
            kr = k+"_idx"
            if kr in kwargs:
                say(KEYWORDCONFLICT%(k,kr,k), vtype=WARNING)
            kwargs[kr] = kwargs.pop(k)
    
    if not "section" in kwargs: return              
    section = kwargs.pop("section")
    if section is None: return
    if isinstance( section, slice):
        if Y+"_idx" in kwargs:
                say(KEYWORDCONFLICT%("section",Y+"_idx","section"), vtype=WARNING)
        kwargs[Y+"_idx"] = section
        return 
    else:
        y_idx, x_idx = section
        if X+"_idx" in kwargs:
            say(KEYWORDCONFLICT%("section",X+"_idx","section[1]"), vtype=WARNING) 
        kwargs[X+"_idx"] = x_idx
        
        if Y+"_idx" in kwargs:
            say(KEYWORDCONFLICT%("section",Y+"_idx","section[0]"), vtype=WARNING)
        kwargs[Y+"_idx"] = y_idx

def _updateKwargs(kwargs):
    _updateReduceKwargs (kwargs)
    _updateSectionKwargs(kwargs)
################################################################################


class NotEnoughData(Exception):
    pass

def say(msg, level=1, vtype=NOTICE, indent=True):    
    if _verbose>= level:
        _print_msg(msg, vtype, indent=indent)

def _print_msg(msg, vtype, indent=True):
    if indent:
        print "%s : %s"%(vtype, msg)
    else:
        print "%s : %s"%(vtype, msg), 

            
def objsay(obj,msg, level=1, vtype=NOTICE, indent=True):
    """ say(obj, msg ) 
    function to verbose a message. obj should be an object with the .verbose 
    if obj.verbose>= level: print msg
    attribute
    
    """
    if not hasattr(obj, "verbose"): return 
    if obj.verbose>=level:
        return _print_msg(msg, vtype, indent=indent)
    

_class_dictionary_ = None
def _make_class_dictionary_(module=__name__):
    global _class_dictionary_
    import sys
    _class_dictionary_ = {k.lower().strip("_"):v   for k,v in sys.modules[module].__dict__.iteritems() if type(v) is type and issubclass(v,(DataEntity,_List_,_ListList_)) }

def getClass(classname,primary=False):
    """ Return the right class given for a given name 
    primary keyword argument as no effect here, just for Fits compatibility
    """
    if _class_dictionary_ is None: _make_class_dictionary_()
    return _class_dictionary_[classname]
    

def getSignalData( obj, **kwargs):
    defaultMeanTime(kwargs)
    return obj.getData(**kwargs)

def getMomentData(obj, moment, **kwargs):                
    return computeMoment_datax(
        obj.getData(**kwargs), 
        axis=obj.axes[0]
    )

def getStdData(self, **kwargs):
    defaultStdTime(kwargs)
    return self.getData(**kwargs)



def getSignal(obj, **kwargs):
    return obj._getClass_("signal")( obj.getSignalData(**kwargs), obj.getSignalHeader())

def getN(obj, **kwargs):
    return obj._getClass_("n")( obj.getNData(**kwargs), obj.getNHeader()) 

def getStd(obj, **kwargs):
    return obj._getClass_("std")( obj.getStdData(**kwargs), obj.getStdHeader())  

def getMoment(obj, moment , **kwargs):
    if moment<2 or moment>4:
        return obj._getClass_("moment")( obj.getMometnData(moment, **kwargs), obj.getMomentHeader(moment))
    else:
        return obj._getClass_("m%d"%moment)( obj.getMomentData(moment,**kwargs),
                                              obj.getMomentHeader(moment) )

def getM2(obj, **kwargs):
    return getMoment( obj, 2, **kwargs)

def getM2Error(obj, **kwargs):
    return obj._getClass_("m2error")( obj.getM2ErrorData(**kwargs), obj.getM2ErrorHeader()) 


def getM3(obj, **kwargs):
    return getMoment( obj, 3, **kwargs)

def getM4(obj, **kwargs):
    return getMoment( obj, 4, **kwargs)

def getGain(obj, **kwargs):
    return obj._getClass_("gain")( obj.getGainData(**kwargs), obj.getGainHeader())

def getGainM(obj, **kwargs):
    return obj._getClass_("gainm")(obj.getGainMData(**kwargs), obj.getGainMHeader())


def getNoise2(obj, **kwargs):
    return obj._getClass_("noise2")(obj.getNoise2Data(**kwargs), obj.getNoise2Header())
def getNoise(obj, **kwargs):
    return obj._getClass_("noise")( obj.getNoiseMinData(**kwargs), obj.getNoiseHeader())
getNoiseMin = getNoise
def getENoise(obj, **kwargs):
    return obj._getClass_("noise")( obj.getENoiseData(**kwargs), obj.getENoiseHeader())

def getPolar(obj, **kwargs):
    return obj._getClass_("polar")( obj.getPolarData(**kwargs), obj.getPolarHeader())

def getFlux(obj, **kwargs):
    return obj._getClass_("flux")( obj.getFluxData(**kwargs), obj.getFluxHeader())

def getEFlux(obj, **kwargs):
    return obj._getClass_("flux")( obj.getEFluxData(**kwargs), obj.getEFluxHeader())

def getBias(obj, **kwargs):
    return obj._getClass_("bias")( obj.getBiasData(**kwargs), obj.getBiasHeader())


def getSignalHeader(obj):
    return obj.getHeader()
def getStdHeader(obj):
    return obj.newHeader( {"TYPE":"SIGMA"} )
def getMomentHeader(obj, moment):
    return obj.newHeader( {"TYPE":"SIGMA%d"%moment})
def getNHeader(obj):
    return obj.newHeader( {"TYPE":"N"})
    
def getHeaderCopy( obj):
    return getHeader(obj).copy()
def getHeader( obj):
    return obj.header

def newHeader(self, *args, **kwargs):
    return self.newHeader(*args, **kwargs)

def firstHeader(lst, **kwargs):
    lst[0].getHeader(**kwargs)
    
def firstHeaderCommentFiles(lst,  **kwargs):
    h = lst[0].getHeader()
    h.set("COMMENT", ":FILES:"+",".join(lst.getFileName())+":FILES:")
    return h

def getDirectory(dirName, rootdir=None):    
    if rootdir:
        rootdirs = rootdir.rstrip("/").split("/")
        dirs = dirName.split("/")
        if rootdirs == dirs[0:len(rootdirs)]:
            return "/".join(dirs[len(rootdirs):])
    return dirName

def getTemp(self, **kwargs):
    fname = self.getFileName()
    tc = re.compile( "T([0-9][0-9][0-9])[^0-9]" )
    f = tc.findall(fname)
    if not len (f):
        raise Exception("cannot find temperature information in file name")
    return 273.15-int(f[0]) #convert in Kelvin 


########################################################################
#
#  General functions called on obj 
#
########################################################################
def getData(obj, *args, **kwargs):
    return obj.getData(*args, **kwargs)    
def getDit(obj,*args, **kwargs):
    return obj.getDit(*args, **kwargs)
def getPolar(obj,*args, **kwargs):
    return obj.getPolar(*args, **kwargs)
def getIllumination(obj, *args, **kwargs):
    return obj.getIllumination(*args, **kwargs)
def getKey(obj,*args, **kwargs):
    return obj.getKey(*args, **kwargs)

###############################################################################
class _DataEntity_(object):
    data_params = None    
    ###
    # The following functions must be implemented by the data holder 
    ###
    say = objsay
    _getClass_ = staticmethod(getClass)
    
    def getData(self, copy=False, **kwargs):
        if copy:
            return self._return_array_data_and_scale(self.data.copy(), kwargs)
        else:
            return self._return_array_data_and_scale(self.data   , kwargs)
    
    def getShape(self):
        return self.data.shape
    #dataShape is the same then getShape at HDU level
    getDataShape = getShape
        
    def _return_array_data_and_scale(self, data, kwargs):
        data = self._return_array_data(data, kwargs)        
        data = self.offsetData(data, **kwargs)
        data =  self.scaleData(data , **kwargs)
        return data
    
    def _return_array_data(self, data, kwargs):
        axes = self.axes if isinstance(data, np.ndarray) else []
        if axes:
            _updateKwargs(kwargs)
            if len(kwargs): self.say("reducing the data with %s"%kwargs)            
            data = DataX(data, axes).apply( **kwargs)
        elif len(kwargs):
            ###
            # Decide or not to put an error here 
            print "Warning: entity has no axes name, data is not a DataX. reduce and\
section kwargs will be ignored "
        
        return data        

    def offsetData(self, data, offset=None,**kwargs):
        """
        Substract an offset to the reduced data
        if keyword offset is present use it otherwhise look at 
        the hdu.offset attribute. 
        If all of them are None data is reutrned as it is silently
        """
        data_params = self._getDataParams()
        if data_params.get("offset",None) is None and offset is None: return data
        
        self.say( "Substract offset to the data") 
        return data - self.getOffset(offset=offset,**kwargs)
    def _getDataParams(self):
        if self.data_params is None:
            self.data_params  = {}
        return self.data_params

    def getOffset(self, offset=None, **kwargs):                
        data_params = self._getDataParams()
        offset = data_params.get("offset",None) if offset is None else offset    
        
        if offset is None: return 0.0
        if isdatax(offset):
            return offset.apply(**kwargs)        
        elif len(kwargs):
            ###
            # Decide or not to put an error here 
            print "Warning: offset is not a DataX. reduce and\
section kwargs will be ignored"
        return offset

    def getScale(self, scale=None, **kwargs):
        data_params = self._getDataParams()
        scale = data_params.get("scale",None) if scale is None else scale
        if scale is None: return 1.0
        if isdatax(scale):
            return scale.apply(**kwargs)        
        return scale
    
    def scaleData(self, data, scale=None, **kwargs):
        """
        Multiply by a scale factor the data
        if keyword scale is present use it otherwhise look at 
        the hdu.scale attribute. 
        If all of them are None data is reutrned as it is silently
        """
        data_params = self._getDataParams()
        if data_params.get("scale",None) is None and scale is None: return data
        self.say( "Rescaling the data ") 
        return data*self.getScale(scale=scale,**kwargs)

    def setOffset(self, offset):
        """ 
        Set the offset to the HDU 
        offset must be compatible with the data and can be a DataX object 
        """
        data_params = self._getDataParams()
        data_params['offset'] = offset
        
    def setScale(self, scale):
        """ 
        Set the scale to the HDU 
        scale must be compatible with the data and can be a DataX object 
        """
        data_params = self._getDataParams()        
        data_params['scale'] = scale
    
    def delOffset(self):
        data_params = self._getDataParams()  
        data_params.pop("offset", None)
    
    def delScale(self):
        data_params = self._getDataParams()  
        data_params.pop("scale", None)
                
class _KeyEntity_(object):
    def getFilePath(self):
        raise NotImplementedError()
    
    def getKey(self,k, default=None):
        kc = self.header.get(k, default) 
        if isinstance(kc , tuple):
            kc = kc[0]
        
        return kc
    def setKey(self,k, v):
        self.header[k] = v
    def hasKey(self,k):
        return k in self.header
    def getKeyComment(self,k):
        kc = self.header.get(k) 
        if isinstance(kc , tuple):
            return kc[1]
        return ""
      
    def getHeader(self, copy=False):
        if copy:
            return self.header.copy()        
        return self.header
    def newHeader(self, *args, **kwargs):
        return dict(*args,**kwargs)
    
    
    def getAttr(self, attr):
        return self.__getattribute__(attr)
    def callAttr(self, attr, *args, **kwargs):
        return self.getAttr(attr)( *args, **kwargs)
    
    def getDit(self):
        return self.getKey("DET DIT")
    def getNDit(self):
        return self.getKey("DET NDIT")

    def getDisp(self, **kwargs):
        return self.getKey("INS OPTI2 NAME ")
    def getConfig(self):
        [("DIT",  self.getDit()  ),
         ("POLAR",self.getPolar()),
         ("DISP" ,self.getDisp() )         
        ]
          

    def getMjdObs(self):
        return self.getKey("MJD-OBS")
    
    def getCycleTime(self):
        return self.getKey("DET READOUT CYCLETIME")

    
    def getPolar(self, **kwargs):
        if self.hasKey( "DET POLAR"):
            return self.getKey("DET POLAR")
        # if no polar is found try to gess it with file name
        # (this is for old files on BETI)
        fname = self.getFileName()
        tc = re.compile( "P([0-9][0-9][0-9][0-9])[^0-9]" )
        f = tc.findall( fname)
        if not len (f):
            raise Exception("cannot find polar information in file name")
        return int(f[0])
    def getIllumination(self, **kwargs):
        return 0
    def getShutter(self, shut, **kwargs):
        return self.getKey("INS.SHUT%d.ST"%(shut))
    
    def getShutters(self, **kwargs):
        return reduce( lambda x,i: x+self.getKey("INS.SHUT%d.ST"%(i+1) )*(2**i), range(NSHUTTERS), 0)

    
        
    
def _map( func, iterable):
    if not hasattr( iterable, "__iter__"):
        return func(iterable)
    
    output = map( func, iterable)
    if isdatax(iterable):
        return DataX( output, iterable.axes)
    return type(iterable)(output)
        
class _ParamsDerived_(object):
    def getDitData(self, **kwargs):
        shape = self.getDataShape()

        dit = self.getDit() #dit can be an array and should have
                            #len equal to first element of data shape
        if len(dit):
            if len(dit)!=shape[0]:
                raise Exception("invalid dit dimension compare to data shape")
            N = reduce( lambda x,y: x*y, shape[1:])
            dits = np.tile( dit, N).reshape(shape)
        else:
            dits    = np.ndarray(shape, dtype=float)
            dits[:] = self.getDit()
        _updateKwargs(kwargs)
        return DataX( dits, self.getDataAxes()).apply(**kwargs)
    
    def getFileDirectory(self):
        path = self.getFilePath()
        return _map( lambda p:os.path.split(p)[0], path )
    
    def getFileName(self):
        path = self.getFilePath()
        return _map( lambda p: os.path.split(p)[1], path)

    def getDirectory(self, rootdir=None):
        dirs = self.getFileDirectory()
        return _map( lambda d: getDirectory(d,rootdir=rootdir), dirs)
    
    
class _ImageDerived_(object):
    """ Some function depending only on these lower level functions:
        getSignal, getMomment, getN, getStd, getKey, getData
    """

    def getMeanData(self, **kwargs):
        """ Return the mean of data, always a scalar """
        return self.getData(**kwargs).mean()
    
    def getMedianData(self, **kwargs):
        """ Return the Median of data """
        return median(self.getData(**kwargs))
    
    def getStdData(self, **kwargs):
        """ Return the standart deviation of data as a scalar"""
        return self.getData(**kwargs).std()
    
    
    def getMeanSignal(self, **kwargs):
        defaultMeanKwargs(kwargs)
        return self.getSignalData(**kwargs)        
    
    def getMedianSignal(self, **kwargs):
        defaultMedianKwargs(kwargs)
        return self.getSignalData(**kwargs)        
    
    def getStdSignal(self, **kwargs):
        defaultStdKwargs(kwargs)
        return self.getSignalData(**kwargs)        
    
    def getSignalErrorData(self, **kwargs):
        return self.getStdData(**kwargs) / np.sqrt( self.getNData(**kwargs))
    
    def getSignalError(self, **kwargs):
        return getStd(self) 
    
    def getMeanSignalError(self, **kwargs):
        axis = PIXELAXES
        #silently remove all _reduce concerning pixels
        for k in axis:
            kwargs.pop(k,None) 
        kwargs.pop("freduce", None)
        
        m2 = self.getM2Data(**kwargs)
        axis = [ax for ax in axis if ax in m2.axes]        
        m2mean = m2.mean( axis=axis )
        Ntime = self.getNData(**kwargs).sum( axis=axis)
        return np.sqrt( m2mean/ Ntime )
        #return np.sqrt( m2mean/ (size( m2, axis=axis)*Ntime) )
        
        #if freduce==np.sum:
        #    return np.sqrt( freduce(m2) )
        #else:# assume it is mean or median 
        #    return np.sqrt(freduce(m2) / m2.size)
    
    def getMeanMoment(self, moment, **kwargs):
        defaultMeanKwargs(kwargs)
        return self.getMomentData(moment, **kwargs)                
    def getMedianMoment(self, moment, **kwargs):
        kwargs['freduce'] = np.median
        defaultMedianKwargs(kwargs)
        return self.getMomentData(moment, **kwargs)    
    def getStdMoment(self, moment, **kwargs):
        defaultStdKwargs(kwargs)
        return self.getMomentData(moment, **kwargs)
    
    def getMeanN(self, **kwargs):
        defaultMeanKwargs(kwargs)
        return self.getNData(**kwargs)                
    def getMedianN(self, **kwargs):
        defaultMedianKwargs(kwargs)
        return self.getNData(**kwargs)        
    def getStdN(self, **kwargs):
        defaultStdKwargs(kwargs)
        return self.getNData(**kwargs)
    
    def getM2Data(self,  **kwargs):
        return self.getMomentData(2, **kwargs)
    def getM2Header(self, **kwargs):
        return self.getMomentHeader(2, **kwargs)
    getM2 = getM2
    
    def getMeanM2(self, **kwargs):
        return self.getMeanMoment(2, **kwargs)
    def getMedianM2(self, **kwargs):
        return self.getMedianMoment(2, **kwargs)
    def getStdM2(self, **kwargs):
        return self.getStdMoment(2, **kwargs)

    def getM2ErrorData(self, **kwargs):
        m2  = self.getM2Data(**kwargs)
        m4  = self.getM4Data(**kwargs)
        N   = self.getNData(**kwargs)
        return computeVarianceErrorbar(m2, m4, N)
    
    def getM2Error(self, **kwargs):
       return _M2Error_(self.getM2ErrorData(**kwargs), self.getHeader())
    
    def getMeanM2Error(self, **kwargs):
        axis = PIXELAXES
        #silently remove all _reduce concerning pixels
        for k in axis:
            kwargs.pop(k+"_reduce",None) 
        kwargs.pop("freduce", None)
                
        verr = self.getM2ErrorData(**kwargs)        
        verr2 = verr*verr
        axis = [ax for ax in axis if ax in verr2.axes]
        Ntime = self.getNData(**kwargs).sum( axis=axis)
        return np.sqrt( verr2.mean( axis=axis) / Ntime )
        #return np.sqrt( verr2.mean( axis=axis)/ float(size(verr2, axis=axis)) )
    
        #if freduce == np.sum:
        #    return np.sqrt( freduce(verr**2) )
        ##else assume it is mean or median !!!
        #return np.sqrt( freduce(verr**2)/float(verr.size))

    def getM3Data(self, **kwargs):
        return self.getMomentData(3, **kwargs)
    def getM3Header(self, **kwargs):
        return self.getMomentHeader(3, **kwargs)
    getM3 = getM3
                
    def getMeanM3(self, **kwargs):
        return self.getMeanMoment(3, **kwargs)
    def getMedianM3(self, **kwargs):
        return self.getMedianMoment(3, **kwargs)
    def getStdM3(self, **kwargs):
        return self.getStdMoment(3, **kwargs)
    
    def getM4Data(self, **kwargs):
        return self.getMomentData(4, **kwargs)
    def getM4Header(self, **kwargs):
        return self.getMomentHeader(4, **kwargs)
    getM4 = getM4
    
    def getMeanM4(self, **kwargs):
        return self.getMeanMoment(4, **kwargs)
    def getMedianM4(self, **kwargs):
        return self.getMedianMoment(4, **kwargs)
    def getStdM4(self, **kwargs):
        return self.getStdMoment(4, **kwargs)

    def getStdData(self,**kwargs):
        """ Return the standart deviation array data """
        return np.sqrt( self.getM2(**kwargs))    
    getStd = getStd
    
    def getDit(self, **kwargs):
        return self.getKey("DET DIT", **kwargs)
    def getNDit(self, **kwargs):
        return self.getKey("DET NDIT", **kwargs)    
    
    getSignal = getSignal
    getN      = getN
    getMoment = getMoment

    
    
class _Entity_(_DataEntity_, _KeyEntity_,baseplot._Plots_):
    pass
    
class DataEntity(_Entity_, _ParamsDerived_):
    """
    A very simple data older 
    """
    def __init__(self,data=None, header=None, axes=None, name="", **kwargs):
        header = header or {}
        header.update(kwargs)
        self.header = header
        self.data   = data
        self.name   = name
        if axes is not None: self.axes = list(axes)
        else:
            if isdatax(data): self.axes = list(data.axes)
    @property
    def name(self): return self._name
    @name.setter
    def name(self, value): self._name = str(value)
    @name.deleter
    def name(self): self._name = ""

    @staticmethod
    def isList():
        return False
    @staticmethod
    def isListList():
        return False
    @staticmethod
    def isEntity():
        return True
    
    def getDataAxes(self):
        return self.axes
    
    @classmethod
    def _add_key_capability(cls,name,func):
        getname  = "get"+name.capitalize()        
        setattr(cls, getname, func)
    @classmethod
    def _add_data_capability(cls,name,datafunc, headerfunc=newHeader, clsname=None, add_scalar=ADD_SCALAR):
        getname    = "get"+name.capitalize()+"Data"
        headername = "get"+name.capitalize()+"Header"                
        setattr(cls, getname, datafunc)
        if clsname:
        ###
        # if a class name is present means it has a method to encapsulate
        # in a new DataEntity (e.g. HDU or other)
            encapsname    = "get"+name.capitalize()
            def tmp_get(self, *args, **kwargs):                
                return self._getClass_(clsname)( getattr(self,getname)(*args, **kwargs),
                                                 getattr(self,headername)())
            setattr(cls, encapsname, tmp_get)            
            setattr(cls, headername, headerfunc)
        if add_scalar:
            cls._add_scalar_data_capability(name)
            
    @classmethod
    def _add_scalar_data_capability(cls, name, reduces=REDUCE_TO_SCALAR):
        for fname,func in reduces.iteritems():
            getrname  = "get"+fname.capitalize()+name.capitalize()
            getname   = "get"+name.capitalize()+"Data"    
            setattr(cls, getrname , reduce_decorator(getname,func))


class _OneD_(DataEntity):
    pass

class _TwoD_(DataEntity, baseplot._TwoD_):
    """
    function for a simple two D image 
    """
    def getYX(self, **kwargs):
        shapes = self.getDataShape()
        xi = self.axes.index(X)
        yi = self.axes.index(Y)

        if yi<xi:
            y,x = np.mgrid[0:shapes[yi],0:shapes[xi]]
        else:
            x,y = np.mgrid[0:shapes[yi],0:shapes[xi]]
            
        y = self._return_array_data( y, kwargs)
        x = self._return_array_data( x, kwargs)
        return y,x 
    
    def getX(self, **kwargs):
        return self.getYX(**kwargs)[1]
    def getY(self, **kwargs):
        return self.getYX(**kwargs)[0]
    
    def getImage(self, **kwargs):
        shape = self.getShape()
        if len(shape)!=2:
            raise Exception("Not an image")                                      
        return self.getData(**kwargs)

    def getDitData(self, **kwargs):
        d = np.ndarray(self.getDataShape(), dtype=float)
        d[:] = self.getDit() #dit should be a scalar here
        return DataX( d, self.getDataAxes()).apply(**kwargs)
        
    def median_filter(self, **kwargs):
        mf_keys = ["size", "footprint", "output", "mode", "cval", "origin"]

    median_filter.__doc__ = ndim.median_filter.__doc__

    
class _ThreeD_(DataEntity):
    def getImage(self, **kwargs):
        for axis in self.axes[0:-2]:
            if not axis+"_idx" in kwargs and not axis+"_section" in kwargs and not axis+"_reduce" in kwargs:
                raise Exception("To get an image, one of {axis}_ids, {axis}_section, {axis}_reduce must be present".format(axis=axis))                            
        return self.getData(**kwargs)

class _NoData_(object):
    def getSignalData(self, **kwargs):
        raise Exception("No signal data in this object")
    def getMomentData(self, moment, **kwargs):
        raise Exception("No moment data in this object")    
    def getNData(self, **kwargs):
        raise Exception("No N data in this object")
    def getMjd(self):
        return self.getMjdObs()

    
class _Table_(DataEntity):
    _keys_as_table = {}
    def getData(self, item=None , **kwargs):
        return self._return_array_data( self.data[item], kwargs)
    def fillHeader(self, index, header, keys_as_table=None):
        keys_as_table = keys_as_table or self._keys_as_table
        for kf in keys_as_table:
            k = kf[0]
            if self.hasKey(k):
                val = self.getKey(k)[index]
                header[k] = val

class _KeysTable_(_Table_):
    """ made for a recarray containing some keys value """
    axes = [EXPO]
    _keys_as_table = {}    
    def getKey(self, key, **kwargs):
        return self.getData(key,**kwargs)
    def hasKey(self, key, **kwargs):
        return key in self.data.names
    def getKeyComment(self, key, **kwargs):
        return ""
    def getConfig(self, **kwargs):
        return []
    def getPolar(self, **kwargs):
        return self.getKey("DET POLAR",**kwargs)
    def getIllumination(self, **kwargs):
        return self.getKey("ILLUMINATION",**kwargs)
    def getFilePath(self, **kwargs):
        return self.getKey("FILENAME", **kwargs)
    def getRealKey(self, key, **kwargs):
        return super(_KeysTable_, self).getKey(key, **kwargs)


def _list_decorator_prepare(obj, onsection, kwargs, lenfunc=len, inaxis=None, on_default_attr="_on_default"):                
        section = None
        freduce = None        
        axes = None

        
        if onsection is False:
            if hasattr(obj,on_default_attr):
                onsection = getattr(obj,on_default_attr)
            else:
                onsection = None
        
        
        if inaxis is not None:
            axes = [inaxis]
        elif hasattr(obj ,"axes"):
            axes =  obj.axes
            
        if axes is not None:
            if axes and len(axes):
                if axes[0] in IDXKEYS: # check is the short/lazy keyword is accepted
                    section = kwargs.pop(axes[0], None)                    
                if axes[0]+"_idx" in kwargs:
                    if section is not None:
                        say("conflict between '%s' and '%s' keyword, '%s' is retained"%(axes[0]+"_section", axes[0], axes[0])) 
                    section = kwargs.pop(axes[0]+"_idx", None)

            ######
            # If section is anything else than a slice, a int None or array 
            # convert it to an array 
            if (section is not None) and (not isinstance(section,slice)) and (not isinstance(section,(int,long))) and (not isinstance(section,np.ndarray)):
                section = np.array(section)
            ####
            # if section is boolean convert it to an array of index
            if isinstance(section, np.ndarray) and section.dtype == np.bool:
                section = np.where(section.flat)[0]
                
            if isdatax(section):
                axes = section.axes
            else:
                axes = axes[0:1]
            freduce = { ax+"_reduce":kwargs.pop(ax+"_reduce") for ax in axes if ax+"_reduce" in kwargs}        
        if isinstance(onsection, slice):
            onsection = np.r_[0:lenfunc(obj)][onsection]
        
        # ## ## ## ## ##
        # if onsection is a scalar, section and freduce should be None !
        # ## ## ## ## ##
        if onsection is not None and not hasattr(onsection, "__len__"):
            if section is not None:
                raise IndexError( " %s is %s on an index with no lens "%(axes[0]+"_idx",section))
            #if freduce is not None:
            #    raise IndexError( " %s is %s on an index with no lens "%(axes[0]+"_reduce",freduce))
        
        if onsection is None and section is not None:                       
            index = np.r_[0:lenfunc(obj)][section]
        elif section is not None:                
            index = np.r_[0:len(onsection)][section]
        else:
            index = onsection
            
        if index is None:
            index = range(lenfunc(obj))
        if isinstance(index, np.ndarray): index = index.flat
        return index, section, freduce

def _return_list_array_decorator(A, section, freduce):
    ##
    # At this point A should be flat
    # we need to return A to reflect the section attribute if this one
    # was a N>2 array dimention.
    if isinstance( A, np.ndarray):
        if isinstance( section,  np.ndarray):
            index = np.arange(section.size).reshape(section.shape)
            if isdatax( section):                
                index = DataX( index, section.axes )
            print A, index
            A = A[index]
    
    if isdatax( A) and freduce is not None and len(freduce):
        A = A.apply( **freduce) 
        
    return A


def _get_item_exec_method(obj, funcname, item , args, kwargs):
    return getattr(obj[item], funcname)(*args, **kwargs)
def _get_item_exec_func(obj, func, item , args, kwargs):
    return func(obj[item],*args, **kwargs)
_len_func = len

def _list_decorator(funcname, on=False, inarray=True, len_func=len, exec_func=_get_item_exec_func, inaxis=None, on_default_attr="_on_default"):
    """
    A decorator that loop over the list and apply the right method
    if defaulindex is not None use that index as default insteed of looping 
    if on  is not None the onis always used and default is ignored 
    """
    def tmp_decorator_list(self,*args, **kwargs):
        """
        A list looper for the attribute %s
        """%funcname          
        index, section, freduce = _list_decorator_prepare(
            self, on , kwargs, len_func, inaxis=inaxis, on_default_attr=on_default_attr
        )                
        if  hasattr(index,"__iter__"):
            A = self.finalise([ exec_func(self,funcname,i, args, kwargs) for i in index ], inarray=inarray, inaxis=inaxis)      
        else:
            return exec_func(self,funcname,index, args, kwargs)
        
        return _return_list_array_decorator( A, section, freduce)    
    return tmp_decorator_list


def list_decorator(func_or_methodname, on=False, inarray=True, inaxis=None):
    if isinstance(func_or_methodname, basestring):
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_method, inaxis=inaxis)
    else:
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_func, inaxis=inaxis)

def data_decorator(func_or_methodname, on=False, inarray=True, inaxis=None):
    if isinstance(func_or_methodname, basestring):
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_method, inaxis=inaxis,
                               on_default_attr="_on_data_default"
                           )
    else:
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_func, inaxis=inaxis,
                               on_default_attr="_on_data_default"
                           )

def key_decorator(func_or_methodname, on=False, inarray=True, inaxis=None):
    if isinstance(func_or_methodname, basestring):
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_method, inaxis=inaxis,
                               on_default_attr="_on_key_default"
                           )
    else:
        return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=len,
                               exec_func=_get_item_exec_func, inaxis=inaxis,
                               on_default_attr="_on_key_default"
                           )

def reduce_decorator(getname,func):    
    def tmp_get(self, *args, **kwargs):
        return func( getattr(self,getname)(*args, **kwargs)) 
    return tmp_get
    
def _polar_len(obj):    
    return len(obj.getPolarSet())

def _get_polar_exec_method(obj, funcname, item , args, kwargs):
    return getattr(obj.withPolar(obj.getPolarSet()[item]),funcname)( *args, **kwargs)

def polar_decorator(func_or_methodname, on=None, inarray=True, inaxis=None):
    return _list_decorator(func_or_methodname, on=on, inarray=inarray, len_func=_polar_len,
                           exec_func=_get_polar_exec_method,  inaxis=inaxis)

def _dummy_(val):
    return val

def _exec_with_range(methodname, ranges, wrap, args, kwargs):
    minval, maxval = ranges
    def _tmp_exec_with_range(obj):
        if minval is None and maxval is None: return True        
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        if maxval is None:
            return value>=minval
        if minval is None:
            return value<=maxval
        return (value>=minval) & (value<=maxval)
    return _tmp_exec_with_range

def _exec_with_val(methodname, valtest, wrap, args, kwargs):
    def _tmp_exec_with_val(obj):
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        return valtest==value
    return _tmp_exec_with_val

def _exec_with_iterable(methodname, lst, wrap, args, kwargs):
    def _tmp_exec_with_iterable(obj):
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        return value in lst
    return _tmp_exec_with_iterable

def _exec_with_array(methodname, A, wrap, args, kwargs):
    def _tmp_exec_with_array(obj):
        value = wrap(getattr(obj,methodname)(*args,**kwargs))
        return any(value == A)
    return _tmp_exec_with_array
                     
def _with_decorator_method(methodname, wrap):
    def tmp_with_method(self,values, *args, **kwargs):

        if not isinstance( values, np.ndarray) and hasattr( values, "__call__"):
            values = values([wrap(getattr(obj,methodname)(*args,**kwargs)) for obj in self])
        
        if isinstance( values, np.ndarray):
            ffunc = _exec_with_array(methodname, values, wrap,args, kwargs)
        elif isinstance( values, tuple):            
            ffunc = _exec_with_range(methodname, values, wrap, args, kwargs)
        elif hasattr( values, "__iter__"):
            ffunc = _exec_with_iterable(methodname, values,wrap,  args, kwargs)
        else:
            ffunc = _exec_with_val(methodname, values, wrap, args, kwargs)
        
        return self.filter(ffunc)
    return tmp_with_method
            
         
def with_decorator(func_or_methodname, wrap=_dummy_):
    return _with_decorator_method(func_or_methodname, wrap)

def set_decorator(methodname, wrap=_dummy_, inaxis=None):
    def tmp_set_decorator(self,*args, **kwargs):
        values = list(set([wrap(getattr(obj,methodname)(*args,**kwargs)) for obj in self]))
        values.sort()
        if inaxis is not None:
            return DataX(np.array(values), [inaxis])
        return values
    return tmp_set_decorator


def header_decorator(getfunc, on=0):
    def tmp_header_decorator(self, *args, **kwargs):
        getf   = getattr(self[on], getfunc)
        #kwargs.setdefault("copy", True)
        header = getf(*args, **kwargs)
        header.set("COMMENT",":FILES:"+",".join(self.getFileName())+":FILES:")
        return header
    return tmp_header_decorator


def _getWins(obj):
    return obj.getWins()
def _exec_wins(obj, funcname, item , args, kwargs):
    kwargs[WIN] = item
    return getattr(obj, funcname)(*args, **kwargs)

def winlist_decorator(funcname, on=None, inarray=True):
    return _list_decorator(funcname, on=on, inarray=inarray, len_func=_getWins,
                           exec_func=_exec_wins)




def tmp_winlist_decorator(funcname, on=None, inarray=True, attr=WIN, lenfunc=_getWins):
    """
    a decorator that loop over the windows and apply the right method
    """
    def tmp_decorator_winlist(self,*args, **kwargs):
        """
        A list looper for the attribute %s
        """%funcname        
        index, section, freduce = _list_decorator_prepare(
            self, on, kwargs, lenfunc
        )
        if hasattr(index,"__iter__"):
            output = []
            for valindex in index:
                kwargs[attr] = valindex
                output.append( getattr(self, funcname)(*args, **kwargs) )
                
            A = self.finalise( output, inarray=inarray)
        else:
            kwargs[attr] = index
            return getattr(self,funcname)(*args, **kwargs)
        
        ############
        # now check the reduce keyword before returning a dataX
        ###########
        if isdatax( A) and freduce is not None:
            return A.reduce( freduce, axis=self.axes[0] )
        return A
    
    return tmp_decorator_winlist

class _WindowData_(_TwoD_, _ImageDerived_, _ParamsDerived_):
    """
    Data for a single detector window (output) 
    """
    axes = [TIME, SAMPIX, Y, X]
    def getXY(self, *args, **kwargs):
        raise NotImplemented("getXY not yet implemented")
    def getFrameRate(self, **kwargs):
        return self.getKey("FREQ")
    def getOutput(self, **kwargs):
        return self.getKey(OUTPUT)
    getSignalData = getSignalData
    getStdData    = getStdData
    getMomentData = getMomentData
    def getNData(self, **kwargs):
        shape = list(self.data.shape)
        N = DataX( np.ones( shape[1:] , dtype=int), self.axes[1:])
        N.flat[:] = shape[0]
        return N.apply(**kwargs)
        #return N.sum(axis=TIME) #lazy way to do it !
    def getDataShape(self):
        return self.data.shape
    

    def getFrequence( self, freq=None ,cut=False, fmin=None, fmax=None, **kwargs):
        if freq is None:
            freq = self.getFrameRate()
        N = self.getDataShape()[0]
        f = (np.arange(N) / float(N))*freq  
        
        if cut:
            f = f[0:f.shape[0]/2]
        if fmax is not None:          
            f = f[ f<=fmax ]
        if fmin is not None:
            f = f[ f>=fmin ]
        return f
        
    def getFFT(self, freq=None, cut=False, fmin=None, fmax=None, **kwargs):
        data = self.getData(**kwargs)
        N = data.shape[0]        
        fft =  np.fft.fft(data)
        if freq is None:
            freq = self.getFrameRate()
        f = (np.arange(N) / float(N))*freq

        imax = int(np.ceil( N*fmax / freq))  if fmax is not None else None
        imin = int(np.ceil( N*fmin / freq))  if fmin is not None else 0
        return DataX( fft[imin:imax], ["freq"])
    
    def getPSD(self, dt=None, freq=None, cut=False, fmin=None, fmax=None, **kwargs):
        if freq is None:
            freq = self.getFrameRate() 
        if dt is None:
            dt = 1./freq        
        tf = self.getFFT(freq=freq, cut=cut, fmin=fmin, fmax=fmax, **kwargs ).A
        return  DataX( abs(tf*tf.conjugate())*dt, ["freq"])
        
    getSignalHeader = getHeaderCopy
    getMomentHeader = getHeaderCopy
    getStdHeader    = getHeaderCopy
    getNHeader      = getHeaderCopy

class _Signal_(_TwoD_,_NoData_):    
    """
    Contain a twod image with averaged (or not) signal
    """
    axes = [Y,X]
    getSignal = getSignal
    def getSignalData(self, **kwargs):
        if (TIME+"_reduce" in kwargs) or (TIME+"_idx" in kwargs):
            raise KeyError("Combined images does not have temporal axis")
        return self.getData(**kwargs)
    getSignalHeader = getHeaderCopy
    
    

class _Flux_(_TwoD_):
    """ Contain a flux image. Flux is derived from a linear fit of several exposures 
    signal = Flux *DIT + Bias 
    """
    axes = [Y, X]
    getFluxData = getData
    getFluxHeader = getHeaderCopy
    getFlux = getFlux
    
    
class _Bias_(_TwoD_):
    """ Bias here is the image of the flux derived  from a linear fit of several exposures.
    signal = Flux *DIT + Bias 
    """
    axes = [Y, X]
    getBiasData   = getData
    getBiasHeader = getHeaderCopy
    getBias       = getBias 
    
    
class _FluxCube_( _ThreeD_, _Flux_):
    axes = [POLAR, Y, X]
class _BiasCube_( _ThreeD_, _Bias_):
    axes = [POLAR, Y, X]
class _PolarCube_(_OneD_):
    axes = [POLAR]


class _Moment_(_TwoD_, _NoData_):
    axes = [Y,X]
    def getMjd(self):
        return self.getMjdObs()
    getMomentData = getData
    getMomentHeader = getHeaderCopy
    getMoment = getMoment
        
class _M2_(_Moment_):
    """ Momment 2 of combined images """
    axes = [Y,X]  
    def getMomentData(self, moment=2, **kwargs): 
        if (TIME+"_reduce" in kwargs) or (TIME+"_idx" in kwargs):
            raise KeyError("Combined images does not have temporal axis")
        if moment!=2: raise ValueError("Moment should be equal to 2 for this object")
        return self.getData(**kwargs)
    def getM2Data( self, **kwargs):
        return self.getMomentData( 2, **kwargs)
    getM2 = getM2
    getM2Header = getHeaderCopy
    def getStdData(self,**kwargs):
        return np.sqrt(self.getM2Data(**kwargs))
    getStd = getStd
    getStd = getHeaderCopy
    
class _M2Error_(_TwoD_, _NoData_):
    axes = [Y,X]
    getM2ErrorData = getData
    getM2Error = getM2Error
    getM2ErrorHeader = getHeaderCopy
        
class _M3_(_Moment_):
    def getMomentData(self, moment=3, **kwargs):
        if (TIME+"_reduce" in kwargs) or (TIME+"_idx" in kwargs):
            raise KeyError("Combined images does not have temporal axis")
        if moment!=3:
            raise ValueError("Moment should be equal to 3 for this object")
        return self.getData(**kwargs)
    getM3Data = getData
    getM3Header = getHeaderCopy
    getM3 = getM3
    
class _M4_(_Moment_):
    def getMomentData(self, moment=4, **kwargs):
        if (TIME+"_reduce" in kwargs) or (TIME+"_idx" in kwargs):
            raise KeyError("Combined images does not have temporal axis")
        if moment!=4: raise ValueError("Moment should be equal to 4 for this object")
        return self.getData(**kwargs)  
    getM4Data = getData
    getM4Header = getHeaderCopy
    getM4 = getM4

class _N_(_TwoD_,_NoData_):
    axes = [Y, X]
    def getNData(self, **kwargs):
        if (TIME+"_reduce" in kwargs) or (TIME+"_idx" in kwargs):
            raise KeyError("Combined images does not have temporal axis")
        return self.getData(**kwargs)
    getN = getN
    getNHeader = getHeaderCopy
    
    def getMjd(self):
        return self.getMjdObs()


class _Std_(_TwoD_, _NoData_):
    axes = [Y, X]    
    getStdData = getData
    getStd     = getStd
    getStdHeader = getStdHeader
    
    def getMomentData(self, moment=2, **kwargs):
        if moment>2:
            raise Exception("All momement >2 are lost here")
        data = self.getStdData(**kwargs)
        return data*data

    
class _Score_(_TwoD_):
    axes = [Y, X]
    def bestNear(self, yx, dx=4,dy=4, N=0):
        sx = dx/2
        sy = dy/2
        data = self.data
        y, x = np.mgrid[ 0:data.shape[0], 0:data.shape[1] ]
        s = ( slice(yx[0]-sy,yx[0]+sy), slice(yx[1]-sx,yx[1]+sx))
        data, y, x = data[s], y[s], x[s]
        y, x = y[data>-999], x[data>-999]
        data = data[data>-999]
        if not len(data): return None
        st = data.argsort()
        data, y, x = data[st][N], y[st][N], x[st][N]
        return (y,x), data


class _CubeCapabilities_(object):
    def getSignalData(self,**kwargs):
        """ Average the time axes of the cube and return data """
        return self.getData(**kwargs).mean(axis=self.axis[0])
    getSignalHeader = key_decorator("getHeader", on=0, inarray=False)
    
    def getMomentData(self, moment, **kwargs):
        """ Compute the moment on the last dimention of the cube """
        return computeMoment_datax(
            self.getData(**kwargs), 
            axis=self.axes[0]
        )    
    getMomentHeader = getMomentHeader
    
    def getStdData(self, **kwargs):
        """ Compute the standart deviation on the last dimention of the cube """
        return self.getData(**kwargs).std(axis=self.axis[0])
    getStdHeader = getStdHeader
            
    def getNData(self, **kwargs):
        """ Return an image containing the number of data on the time dimtion of the cube"""
        shape = self.getShape()
        N    =  DataX(np.zeros( shape[1:], dtype=int), self.axes[1:])
        N[:] = shape[0]
        return self._return_array_data(N, kwargs)    
    getNHeader = getNHeader
    
    def getJitters(self, clipmin=None, clipmax=None, **kwargs):
        """ Return the jitter (baricenter) of a cube """
        data = getJitters(self.getData(**kwargs), clipmin=clipmin, clipmax=clipmax)
        return data
    
class _Cube_(_ThreeD_,_CubeCapabilities_, _ImageDerived_, _ParamsDerived_):
    """ 
    function for a [TIME, "y", "x"] cube 
    """    
    axes = [TIME, Y, X]
    
    def getMjd(self, **kwargs):
        N = self.getShape(self.axes.index['time'])
        mjd = DataX(self.getMjdObs()+np.arange(N)*(self.getCycleTime()/(24.*3600)), [TIME])
        return mjd.apply(**kwargs)



class _LongDarkSignalCube_(_Cube_):
    """ 
    Contains cube of signal obtained during long dark. Several files 
    are concatained into a 4D cube with axes: "file", TIME, "y", "x"
    """
    axes = [FILE, TIME, Y, X]
    getSignalData = getSignalData
    
    def getMomentData(self, moment, **kwargs):
        raise Exception("Moments are lost in this HDU")    
    def getStdData(self, **kwargs):
        raise Exception("Std are lost in this HDU")
    def getStdHeader(self, **kwargs):
        raise Exception("Std are lost in this HDU")
    
    
class _LongDarkStdCube_(_Cube_):
    """ 
    Contains cube of STD obtained during long dark. Several files 
    are concatained into a 4D cube with axes: "file", TIME, "y", "x"
    """
    axes = [FILE, TIME, Y, X]
    def getSignalData(self, **kwargs):
        raise Exception("This HDU does not contain sgnal information, only std")
    def getStdData(self, **kwargs):
        if TIME+"_reduce" in kwargs or FILE+"_reduce" in kwargs:
            self.say("WARNING in getStd time_reduce and file_reduce ignored", -1)
        return np.sqrt(self.getMoment(2, **kwargs))
    
    def getMomentData(self, moment, **kwargs):
        if moment>2:
            raise Exception("%d do not possess moments of order >2, sorry"%self.__class__)
        if TIME+"_reduce" in kwargs or FILE+"_reduce" in kwargs:
            self.say("WARNING in getMoment time_reduce and file_reduce ignored", -1)        
                
        kwargs.pop(TIME+"_reduce", None)
        kwargs.pop(FILE+"_reduce", None)
        data = self.getData(**kwargs).transform( [(TIME,FILE, TIME),None] )
        # quadratic sum of variance (can do it because N is always the same for each bins)
        data2 = data*data
        var = 1./float(data.shape[0])* (data2.sum(axis=TIME) ) 
        return var

class _SignalCube_(_Signal_):
    axes = [EXPO, Y, X]
class _M2Cube_(_M2_):
    axes = [EXPO, Y, X]
class _M3Cube_(_M3_):
    axes = [EXPO, Y, X]
class _M4Cube_(_M4_):
    axes = [EXPO, Y, X]
class _NCube_(_N_):
    axes = [EXPO, Y, X]


class _Gain_(_TwoD_):
    axes = [Y, X]
    getGainData   = getData
    getGainHeader = getHeaderCopy
    getGain = getGain

class _GainM_(_TwoD_):
    axes = [Y, X]
    getGainMData   = getData
    getGainMHeader = getHeaderCopy
    getGainM = getGainM

    
class _Noise2_(_TwoD_):
    axes = [Y, X]
    getNoise2Data   = getData
    getNoise2Header = getHeaderCopy
    getNoise2 = getNoise2

class _Noise_(_TwoD_):
    axes = [Y, X]
    getNoiseData   = getData
    getNoiseHeader = getHeaderCopy
    getNoise = getNoise
    
class _ENoise_(_TwoD_):
    axes = [Y, X]
    getENoiseData   = getData
    getENoiseHeader = getHeaderCopy
    getENoise = getENoise
    
class _Coeff_(_ThreeD_):
    axes = ["degree", Y, X]

class _GainCube_(_ThreeD_):
    axes = [POLAR,Y, X]
    getGainData   = getData
    getGainHeader = getHeaderCopy
    getGain = getGain
    
class _Noise2Cube_(_ThreeD_):
    axes = [POLAR,Y,X]
    getNoise2Data   = getData
    getNoise2Header = getHeaderCopy
    getNoise2 = getNoise2
    
class _NoiseCube_(_ThreeD_):
    axes = [POLAR,Y,X]
    getNoiseData   = getData
    getNoiseHeader = getHeaderCopy
    getNoise = getNoise
    

class _Jitter_(_TwoD_):
    axes = [TIME, "yx"]
    
    def getX(self, **kwargs):
        kwargs["xy_index"] = 1
        return self.getData(**kwargs)
    def getY(self, **kwargs):
        kwargs["xy_index"] = 0
        return self.getData(**kwargs)
    
    def getXfft(self, cut=False,fmin=None, fmax=None,  **kwargs):
        return getJitterFFT(self.getX(**kwargs), cut=cut, fmin=fmin, fmax=fmax)
    def getYfft(self, cut=False,fmin=None, fmax=None, **kwargs):
        return getJitterFFT(self.getY(**kwargs), cut=cut, fmin=fmin, fmax=fmax)
    def getXpsd(self, cut=False, fmin=None, fmax=None,**kwargs):
        return getJitterPSD(self.getX(**kwargs), 1.0/self.getKey("FREQ"), cut=cut, fmin=fmin, fmax=fmax, freq=self.getFreq(cut=cut))
    def getYpsd(self, cut=False, fmin=None, fmax=None,**kwargs):
        return getJitterPSD(self.getY(**kwargs),  1.0/self.getKey("FREQ"), cut=cut, fmin=fmin, fmax=fmax, freq=self.getFreq(cut=cut))
    def getFreq(self, cut=False, fmin=None, fmax=None):
        N = self.getShape()[1]
        N2 = N/2 if cut else N
        freq = self.getKey("FREQ")
        f = (np.arange(N2) / float(N))*freq 
        if fmin is not None:
             f = f[f>=fmin]
        if fmax is not None:
             f = f[f<=fmax]
        return f      
    def getScanNum(self):
        return np.arange( self.getKey("NAXIS2")) 

####################################################
#
#  Group entities 
#
####################################################



def getitem(self, items):
    """
    getitem( lst, item)
    mimic fancy numpy index for detector lists 
    """
    if not isinstance( items, slice) and not hasattr( items, "__iter__"):
        return list.__getitem__(self, items)
    
    if isinstance( items, np.ndarray):
        items = np.arange( len(self))[ items].flat
    else:
        items = np.arange( len(self))[ np.r_[items] ].flat
    return self.__class__( [self[i] for i in items] )


class _List_(list):
    def __init__(self, lst, *args, **kwargs):
        axis = kwargs.pop("axis", None)
        list.__init__(self, lst,*args, **kwargs)
        self._subclass_list(self)
        if axis is not None: self.axes = [axis]
        
    def __getitem__(self,item):
        if isinstance(item, basestring):
            names  = [subs.name for subs in self]
            try:
                item = names.index(item)
            except ValueError:
                raise ValueError("data entity  '%s' not found"%item)
        return list.__getitem__( self, item)
    
    say = objsay
    _getClass_ = staticmethod(getClass)
    @staticmethod
    def isList():
        return True
    @staticmethod
    def isListList():
        return False
    @staticmethod
    def isEntity():
        return False

    
    @classmethod
    def _subclass_list(cls,lst):
        if not hasattr(cls,"subclasses"): return 
        for index,hdu in enumerate(lst):
            subkey = hdu.name if hdu.name in cls.subclasses else index if index in cls.subclasses else hdu.__class__ if hdu.__class__ in cls.subclasses else None
            
            if subkey in cls.subclasses:
                lst[index].__class__  = cls.subclasses[subkey]

    def _getAxis(self,axis=None):
        if axis is not None:
            return [axis]
        return self.axes

    
    def finalise(self, data, inarray=True, inaxis=None):
        if inarray is True:
            return self.inarray(data, axes=self._getAxis(inaxis))
        if isinstance(inarray, basestring):
            return self._getClass_(inarray)(data)    
        return data

    @staticmethod
    def inarray(data, axes=None):        
        if axes and len(axes):       
            return dataxconcat(data, axes[0])
        
        return np.asarray(data)
    
    @staticmethod
    def isarray(data):
        return isinstance( data, np.ndarray)
        
    def getDataShape(self):
        if not len(self): return tuple()
        shapes = self.getShape()
        dshape = self[0].getDataShape()
        if isinstance( shapes, tuple):
            return shapes
        if (dshape != shapes).sum():
            raise Exception("Seems that individual data have not the same shape")
        return tuple( [len(self)]+list(dshape) )
    
    def getDataAxes( self):
        if hasattr(self, "data_axes"): return self.data_axes
        if not len(self): return self.axes
        return self.axes+self[0].getDataAxes()
    newHeader     = key_decorator("newHeader", 0, inarray=False)
    getAttr       = key_decorator("getAttr", inarray=False)
    callAttr      = key_decorator("callAttr", inarray=False)


class _ListCapabilities_(object):
    _on_default       = 0
    _on_data_default  = 0
    _on_key_default   = 0
    def add_scalar_capability(self, name, func, on=False, inarray=True, inaxis=None):
        if on is False:
            on = self._on_key_default
       
        self._add_key_capability(name, on=on, inarray=inarray, inaxis=inaxis)
        if on is None:
            class_set = set()            
            for child in self:
                if child.__class__ in class_set: continue
                class_set.add( child.__class__)
                child._add_data_capability(name, func)
        else:
            self[on]._add_key_capability(name, func)
    
    def add_data_capability(self, name, func,headerfunc=newHeader,
                            on=False, clsname=None, add_scalar=ADD_SCALAR):
        
        if on is False:
            on = self._on_data_default
       
        self._add_data_capability(name, on=on,clsname=clsname,add_scalar=add_scalar)
        if on is None:
            class_set = set()
            for child in self:
                if child.__class__ in class_set: continue
                class_set.add( child.__class__)
                child._add_data_capability(name, func, headerfunc, clsname=clsname, add_scalar=add_scalar)
        else:
            self[on]._add_data_capability(name, func, headerfunc, clsname=clsname, add_scalar=add_scalar)

    
    def add_scalar_data_capability(self, name, reduces=REDUCE_TO_SCALAR, inaxis=None, on=False):
        if on is False:
            on = self._on_data_default
        self._add_scalar_data_capability(name, reduces=reduces, inaxis=inaxis, on=on)
        if on is None:
            class_set = set()
            for child in self:
                if child.__class__ in class_set: continue
                class_set.add( child.__class__)
                child._add_scalar_data_capability(name,reduces=reduces)
        else:
            self[on]._add_scalar_data_capability(name, reduces=reduces)
    
    @classmethod
    def _add_key_capability(cls,name, on=False, inarray=True, inaxis=None):
        if on is False:
            on = cls._on_key_default
        getname  = "get"+name.capitalize()
        if on is None:
            # if on is None means that it loop over the list
            # so create the with decorator and the set decorator
            withname = "with"+name.capitalize()
            setname  = "get"+name.capitalize()+"Set"
            setattr(cls, withname, with_decorator(getname))
            setattr(cls, setname , set_decorator(getname, inaxis=inaxis))
        
        setattr(cls, getname, key_decorator(getname, on=on, inarray=inarray))
    
    @classmethod
    def _add_data_capability(cls, name, on=False, clsname=None, add_scalar=ADD_SCALAR):
        if on is False:
            on = cls._on_data_default            
        getname    = "get"+name.capitalize()+"Data"
        headername = "get"+name.capitalize()+"Header"
        setattr(cls, getname   ,  data_decorator(getname   ,   on=on) )
        
        if clsname:
            ###
            # if a class name is present means it has a method to encapsulate
            # in a new DataEntity (e.g. HDU or other)
            encapsname    = "get"+name.capitalize()
            def tmp_get(self, *args, **kwargs):                
                return self._getClass_(clsname)( getattr(self,getname)(*args, **kwargs),
                                                 getattr(self,headername)())
            setattr(cls, encapsname, tmp_get)
            cls._add_headerfunc(headername, on = on)

        if add_scalar:
            cls._add_scalar_data_capability(name, on=on)
                
    @classmethod
    def _add_scalar_data_capability(cls, name, reduces=REDUCE_TO_SCALAR, on=False, inaxis=None):
        if on is False:
            on = cls._on_data_default  
        for fname in reduces:
            getname  = "get"+fname.capitalize()+name.capitalize()
            setattr(cls, getname , data_decorator(getname ))
            if on is None:
                withname = "with"+fname.capitalize()+name.capitalize()
                setname  = "get"+fname.capitalize()+name.capitalize()+"Set"        
                setattr(cls, withname, with_decorator(getname))
                setattr(cls, setname , set_decorator(getname, inaxis=inaxis))
            
            
    @classmethod
    def _add_headerfunc(cls, headername, on=False):
        if on is False:
            on = cls._on_data_default
        if on is None:
            setattr(cls, headername,  header_decorator(headername))
        else:
            setattr(cls, headername,  key_decorator(headername,   on=on) )
    
    getShape       = key_decorator("getShape")
    getData        = data_decorator("getData")
    getKey         = key_decorator("getKey")
    getKeyComment  = key_decorator("getKeyComment")
    getHeader      = key_decorator("getHeader", inarray=False)
    getFilePath      = key_decorator("getFilePath")
    
class _ImageCapabilities_(_ListCapabilities_):
    _on_default       = 0
    _on_data_default  = 0
    _on_key_default   = 0
    
        
    getIllumination = key_decorator("getIllumination")
    getShutter      = key_decorator("getShutter")
    getShutters     = key_decorator("getShutters")
    getPolar        = key_decorator("getPolar")
    
    getJitter       = data_decorator("getJitter")
    getMedianData   = data_decorator("getMedianData")
    getMeanData     = data_decorator("getMeanData")
    getStdData      = data_decorator("getStdData")
    getStdHeader    = key_decorator("getStdHeader"  , 0,  inarray=False )
    getSignalData   = data_decorator("getSignalData")
    getSignalHeader = key_decorator("getSignalHeader", 0, inarray=False )
    getMomentData   = data_decorator("getMomentData")
    getMomentHeader = key_decorator("getMomentHeader", 0, inarray=False )

    getNData        = data_decorator("getNData")
    getNHeader = key_decorator("getNHeader", 0, inarray=False )
        
    # the relevent data shape or axes are the one of the unique member
    getDataAxes     = key_decorator("getDataAxes"  , inarray=False )
    getDataShape    = key_decorator("getDataShape" , inarray=False )
    
    getXY = data_decorator("getXY")
    getX  = data_decorator("getX" )
    getY  = data_decorator("getY" )
    
    getOffset = data_decorator("getOffset")
    setOffset = data_decorator("setOffset",inarray=False)
    delOffset = data_decorator("delOffset",inarray=False)
    getScale  = data_decorator("getScale")
    setScale  = data_decorator("setScale", inarray=False)
    delScale  = data_decorator("delScale", inarray=False )

    
class _Image_(_List_, _ImageDerived_, _ParamsDerived_,_ImageCapabilities_):
    """ Class for a list containing THE image or cube in the first member (e.i. one HDU) """
    def getImages(self, **kwargs):    
        return self.getData(**kwargs)

class _Combined_(_List_, _ImageCapabilities_, _ImageDerived_, _ParamsDerived_, baseplot._Combined_ ):
    """
    class containing 5 or 6  members:
    0: the averaged signal 
    1: the moment of order 2
    2: the moment of order 3
    3: the moment of order 4
    4: the number of pixel used to compute 0,1,2,3 and 4    
    """
    subclasses = {                   
        0:_Signal_,        
        1:_M2_,
        2:_M3_,
        3:_M4_,
        4:_N_
    }    
    axes = ["type", Y,X]
    getData   = data_decorator("getData")
    
    getSignalData   = data_decorator("getSignalData",   on=0, inarray=True)
    getSignalHeader = key_decorator("getSignalHeader", on=0, inarray=False)
            
    hmoments = {2:1,3:2,4:3}
    
    def getMomentData(self, moment, **kwargs):
        return self[self.hmoments[moment]].getMomentData(moment, **kwargs)
    def getMomentHeader(self, moment, **kwargs):            
        return self[self.hmoments[moment]].getHeader(**kwargs)
    
    def getStdData(self, **kwargs):
        m2 =  self.getM2Data(**kwargs)
        return np.sqrt(m2)
    def getStdHeader(self, **kwargs):
        return self.getM2Header(**kwargs)
    
    getNData   = data_decorator("getNData",  on=4   )
    getNHeader = key_decorator("getNHeader",on=4, inarray=False)
    
    
    
class _Combined2_(_Combined_):
    """
    Pionier/Gerard type of file     
    class containing 5 or 6  members:
    0: the averaged signal 
    1: the moment of order 2
    2: the moment of order 3
    3: the moment of order 4    
    """
    subclasses = {                   
        0:_Signal_,        
        1:_M2_,
        2:_M3_,
        3:_M4_     
    }    
    def getNData(self, **kwargs):
        shape = self[0].getShape()
        Nv = self[0].getNDit()
        N =  np.zeros( shape, dtype=int)
        N[:] = Nv
        return self[0]._return_array_data(N, kwargs)
    


 
    
    
class _Frames_(_List_,_CubeCapabilities_, _ImageCapabilities_, _ImageDerived_, _ParamsDerived_, baseplot._Combined_):
    """
    Class for a list of Frames (e.g. long hdu fits file) 
    """
    axes = [TIME,Y, X] # the frame number is time
        
    def getMomentHeader(self, moment, **kwargs):
        h = self[0].header.__class__()
        h.set("MOMENT", moment)
        return h
    
    def getShape(self):
        if not len(self): return tuple()
        return tuple( [len(self)]+list( self[0].getShape()) )

    def getNData(self, **kwargs):
        if not len(self): return self.finalise([])
        N = DataX(np.zeros( self[0].getShape() , dtype=int), self[0].axes).apply(**kwargs)
        N[:] = len(self)
        return N
    
    def getNHeader(self, **kwargs):
        return self.header[0].__class__()
    
    def getMjd(self, **kwargs):
        N   = len(self)
        mjd = DataX(self[0].getMjdObs()+np.arange(N)*(self.getCycleTime()/(24.*3600)), self.axes[0])
        return mjd.apply(**kwargs)
    
    def initCombinedData(self, moments=(2,3,4), num=0, clipping=None, offset=None):        
        fh  = self[num]        
        lst = [pf.PrimaryHDU( header=fh.header.copy())]
        lst[0].name = "mean"
        for m in moments:
            lst.append(pf.ImageHDU(name="M%d"%m ))
        lst.append(pf.ImageHDU(name="N" ))        
        self.combinedData = CombinedImage(lst)
        
        data = fh.data.copy()
        if offset is not None:
            data -= offset
        
        cl = clipping(data) if clipping else True
        self.sumdata = np.ndarray(data.shape, dtype=np.int32) # need int 32 to 
        # suport addition
        self.sumdata[:] = data*cl
        
        self.combinedData['mean'].data = np.float32(data*cl)
        for m in moments:
            self.combinedData["M%d"%m].data = np.zeros(data.shape, dtype=np.float32)
        
        if clipping:
            self.combinedData["N"].data = cl
        else:
            self.combinedData["N"].data = np.ones(data.shape, dtype=int)



class _Ptc_(_List_, _ListCapabilities_, baseplot._Ptc_):
    subclasses = {
        0:_Gain_,
        1:_Noise2_,
        2:_Noise_,
        3:_N_
    }
    hgain = 0
    hnoise2 = 1
    hnoise_min = 2
    hN = 3
    # datashape is the first image shape
    getDataAxes    = key_decorator("getDataAxes", on=0, inarray=False)
    getDataShape   = key_decorator( "getDataShape", on=hgain , inarray=False)
    
    getGainData       = data_decorator( "getData", on=hgain      )
    getGainHeader     = key_decorator( "getHeader", on=hgain , inarray=False)
    getNoise2Data     = data_decorator( "getData", on=hnoise2    )
    getNoise2Header   = key_decorator( "getHeader", on=hnoise2 , inarray=False)
    getNoiseMinData   = data_decorator( "getData", on=hnoise_min )
    getNoiseMinHeader = key_decorator( "getHeader", on=hnoise_min , inarray=False)
    getNData          = data_decorator( "getData", on=hN )
    getNHeader        = key_decorator( "getHeader", on=hN, inarray=False)
    
    getGain      = getGain
    getNoise2    = getNoise2
    getNoiseMin  = getNoiseMin
    getN         = getN
    
        
class _Linearity_(_List_, _ListCapabilities_, baseplot._Linearity_):
    subclasses = {
        0:_Flux_,
        1:_Bias_,
        2:_N_
    }

    # datashape is the first image shape
    getDataAxes    = key_decorator("getDataAxes", on=0, inarray=False)
    getDataShape     = key_decorator( "getDataShape", on=0 , inarray=False)
    
    getFluxData   =  data_decorator( "getData", on=0)
    getFluxHeader =  key_decorator( "getHeader", on=0, inarray=False)    
    getBiasData   =  data_decorator( "getData", on=1)
    getBiasHeader =  key_decorator( "getHeader", on=1, inarray=False) 
    getNData      =  data_decorator( "getData", on=2)
    getNHeader    =  key_decorator( "getHeader", on=2, inarray=False)
    
    getFlux =  getFlux
    getBias =  getBias
    getN    =  getN


class _TableImage_(_List_, baseplot._CubeImage_, _ImageCapabilities_, _ImageDerived_, _ParamsDerived_):
    _data_hdu = "IMAGING_DATA"
    _on_data_default = _data_hdu        
    getWinN   = data_decorator("getWinN") 
    getN      = list_decorator("getN"   ,   on=_data_hdu) 
    def getCycleTime(self):
        return self.getKey("DET READOUT CYCLETIME", hdu=0)
    def getFrameRate(self):
        return  1./self.getCycleTime()
    def getConfig(self):
        return _KeyEntity_.getConfig(self)+[
            ("STROKE1", self.getKey("DET SCAN DL1 STROKE")),
            ("STROKE2", self.getKey("DET SCAN DL2 STROKE")),
            ("STROKE3", self.getKey("DET SCAN DL3 STROKE")),
            ("STROKE4", self.getKey("DET SCAN DL4 STROKE"))
        ]
    
class _DarkImage_(_List_, baseplot._Combined_, _ImageDerived_, _ParamsDerived_):
    getSignalData   = data_decorator("getSignalData", 0)
    getSignalHeader = key_decorator("getSignalHeader", 0, inarray=False)
    getMomentData   = data_decorator("getMomentData", 1)
    getMomentHeader = key_decorator("getMomentHeader", 1, inarray=False)    
    getStdData      = data_decorator("getStdData"   , 1)
    getStdHeader    = key_decorator("getStdtHeader", 1, inarray=False) 
    
    def getNData(self, **kwargs):
        raise Exception("N information lost")
    def getMjd(self,**kwargs):
        raise Exception( "No MJD temporal information in this fits file")


class _CubeImage_(_List_, _ImageCapabilities_,  _ImageDerived_, _ParamsDerived_,baseplot._CubeImage_):
    """
    Class for a cube as first member 
    """
    # datashape is the first image shape
    pass 
    
class _CubeImage2_(_List_,  _ImageCapabilities_, baseplot._CubeImage_, _ImageDerived_, _ParamsDerived_):
    """
    This is a 2 member list. 
    The first one is a cube of images the second one a cube of std 
    Usefull for a time resampled long cube (e.g. 4096 images to 124) 
    """
    # datashape is the first image shape
    
    
    getSignalData   = data_decorator("getSignalData", 0)
    getSignalHeader = key_decorator("getSignalHeader", 0, inarray=False)
    getMomentData   = data_decorator("getMomentData", 1)
    getMomentHeader = key_decorator("getMomentHeader", 1, inarray=False)
    
    getStdData     = data_decorator("getStdData"  , 1)
    getStdHeader   = key_decorator("getStdtHeader", 1, inarray=False) 
    getMjd         = key_decorator("getMjd"   , 2)
        
class _JitterList_(_List_, baseplot._Jitter_):
    getX = data_decorator("getX",0)
    getY = data_decorator("getY",0)
    getXfft = data_decorator("getXfft",0)
    getYfft = data_decorator("getYfft",0)
    getXpsd = data_decorator("getXpsd",0)
    getYpsd = data_decorator("getYpsd",0)
    getFreq = data_decorator("getFreq",0)
    getScanNum = data_decorator("getScanNum",0)

###########################################################################
#
#   List of list 
#
############################################################################



class _ListList_(object):
    """
    High level Function for list of fitsfile cube or images
    """
    verbose = 1
    say = objsay
    _getClass_ = staticmethod(getClass)
    
    def idx(self, item):
        return getitem(self, item)
    
    @staticmethod
    def isList():
        return False
    @staticmethod
    def isListList():
        return True
    @staticmethod
    def isEntity():
        return False
    
    def _getAxis(self,axis=None):
        if axis is not None:
            return [axis]
        return self.axes
    def finalise(self, data, inarray=True, inaxis=None):
        if inarray is True:
            return self.inarray( data, axes=self._getAxis(inaxis))
        if isinstance(inarray, basestring):
            return self._getClass_(inarray)(data)
        
        return self.__class__(data)
    @staticmethod
    def inarray(data, axes=None):        
        if axes and len(axes):
            return dataxconcat(data, axes[0])        
        return np.asarray(data)
    
    newHeader     = key_decorator("newHeader", 0, inarray=False)
    getAttr       = key_decorator("getAttr", inarray=False)
    callAttr      = key_decorator("callAttr", inarray=False)
    getShape      = key_decorator("getShape")
    def getDataShape( self):
        if not len(self): return tuple()
        shapes = self.getShape()
        dshape = self[0].getDataShape()
        if isinstance( shapes, tuple):
            return shapes
        if (dshape != shapes).sum():
            raise Exception("Seems that individual data have not the same shape")
        return tuple( [len(self)]+list(dshape) )
    
    def getDataAxes( self):
        if hasattr(self, "data_axes"): return self.data_axes
        if not len(self): return self.axes
        return self.axes+self[0].getDataAxes()

    def order(self, method, *args, **kwargs):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        values = method(*args, **kwargs)
        indexes = np.argsort(values)
        return self.__class__ (  [self[i] for i in indexes] )
    
    def filter(self, method):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        return self.__class__ ( [fts for fts in self if method(fts)] )
    

class _LinearFitCapability_(object):    
    def linearfit(self, xmethod, ymethod, xrange=None, yrange=None, kw_x=None, kw_y=None, **kwargs):
        #self._parseDefaultGet(kwargs)
                
        if issubclass( type(xmethod), str):
            xmethod = self.__getattribute__(xmethod)
        if issubclass( type(ymethod), str):
            ymethod = self.__getattribute__(ymethod)
        kw_x = kw_x or kwargs
        kw_y = kw_y or kwargs
        x = xmethod(**kw_x)
        y = ymethod(**kw_y)
        
        ishape = list(x.shape)
        iaxes  = x.axes if hasattr(x, "axes") else range(len(ishape))
        ishape.remove(ishape[0])
        ishape = tuple(ishape) 
        
        N = len(x)
        x = np.asarray(x).reshape( (N,-1)).T
        y = np.asarray(y).reshape( (N,-1)).T
        
        global vectorialized
        if vectorialized:
            coeff = vectorialized_linearfit( np.asarray(x), np.asarray(y), 
                                 xrange, yrange
                             ).T
        else:
            coeff = linfitarray( np.asarray(x), np.asarray(y), 
                                 xrange, yrange
                             ).T
        
        #coeff = DataX( coeff.reshape( [3]+list(ishape)), ["coeff"]+iaxes)
        coeff = coeff.reshape( [3]+list(ishape))
        return coeff
        
class _PtcCapability_(_LinearFitCapability_):
    """ A set of functions dedicated to PTC computation """
    _ptc_hash = 0

    def getMinDitStd(self, **kwargs):
         dit = self.getDit()
         minditimg = self.idx(np.argmin(dit))
         return minditimg.getStd(**kwargs)
  
    def getPtc(self, signalrange=None, m2range=None, force=False):
        h = (signalrange, m2range)
        if h!=self._ptc_hash or force:            
            self.ptc = self.compute_ptc(signalrange=signalrange, m2range=m2range)           
            self._ptc_hash = h
        return self.ptc
    
    def compute_ptc(self, signalrange=None, m2range=None, **kwargs):
        xrange = signalrange
        yrange = m2range

        medbox = kwargs.pop("medbox", None)

        polar = 0
        try:
            polars = self.getPolar()
            if len(set(polars))>1:
                self.say("Found more than one detector polar in this list: %s"%(set(polars)), vtype=WARNING)
            polar = max(polars)
        except:
            self.say("Cannot determine detector polar", vtype=WARNING)

        self.say("Computing full frame PTC with signalrange=%s, m2range=%s for polar %d"%(signalrange, m2range, polar), 1)
            
        coeff = self.linearfit( "getSignalData", "getM2Data", **kwargs)    
        noisemin = self.getMinDitStd(**kwargs)
        k     = 1./coeff[0]
        eta2  = (coeff[1]*k**2)
        N = coeff[2]
        
        if xrange is None:
            xrange = (None, None)
        if yrange is None:
            yrange = (None, None)  
        
        if medbox is not None:
            from scipy import signal as sig
            k    = sig.medfilt2d(np.asarray(k),    medbox)
            eta2 = sig.medfilt2d(np.asarray(eta2), medbox) 
        
        k, eta2, N = self._getClass_("gain",True)(k), self._getClass_("noise2")(eta2), self._getClass_("n")(N)
        
        
        k.setKey("XFITMIN", xrange[0] or "None" )
        k.setKey("XFITMAX", xrange[1] or "None" ) 
        k.setKey("YFITMIN", yrange[0] or "None" )
        k.setKey("YFITMAX", yrange[1] or "None" )
        k.setKey("HIERARCH ESO DET POLAR", polar)
        
        if hasattr( self, "makeKeysTable"):
            return self._getClass_("ptc")( [k, eta2, noisemin, N, self.makeKeysTable()])
        else:
            return self._getClass_("ptc")( [k, eta2, noisemin, N])
        
    
    def getGainData(self, **kwargs):
        return self.getPtc(signalrange=kwargs.pop("signalrange",None), 
                           m2range=kwargs.pop("m2range",None)
        ).getGainData(**kwargs)            
    def getGain(self, **kwargs):
        return self.getPtc(signalrange=kwargs.pop("signalrange",None), 
                           m2range=kwargs.pop("m2range",None)
        ).getGain(**kwargs) 
        
    def getNoise2Data(self, **kwargs):
        return self.getPtc(signalrange=kwargs.pop("signalrange",None), 
                           m2range=kwargs.pop("m2range",None)
                       ).getNoise2Data(**kwargs)
    def getNoise2(self, **kwargs):
        return self.getPtc(signalrange=kwargs.pop("signalrange",None), 
                           m2range=kwargs.pop("m2range",None)
                       ).getNoise2(**kwargs)
    
    def getNoiseData(self, **kwargs):
        return self.getStdData(**kwargs)
    
    def getNoise(self, **kwargs):
        return self._getClass_("noise")(self.getNoiseData(**kwargs))
    
    def getENoiseData(self, **kwargs):
        return self.getStdData(**kwargs)*self.getGainData(**kwargs)
    def getENoise(self, **kwargs):
        return self._getClass_("enoise")( self.getENoiseData(**kwargs))
    
    def getNoiseMinData(self, **kwargs):
        return self.getMinDitStd().getData(**kwargs)
        #return self.getPtc(signalrange=kwargs.pop("signalrange",None), 
        #            m2range=kwargs.pop("m2range",None)
        #).getNoiseMin(**kwargs)
    def getNoiseMin(self, **kwargs):
        return self.getMinDitStd(**kwargs)
        
    def getENoiseMinData(self, gain_reduce=None, **kwargs):
        """ Return Noise in e- """
        kwg = kwargs.copy()
        if gain_reduce:
            kwg["freduce"] = gain_reduce
        return self.getNoiseMinData(**kwargs)*self.getGainData(**kwargs)
    
    def getENoiseMin(self, gain_reduce=None, **kwargs):         
        return self._getClass_("enoise")(self.getENoiseMinData(gain_reduce=gain_reduce, **kwargs))
    


class _LinearityCapability_(_LinearFitCapability_):
    _linearity_hash = 0
    def getLinearity(self, ditrange=None, signalrange=None, force=False):
        """        
        """
        h = (ditrange, signalrange)
        if h != self._linearity_hash or force:
            
            self.linearity = self.compute_linearity(ditrange=ditrange, signalrange=signalrange)
            self._linearity_hash = h        
        return self.linearity
    
    def compute_linearity(self, ditrange=None, signalrange=None,**kwargs):        
        yrange = signalrange
        xrange = ditrange
        
        medbox = kwargs.pop("medbox", None)
                
        if xrange is None:
            xrange = (None, None)
        if yrange is None:
            yrange = (None, None)  

        polar = 0
        try:
            polars = self.getPolar()
            if len(set(polars))>1:
                self.say("Found more than one detector polar in this list: %s"%(set(polars)), vtype=WARNING)
            polar = max(polars)
        except:
            self.say("Cannot determine detector polar", vtype=WARNING)
            
        self.say("Computing full frame Linearity with ditrange=%s, signalrange=%s for polar %d"%(ditrange, signalrange, polar), 1)
            
        coeff = self.linearfit( "getDitData", "getSignalData", 
                                xrange=xrange, yrange=yrange, **kwargs)    
        
        if medbox is not None:
            from scipy import signal as sig
            coeff[0]    = sig.medfilt2d(np.asarray(coeff[0]), medbox)
            coeff[1]    = sig.medfilt2d(np.asarray(coeff[1]), medbox) 
        
        
        flux = self._getClass_("flux", True)(coeff[0])
        bias = self._getClass_("bias")(coeff[1])
        N    = self._getClass_("n")(coeff[2])
        
        flux.setKey("XFITMIN", xrange[0] or "None" )
        flux.setKey("XFITMAX", xrange[1] or "None" ) 
        flux.setKey("YFITMIN", yrange[0] or "None" )
        flux.setKey("YFITMAX", yrange[1] or "None" )
        flux.setKey("HIERARCH ESO DET POLAR", polar)

        if hasattr( self, "makeKeysTable"):
            return self._getClass_("linearity")([flux,bias,N,self.makeKeysTable()])
        else:
            return self._getClass_("linearity")([flux,bias,N])
    def getFluxData(self, **kwargs):
        return self.getLinearity(ditrange=kwargs.pop("ditrange",None), 
                    signalrange=kwargs.pop("signalrange",None)
        ).getFluxData(**kwargs)
    
    def getFlux(self, **kwargs):
        return self.getLinearity(ditrange=kwargs.pop("ditrange",None), 
                    signalrange=kwargs.pop("signalrange",None)
        ).getFlux(**kwargs)
    
    def getEFluxData(self, gain_reduce=None, **kwargs):
        kwg = kwargs.copy()
        if gain_reduce:
            kwg["freduce"] = gain_reduce
        return self.getFluxData(**kwargs)*self.getGainData(**kwg)
    
    def getEFlux(self,  gain_reduce=None, **kwargs):
        return self._getClass_("flux")( self.getEFluxData(gain_reduce=gain_reduce, **kwargs),
                       self.getFluxHeader()
                   )
    
    def getBiasData(self, **kwargs):
        return self.getLinearity(ditrange=kwargs.pop("ditrange",None), 
                                 signalrange=kwargs.pop("signalrange",None)
        ).getBiasData(**kwargs)
    
    def getBias(self, **kwargs):
        return self.getLinearity(ditrange=kwargs.pop("ditrange",None), 
                                 signalrange=kwargs.pop("signalrange",None)
        ).getBias(**kwargs)
    
    

class _ListListCapabilities_(_ListCapabilities_):
    _on_default      = None
    _on_data_default = None
    _on_key_default  = None
    
    def add_scalar_capability(self, name, func, on=False, inarray=True, inaxis=None):        
        self._add_key_capability(name, inarray=inarray, inaxis=inaxis)
        class_set = set()        
        for child in self:
            if child.__class__ in class_set: continue
            class_set.add( child.__class__)
            child.add_scalar_capability(name, func, on=on, inarray=inarray, inaxis=inaxis)
    
    def add_data_capability(self, name, func, headerfunc=newHeader, on=False, clsname=None, add_scalar=ADD_SCALAR):
        self._add_data_capability(name, clsname=clsname, add_scalar=add_scalar)
        class_set = set()
        for child in self:
            if child.__class__ in class_set: continue
            class_set.add( child.__class__)
            child.add_data_capability(name, func,  headerfunc=headerfunc, on=on,clsname=clsname, add_scalar=add_scalar)

    def add_scalar_data_capability(self, name, reduces=REDUCE_TO_SCALAR, inaxis=None, on=False):
        
        self._add_scalar_data_capability(name, reduces=reduces, inaxis=inaxis)        
        class_set = set()
        for child in self:
            if child.__class__ in class_set: continue
            class_set.add(child.__class__)
            child._add_scalar_data_capability(name,reduces=reduces, inaxis=inaxis,on=on)
       
            
class _ImagesCapabilities_(_ListListCapabilities_,_ImageCapabilities_):
    
    
    getXY  = data_decorator("getXY",0)
    getX   = data_decorator("getX",0)
    getY   = data_decorator("getY",0)
    
    
    withDit   = with_decorator("getDit")
    withPolar = with_decorator("getPolar")
    withIllumination = with_decorator("getIllumination")
    withShutters     = with_decorator("getShutters")
    withShutter      = with_decorator("getShutter")
    withMjd          = with_decorator("getMjd")
    withKey          = with_decorator("getKey")
    withMjd          = with_decorator("getMjd")
    withConfig       = with_decorator("getConfig")
    withSignal       = with_decorator("getSignalData",np.mean)
        
    getConfigSet = set_decorator("getConfig" , inaxis=CONFIG)     
    getPolarSet  = set_decorator("getPolar"  , inaxis=POLAR)
    getDitSet    = set_decorator("getDit"    , inaxis=DIT)
    getIlluminationSet = set_decorator("getIllumination", inaxis=ILLUMINATION)
    getShuttersSet     = set_decorator("getShutters", inaxis=CONFIG)
    getKeySet          = set_decorator("getKey", inaxis="key")
    
class _ImagesDerived_(_ImageDerived_):
    pass
    
class _Images_(_ListList_, _ImagesCapabilities_, _ImagesDerived_, _ParamsDerived_,
               _PtcCapability_, _LinearityCapability_,  baseplot._Images_):
    axes = [EXPO]
    
class _CombinedImages_(_Images_):
    axes = [EXPO]

class _CombinedLongDark(_Images_):
    pass

class _PtcImages_(_ListList_, _ParamsDerived_, _ListListCapabilities_):
    axes = [POLAR]
    getGainData   = data_decorator("getGainData")
    getGainHeader = header_decorator("getGainHeader")    
    getGain = getGain
    
    getNoise2Data = data_decorator("getNoise2Data")
    getNoise2Header = header_decorator("getNoise2Header")    
    getNoise2 = getNoise2
    
    getNoiseData = data_decorator("getNoiseData")
    getNoiseHeader = header_decorator("getNoiseHeader")    
    getNoise = getNoise

    getPolar = header_decorator("getPolar")
    
class _LinearityImages_(_ListList_, _ParamsDerived_, _ListListCapabilities_):
    axes = [POLAR]
    getFluxData = data_decorator("getFluxData")
    getFluxHeader = header_decorator("getFluxHeader")    
    getFlux = getFlux
    
    getBiasData   = data_decorator("getBiasData")
    getBiasHeader = header_decorator("getBiasHeader")
    getBias = getBias
    
    getPolar = header_decorator("getPolar")
    
class _ImagingDataList_(_Images_):
    axes = [WIN]
        
class _ImagingData_(_DataEntity_, _ImagesCapabilities_, _ImagesDerived_):
    """VLTi IMAGING_DATA kind of object     
    """
    axes    = [WIN]
    winaxes = [TIME, SAMPIX, Y, X]
    
    def getWindow(self, output, **kwargs):
        h = self.newHeader()
        h["FREQ"]   = self.getFrameRate()
        h[OUTPUT] = output
        return self._getClass_("windowdata")( self.getWinData(win=output, **kwargs), h)
    
    def getWinData(self, win, windark=None, **kwargs):
        """
        Return the data of a given window. 
        if windark number is specified the windark window is substracted         
        """
        win = self.winIndex(win)
        
        data = self._getData_(win, **kwargs)        
        # finaly remove a dark window if any 
        if windark:
            self.say("Substracting the dark window %s"%windark)
            data = data - self._getData_( windark,**kwargs )        
        return data
    
    def getOtherData(self, name, **kwargs):
        data = self._getData_(win, **kwargs)
        return self._return_array_data( self.data[name], kwargs)
    def getOpdData(self, **kwargs):
        return self.getOtherData("OPD", **kwargs)
    def getLocalOpdData(self, **kwargs):
        return self.getOtherData("LOCALOPD", **kwargs)
    def getSteppingPhaseData( self, **kwargs):
        return self.getOtherData("STEPPING_PHASE", **kwargs)
    def getFrameCountData( self, **kwargs):
        return self.getOtherData("FRAMECNT", **kwargs)
    def getTimeData( self, **kwargs):
        return self.getOtherData("TIME", **kwargs)
    getMjd = getTimeData
    
    
    def __getitem__(self, item):
        if isinstance(item, (int,long)):
            return self.getWindow(item)
        item = np.arange(self.getWins())[item]
        return self._getClass_("imagingdatalist")([self.getWindow(w) for w in item])
    
    def __len__(self):
        return self.getWins()
    
    def _return_array_data(self, data, kwargs):
        axes = self.winaxes if isinstance(data, np.ndarray) else []
        if axes:
            _updateKwargs(kwargs)
            if len(kwargs): self.say("reducing the data with %s"%kwargs)            
            data = DataX(data, axes).apply( **kwargs)
        elif len(kwargs):
            ###
            # Decide or not to put an error here 
            print "Warning: entity has no axes name, data is not a DataX. reduce and\
section kwargs will be ignored "        
        return data
    
    def _getData_(self, item, **kwargs):        
        return self._return_array_data_and_scale( self.data[item], kwargs)

    def _getAxis(self,axis=None):
        if axis is not None:
            return [axis]
        return self.axes
    
    def finalise(self, data, inarray=True, inaxis=None):
        if inarray is True:
            return self.inarray(data, axes=self._getAxis(inaxis))
        if isinstance(inarray, basestring):
            return self._getClass_(inarray)(data)    
        return data    
    @staticmethod
    def inarray(data, axes=None):        
        if axes and len(axes):
            return dataxconcat(data, axes[0])
        return np.asarray(data)

    getShape = list_decorator("getShape")
    def getDataShape(self):
        return tuple( [self.getWins()]+list(self[0].shape) )
        
    def getDataAxes(self):
        return self.axes+self.winaxes
    
    def winIndex(self, i):
        """
        tranform i (integer) to the data keyword correxponding to the wanted window
        0 -> DATA1
        1 -> DATA2 
        etc ...
        """
        if isinstance(i, str): return i
        return "DATA%d"%(i+1)
    
    def getWins(self):
        """
        Return the number of windows inside the table. 
        """
        return self.getKey("NREGION")
       
       
    def getFrameRate(self, **kwargs):
        if hasattr( self, "_framerate") and self._framerate is not None:
            return self._framerate
        N   = self.getKey("NAXIS2")
        mjd = self.data["TIME"][slice( 0, min(20,N))]        
        mjd *= (24*3600)
        
        self._framerate = 1./ ((mjd[1:None]-mjd[0:-1]).mean())
        return self._framerate
    
    getData = data_decorator("getData")



    

def restriction_decorator(getfunc, on=0, axes_name=EXPO):
    def tmp_restriction(self, **kwargs):
        gf  = getattr(self, getfunc)
        sec = getattr(self, axes_name+"_idx")
        data = gf(**kwargs)
        if not isinstance( data, np.ndarray) or sec is None: return data
        
        return data.section(sec, axes_name)


    
################################################################################
#
#  Carac a object older ofseveral group of files representing the detector
#
################################################################################

class _CaracCapabilities_(_PtcCapability_, _LinearityCapability_, baseplot._Carac_):
    all_ptc         = None
    all_linearity   = None
    
    longdark    = None
    _all_ptc_hash       = None
    _all_linearity_hash = None
    
    #def compute_all_ptc(self, signalrange=None, m2range=None, **kwargs):
    #    polars = self.getPolarSet()
    #    return self._getClass_("ptcimages")([
    #        self.withPolar(p).compute_ptc(signalrange=signalrange,
    #                                      m2range=m2range, **kwargs)
    #        for p in polars])
    
    compute_ptc       = polar_decorator("compute_ptc", inarray="ptcimages")
    compute_linearity = polar_decorator("compute_linearity", inarray="linearityimages")
    
    getNoiseMinData   = polar_decorator("getNoiseMinData", inaxis=POLAR)
    getNoiseMinHeader = header_decorator("getHeader")
    def getNoiseMin(self, **kwargs):
        return self._getClass_("noise")( self.getNoiseMinData(**kwargs), 
                                         self.getNoiseMinHeader())
    
    
    def getGainMData(self, polarzero=None, signalrange=None,
                     ditrange=None, **kwargs):
        
        lin  = self.getLinearity(ditrange=ditrange, signalrange=signalrange)
        polars = lin.getPolar()
        if len(polars)<1:
            raise NotEnoughData("Cannot conpute the multiplicative gain, number of polars should be >=2")
        if polarzero is None:
            polarzero = min(polars)
        try:
            izero = list(polars).index(polarzero)
        except ValueError:
            raise ValueError("cannot find polar %d in the list"%polarzero)
        flux0 = lin.idx(izero).getFluxData(**kwargs)
        flux  = lin.getFluxData(**kwargs)
        return flux/flux0
    getGainM       = getGainM
    getGainMHeader = header_decorator("getHeader")

    
    def getENoiseMinData(self,signalrange=None, m2range=None,**kwargs):
        noise  = self.getNoiseMinData(signalrange=signalrange, m2range=m2range, **kwargs)
        gain   = self.getGainData(signalrange=signalrange, m2range=m2range, **kwargs)
        return noise*gain
    getENoiseHeader = header_decorator("getHeader")
    getENoise = getENoise
    
    def getEFluxData(self,signalrange=None, ditrange=None, m2range=None, **kwargs):
        flux  = self.getFluxData(ditrange=ditrange, signalrange=signalrange, **kwargs)
        gain  = self.getGainData(signalrange=signalrange, m2range=m2range, **kwargs)
        return flux*gain
    getEFluxHeader = header_decorator("getHeader")
    getEFlux = getEFlux
    
    def withPolar( self, polar):
        ###
        # If polar is a scalar return a list of Images, not carac
        #
        if hasattr(polar, "__iter__"):
            polar = list(polar)
            return self.filter(lambda obj:obj.getPolar() in polar)
        return self._getClass_("images")(self).withPolar( polar)

        

class _Carac_(_CaracCapabilities_, _Images_):
    pass
        

class _AllCombined_(_Combined_, _CaracCapabilities_, _PtcCapability_, _LinearityCapability_, baseplot._Images_):
    _on_data_default  = 0
    _on_key_default = 5
    
    axes = [EXPO]
    kwargs = None
    HKEYS = 5
    HSIGNAL = 0
    HM2 = 1
    HM3 = 2
    HM4 = 3
    HN  = 4
    subclasses = {                   
        HSIGNAL:_SignalCube_,        
        HM2:_M2Cube_,
        HM3:_M3Cube_,
        HM4:_M4Cube_,
        HN:_NCube_,
        HKEYS:_KeysTable_
    }
    def idx(self, item):
        if isinstance( item , (int,long)):
           h0 = self[self.HSIGNAL].getHeader()
           self[self.HKEYS].fillHeader(item ,h0)
           
           return self._getClass_("combined")(                
                [ self._getClass_("signal",True)(self[self.HSIGNAL].getData()[item], h0),
                  self._getClass_("m2")( self[self.HM2].getData()[item], self[self.HM2].header),
                  self._getClass_("m3")( self[self.HM3].getData()[item], self[self.HM3].header),
                  self._getClass_("m4")( self[self.HM4].getData()[item], self[self.HM4].header),
                  self._getClass_("n") ( self[self.HN].getData()[item],  self[self.HN ].header)
              ])
       
        return self.__class__(
            [ self._getClass_("signalcube",True)( self[self.HSIGNAL].getData()[item], self[self.HSIGNAL].header),
              self._getClass_("m2cube")( self[self.HM2].getData()[item], self[self.HM2].header),
              self._getClass_("m3cube")( self[self.HM3].getData()[item], self[self.HM3].header),
              self._getClass_("m4cube")( self[self.HM4].getData()[item], self[self.HM4].header),
              self._getClass_("ncube") ( self[self.HN ].getData()[item], self[self.HN  ].header),
              self._getClass_("keystable")( self[self.HKEYS].getData(item), self[self.HKEYS].header)
          ]
        )    
    getRealKey = list_decorator("getKey", on=0)
    
    
    
    def getPolarSet(self):
        return list(set( self.getPolar()))
    def getDitSet(self):
        return list(set( self.getDit()))
    def getIlluminationSet(self):
        return list(set( self.getIllumination()))
    
################################################################################
#
#  Fit functions
#
################################################################################

def multiple_linregress(x, y):
    #x_mean = np.mean(x, axis=1, keepdims=True)
    s = x.shape
    x_mean = np.mean(x, axis=1).reshape((s[0],1))
    x_norm = x - x_mean
    #y_mean = np.mean(y, axis=1, keepdims=True)
    y_mean = np.mean(y, axis=1).reshape( (s[0],1))
    y_norm = y - y_mean
    
    if isinstance( x, np.ma.masked_array):
        Ns =  np.sum(~x.mask, axis=1)
    else:
        Ns = np.ndarray( (x.shape[0],), dtype=int )
        Ns[0:None] = x.shape[1]

    slope = (np.einsum('ij,ij->i', x_norm, y_norm) /
             np.einsum('ij,ij->i', x_norm, x_norm))
    intercept = y_mean[:, 0] - slope * x_mean[:, 0]
    return np.column_stack((slope, intercept, Ns))


def linregress(x,y, xrange=None, yrange=None):
    """
    linregress(x,y)
    perform a linear regression on vectors a and y 
    xrange and yrange define the range of fit for x and y values
    """
    if not len(x):
        raise Exception("All values are masked")
    x,y = clipfit(x,y, xrange, yrange)
    lx = len(x)
    if lx<2:
        return [-99,-99, 0]
    w = np.linalg.lstsq( np.vstack([ x, np.ones(x.shape)]).T, y)
    return w[0]+[lx]



def vectorialized_linearfit(x,y, xrange=None, yrange=None):
    x, y = cliparray(x, y, xrange=xrange, yrange=yrange)
    return multiple_linregress(x, y)

def clipfit(x, y, xrange=None, yrange=None):
    
    if xrange is None:
        xrange = (None, None)
    if yrange is None:
        yrange = (None, None) 
        
    xmin, xmax = xrange
    ymin, ymax = yrange
    if issubclass(type(x),np.ma.MaskedArray) or issubclass(type(y), np.ma.MaskedArray):
        mask =(x+y).mask
        x = x[~mask]
        y = y[~mask]
    if xmin is not None: 
        test = x>=xmin 
        x = x[test]
        y = y[test]
    if xmax is not None:
        test = x<=xmax        
        x = x[test]
        y = y[test]
    if ymin is not None: 
        test = y>=ymin 
        x = x[test]
        y = y[test]
    if ymax is not None:
        test = y<=ymax        
        x = x[test]
        y = y[test]
    return x,y
    

def linfitarray( x, y, xrange=None, yrange=None):
    """
    linfitarray( x, y)
    
    Perform a linear fit on the first dimension of x,y cubes
    KEYWORDS:
      xrange : 2 val tuple
      yrange : 2 val tuple     
    """
    if not x.shape==y.shape:
        raise Exception("x and y must have the same dimention")
    N = len(x)
    
    out = np.ndarray( (x.shape[0],3) )
    for i in np.arange(N):
        out[i] = linregress( x[i], y[i], xrange=xrange, yrange=yrange)    
    return out


class Areduce(object):
    """
    reducer = Areduce( f1, f2, f3, ...) 
    create a array dimention reducer
    e.g.:
     r = Areduce( np.mean, np.std )
    r( np.random.rand( 30,40) )
    """
    funcs = ()
    def __init__(self, *args):
        self.funcs = args
    def __call__( self, a, axeoffset=0):
        return areduce( self.funcs, a, axeoffset=axeoffset) 

def areduce(reducer, a, axeoffset=0):
    reducer = list(reducer)
    if not len(reducer):
        return a        
    func = reducer[0]
    
    reducer.pop(0)    
    if func is None:
        return areduce( reducer, a, axeoffset+1)    
    return areduce( reducer, func(a, axis=axeoffset), axeoffset)
    



def cliparray( x,y, xrange=None, yrange=None):
    """
    cliparray( x,y, xrange=, yrange=)
    
    return a masked array.
    
    clip the x and y vectors given a tuple of len 2  of range for x and y. 
    All the  x/y data outside x/yrange will be masked. 
    x/yrange can be : 
          None 
          (None, max_val)
          (min_val, None)
          (min_val, max_val)

    """
    test = None
    if xrange is not None:
        xrange = list(xrange)
        if xrange[0] is not None:
            if test is None:
                test = (x>=xrange[0])
            else: test *= (x>=xrange[0])
        if xrange[1] is not None:
            if test is None:
                test = (x<=xrange[1])
            else: test *= (x<=xrange[1])
    if yrange is not None:
        yrange = list(yrange)
        if yrange[0] is not None:
            if test is None:
                test = (y>=yrange[0])
            else: test *= (y>=yrange[0])
        if yrange[1] is not None:
            if test is None:
                test = (y<=yrange[1])
            else: test *= (y<=yrange[1])
    
    if test is not None:
        x = np.ma.array( data=x, mask=~test)
        y = np.ma.array( data=y, mask=~test)
    return x, y





#######################################################################
#
#  Usefull functions 
#
######################################################################


    
################################################################################
#
# Specific functions 
#
################################################################################

def computeMoment_datax(data, moment=2, meandata=None, axis=None):
    return computeMoment2( data, moment=2, meandata=meandata, axis=axis)

def computeMoment(data, moment=2, meandata=None, axis=None):
    return computeMoment2( data, moment=2, meandata=meandata, axis=axis)

def computeMoment_wloop(data, moment=2, meandata=None):
    """
    computeMoment(data, moment=2, meandata=None)
    return the moment of data on the axis 0 
    """
    ishape = data.shape
    reshaped = False
    if len(ishape)>3:
        reshaped = True
        data = data.reshape( tuple([-1,ishape[-2], ishape[-1]]))
        
    if meandata is None:
        meandata = data.mean(axis=0, dtype='f8')
        
    N = data.shape[0]
    total = np.zeros(meandata.shape, dtype='f8')
    for i in np.arange(N):
        total = total + (data[i]-meandata)**moment        
    return total/N


def computeMoment2(data, moment=2, meandata=None, axis=0):
    """
    computeMoment(data, moment=2, meandata=None)
    return the moment of data on the axis 0 
    """
    ishape = data.shape
    reshaped = False    
  
    if meandata is None:
        meandata = data.mean(axis=axis)    

    diff =  (data-meandata)
    if moment == 2:
        diffpower = diff*diff
    elif moment == 3:
        diffpower = diff*diff*diff
    elif moment == 4:
        diffpower = diff*diff*diff*diff
    else:    
        diffpower = (data-meandata)**moment    
    return diffpower.mean(axis=axis)


def meanError(sigma2, axis=None):
    N = size( sigma2, axis=None)
    return np.sqrt( sigma2.mean( axis=axis ) / float(N) )
        
def computeVarianceErrorbar(m2,m4,N):
    """
    computeVarianceErrorbar(m2,m4,N)
    return the variance error bar from the 2nd order moment, the 4th and number of points N
    """
    return np.sqrt( (m4 - m2**2*(N-3.)/(N-1.))/N)
    
    #return np.sqrt(m4/N - m2**2*(N-3.)/(N*(N-1.)))

def computeSkweness(m2,m3):
    """
    computeSkweness(m2,m3)
    Compute the Skewness from 2nd order moment and 3nd order
    """
    return m3/m2**1.5




def getJitters(cube, clipmin=None, clipmax=None):    
    N = len(cube)
    out = np.ndarray( (cube.shape[0],2), dtype=np.float64)
    i=0
    for img in cube:
       out[i] = getJitter(img, clipmin=clipmin, clipmax=clipmax)
       i += 1
    return out
    
def getJitter( img, clipmin=None, clipmax=None):
    img = img.copy()
    if clipmin is not None:
        img[img<clipmin] = clipmin
    if clipmax is not None:
        img[img>clipmax] = clipmax
        
    return ndim.center_of_mass(img)

def getJitterFFT( jitter, freq=None, cut=False, fmin=None, fmax=None):
     fft =  np.fft.fft( jitter)
     if cut:
          fft = fft[0:fft.shape[0]/2]
     if fmax is not None:          
          fft  =  fft[ freq<=fmax ]
          freq = freq[ freq<=fmax ]
     if fmin is not None:
          fft = fft[ freq>=fmin ]
          
     return fft
     
def getJitterPSD(jitter, dt, freq= None, cut=False, fmin=None, fmax=None):
     # should be (pix/sqrt(Hz))
     tf = getJitterFFT(jitter, cut=cut, freq=freq , fmin=fmin, fmax=fmax)
     return abs(tf*tf.conjugate())*dt



def getJitterFFTs( jitters, freq=2000):
    N = jitters.shape[0]
    out = []
    f = (np.arange(N) / float(N))*freq 
    for dim in range( jitters.shape[1]): 
        out.append( [f,getJitter(jitters[:,dim])])
    return out


###############################################################################
#
#  Class manipulation function 
#
#################################################################################
def isEntity(obj):
    return hasattr(obj,"isEntity") and obj.isEntity()

def add_scalar_capability_on(cls_list, name, func, on=False, inarray=True, inaxis=True):
    for c in cls_list:
        if isEntity(c):
            c._add_key_capability(name, func)
        else:
            c._add_key_capability(name, on=on, inarray=inarray, inaxis=inaxis)

def add_data_capability_on(cls_list, name, datafunc, headerfunc=newHeader, on=False,
                           inaxis=None, clsname=None):
    for c in cls_list:
        if isEntity(c):
            c._add_data_capability(name, datafunc, headerfunc=headerfunc, clsname=clsname)
        else:
            c._add_data_capability(name, on=on, clsname=clsname)
            
def add_scalar_capability(name, func, on=False, inarray=True, inaxis=True):
    return add_scalar_capability_on( [DataEntity,_ImageCapabilities_, _ImagesCapabilities_],
                                     name, func, 
                                     on=on, inarray=inarray, inaxis=inaxis
    )    
    
def add_data_capability(name, datafunc, headerfunc=newHeader, on=False, inaxis=None,
                        clsname=None):
    return add_data_capability_on( [DataEntity,_ImageCapabilities_, _ImagesCapabilities_],
                                   name, 
                                   datafunc, headerfunc=headerfunc, clsname=clsname)
    
class Test(DataEntity):
    def getTest(self, **kwargs): 
        return True

#add_scalar_capability( "toto", lambda obj,**kwarg: obj.getKey("MJD-OBS"))
#add_data_capability( "titi"  , lambda obj,*args,**kwarg: obj.getSignalData(*args,**kwarg)/1000,
#                     on=0, clsname="signal")
