import numpy as np
from detector import engine, bases, fits, config



class KeyWindowCapabilities(bases._KeyCubeCapabilities_):
    pass
class DataWindowCapabilities(bases._DataCubeTimeCapabilities_):
    pass



class KeyWindowListCapabilities(bases._KeyCubeListCapabilities_):
    pass
class DataWindowListCapabilities(bases._DataCubeListCapabilities_):
    pass

    
class KeyImagingDataCapabilities(engine._KeyTableDataCapabilities_):
    def getNumberofwindows(self):
        return self.getKey("NREGION")

class DataImagindDataCapabilities(engine._DataTableDataEntityCapabilities_):
    inarray = staticmethod(engine.inarray)
    isarray = staticmethod(engine.isarray)
    finalise = engine.finalise
    _getAxis = engine.getAxis
    
    def isOffsetable(self, name):
        return name[0:4] == "DATA"
    def getDataAxes(self,name):
        if name[0:4] == "DATA":
            return list(Window.axes)
        if name in ["LOCALOPD", "OPD"]:
            return [config.TIME, config.TELESCOPE]
        else: return [config.TIME]
        
    def Windows(self, window=None, **kwargs):
        if window is None: window = range( self.getNumberofwindows() )
        else: window = np.s_[window]
        winclass = self._getClass_("window")

        if not hasattr(window, "__iter__"):
            return winclass( self.getData( self.winIndex(window), **kwargs) )
        
        return self._getClass_("windowlist")(
            [
               winclass( self.getData( self.winIndex(i), **kwargs) ) for i in window
            ]            
        )    
    def getWindows(self, window=None, **kwargs):
        if window is None: window = range( self.getNumberofwindows() )
        else: window = np.s_[window]
        if not hasattr(window, "__iter__"):
            return self.getData( self.winIndex(window), **kwargs) 
        return self.finalise(  [self.getData( self.winIndex(i), **kwargs) for i in window ], inaxis="window")
    
    def winIndex(self, i):
        """
        tranform i (integer) to the data keyword correxponding to the wanted window
        0 -> DATA1
        1 -> DATA2 
        etc ...
        """
        if isinstance(i, str): return i
        return "DATA%d"%(i+1)
    def getOpd(self, **kwargs):
        return self.getData("OPD", **kwargs)
    def getLocalOpd(self, **kwargs):
        return self.getData("LOCALOPD", **kwargs)
    def getSteppingPhase( self, **kwargs):
        return self.getData("STEPPING_PHASE", **kwargs)
    def getFrameCount( self, **kwargs):
        return self.getData("FRAMECNT", **kwargs)
    def getExptime( self, **kwargs):
        return self.getData("EXPTIME", **kwargs)
    def getTime( self, **kwargs):
        return self.getData("TIME", **kwargs)
    getMjd = getTime
        
    
class ImagingData(engine.TableDataEntity, DataImagindDataCapabilities, KeyImagingDataCapabilities):
    pass
class ImagingDataHDU(fits.BinTableHDU, DataImagindDataCapabilities, KeyImagingDataCapabilities):
    pass

class Window(DataWindowCapabilities, KeyWindowCapabilities, engine.DataEntity):
    axes = [config.TIME, config.SAMPIX, config.Y, config.X]

class WindowList(DataWindowListCapabilities,KeyWindowListCapabilities, bases.CubeListDataList):
    axes = [config.WINDOW]
    
engine._update_class_dictionary_(__name__)
