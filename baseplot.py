import plots
import numpy as np


class DetPlot(plots.XYPlot):
    dataKeys = {"x":(None,None),"y":(None,None),"xerr":(None,None),"yerr":(None,None)}
    ckw_errorbar = dict(fmt=".")
    ckw_plot     = dict(fmt=".")
    kw_detector = dict(section=slice(0,None), freduce=np.mean, fcall=None)
    detector    = None
    
    def setData(self, detector=None, x=None, y=None, xerr=None, yerr=None, **kwargs):
        #kwargs= dict(freduce=freduce,section=section,fcall=fcall)
        # leave the detector unchange if this one wasn't set                
        if detector:
            self.detector = detector
        # update the kw_detector for a future call
        self.kw_detector = {k:kwargs.get(k,self.kw_detector.get(k,None)) for k in kwargs} 
        
        self.dataKeys = self.dataKeys.copy()
        if x is not None: self.dataKeys['x'] = x
        if y is not None: self.dataKeys['y'] = y
        if xerr is not None: self.dataKeys['xerr'] = xerr
        if yerr is not None: self.dataKeys['yerr'] = yerr
        
        self.setX(**kwargs)
        self.setY(**kwargs)
        self.setXerr(**kwargs)
        self.setYerr(**kwargs)
    
    def setFromAttr(self, on, attr, **kwargs):
        setattr( self, on,  getattr(self.detector, attr)(**kwargs) )
    def setFromKey( self, on , key, **kwargs):
        setattr( self, on, self.detector.getKey(key, **kwargs))
    def setFromDataKey(self, on, **kwargs):
        if not on in self.dataKeys: return 
        dk = self.dataKeys[on]
        if dk[0] is None: return
        if len(dk)<2 or dk[1] is None:
            return self.setFromAttr( on, dk[0], **kwargs)
        if dk[1] == "key":
            if "polar" in kwargs:
                kw = {"polar":kwargs["polar"]}
            else:
                kw = {}
            return self.setFromKey( on, dk[0], hdu=kwargs.get("hdu", 0), **kw)
        if dk[1] == "nokw":
            if "polar" in kwargs:
                kw = {"polar":kwargs["polar"]}
            else:
                kw = {}

            return self.setFromAttr( on, dk[0], **kw)
        raise ValueError("instruction '%s' not understood"%dk[1])
    
    def setX(self, **kwargs):
        self.setFromDataKey("x", **kwargs)
    def setY(self, **kwargs):
        self.setFromDataKey( "y", **kwargs)
    def setXerr(self, **kwargs):
        self.setFromDataKey("xerr", **kwargs)
    def setYerr(self, **kwargs):
        self.setFromDataKey( "yerr", **kwargs)

class DetImgPlot(plots.ImgPlot):
    ckw_imshow = {"origin":"lower","interpolation":"nearest"}
    ckw_sectionplot = {}
    
    def setData(self, detector=None, **kwargs):
        if detector: self.detector = detector
        self.setImg(**kwargs)
        self.setImgerr(**kwargs)
        self.setSectionplot(**kwargs)
        
    def setImg(self, **kwargs):
        self.img = self.detector.getData(**kwargs)
    def setImgerr(self, **kwargs):
        self.imgerr = None
    def setSectionplot(self, **kwargs):
        self.sectionplot = kwargs.pop("sectionplot",None)
        
    def imshow(self, **kwargs):
        self._parsekwargs_(kwargs, "imshow")
        
        axes = self.getKwAxes(kwargs)
        img = self.substituteImg(kwargs)
        masknan = kwargs.pop("masknan", False)
        if not isinstance( img, plots.np.ma.MaskedArray):
            img = plots.np.asarray(img)
        if masknan:
            img = plots._masknan(img)            
        self.lastimgplot = axes.imshow(img , **kwargs)

        
        if self.sectionplot and hasattr(self.sectionplot, "plot"):            
            self.sectionplot.plot( axes=axes, offset=(-0.5,-0.5))    
        return self.lastimgplot
    
class PlotCollection(plots.PlotClassDict):
    pass

class _Plots_(object):
    _plots  = PlotCollection() #this contain the class plots 
    _oplots = None #this contain the object plot
    ##
    # Functions to handle plots 
    ##
    
    def addPlot(self, name, plot):
        if self._oplots is None:
            self._oplots = PlotCollection()
        self._oplots[name] = plot
    @classmethod
    def _addPlot(cls,name, plot):
        if cls._plots is None:
            cls._plots = PlotCollection()
        cls._plots[name] = plot
        
    @classmethod
    def _getPlotCollection(cls):
        pc = None
        for base in cls.__bases__:
            if hasattr(base, "_getPlotCollection"):
                if not pc:
                    pc = base._getPlotCollection()
                else:
                    pc.update( base._getPlotCollection() )
        if not pc:
            if cls._plots:
                return cls._plots.copy()
            else:
                return PlotCollection()
        if cls._plots:
            pc.update(cls._plots)
        return pc
    
    def getPlotCollection(self, **kwargs):
        pc = self._getPlotCollection()
        if self._oplots:
            pc.update(self._oplots)
        pc.setData( detector=self, **kwargs ) 
        return pc
    
    @classmethod
    def _getPlot(cls, name):
        if cls._plots and name in cls._plots:
            return cls._plots[name]
        for base in cls.__bases__:            
            if hasattr( base, "_getPlot" ):
                return base._getPlot(name)
        raise NameError( "Cannot find plot named '%s' "%name )
    def getPlot(self, name):
        if self._oplots and name in self._oplots:
            return self._oplots[name]
        if self._plots and name in self._plots:
            return self._plots[name]
        return self._getPlot(name)
        raise NameError( "Cannot find plot named '%s' "%name )
    
    @property
    def plots(self):
        return self.getPlotCollection()
    
    def plot(self, plotname, *args, **kwargs):
        plot = self.getPlot(plotname)
        return plot.newWithData(self, *args, **kwargs)

class Image(_Plots_):
    _plots = PlotCollection()
Image._addPlot( "img", DetImgPlot)


class Combined(_Plots_):
    _plots = PlotCollection()

class CubeImage(Combined):
    _plots = PlotCollection()
        
class Ptc(_Plots_):
    _plots = PlotCollection()
class PtcList(_Plots_):
    _plots = PlotCollection()    
class Linearity(_Plots_):
    _plots = PlotCollection()
class LinearityList(_Plots_):
    _plots = PlotCollection()
    
class PtcLinearity(Ptc,Linearity):
    _plots = PlotCollection()

class PtcLinearityList(PtcList,LinearityList):
    _plots = PlotCollection()
    
class _Jitter_(_Plots_):
    _plots = PlotCollection()

class Images(_Plots_):
    _plots = PlotCollection()

class Carac(Images):
    _plots = PlotCollection()

