import tkplots
try:
    import Tkinter as tk
except:
    import tkinter as tk
import ttk

from tkplots import * 
import numpy as np
import rapid

from detector import detplots as plots


class Ptc(plots.Ptc):
    kw_errorbar = {"axes":"ptc"}
    kw_plotfit  = {"axes":"ptc"}
    kw_plot     = {"axes":"ptc"}
    kw_allaxes  = {"ptc":{"figure":"ptc"}}
    
class DetImgPlot(plots.DetImgPlot):
    kw_imshow = {"axes":"img", "origin":"lower" }
    kw_hist   = {"axes":"hist", "color":"black", "normed":True}
    ckw_histsection = {"axes":"hist","color":"red", "alpha":0.8, "normed":True}
    def hist(self, **kwargs):
        self._parsekwargs_(kwargs, "hist")
        
        vmin, vmax, nbins = kwargs.pop("vmin",None), kwargs.pop("vmax",None), kwargs.pop("nbins",100)
        bins = kwargs.pop("bins",None)
        if (bins is None) and (vmin is not None) and (vmax is not None):
            bins = np.linspace(vmin, vmax, nbins)
        axes = self.getKwAxes(kwargs)
        x    = self.substituteX(kwargs)
        masknan = kwargs.pop("masknan", False)
        if masknan and isinstance(x, np.ndarray):
            x= _masknan(x)            
        h = axes.hist(x, bins=bins,**kwargs)
        self.histsection(bins=bins)
        return h
    def histsection(self, **kwargs):
        if not hasattr(self, "sectionplot"):
            return None
        self._parsekwargs_(kwargs, "histsection")
        img = self.substituteImg(kwargs)[self.sectionplot]
        x = img.flatten()
        axes = self.getKwAxes(kwargs)
        return axes.hist(x, **kwargs)
        
class FlatPlot(DetImgPlot):
    kw_allaxes = { "hist":{"figure":"flat_hist"},
                    "img":{"figure":"flat_imshow"}
                }
class GainPlot(DetImgPlot):
    kw_allaxes = { "hist":{"figure":"gain_hist", "vmin":0, "vmax":5},
                    "img":{"figure":"gain_imshow"}
    }

def _masknan(x):
    return np.ma.masked_array( x, np.isinf(x)+np.isnan(x))


class QuitButton(tk.Button):
    def __init__(self, parent, **kwargs):
        kwargs.setdefault("text","QUIT")
        kwargs.setdefault("command",self._quit)
        tk.Button.__init__(self, parent, **kwargs)
        self.parent = parent 
    def _quit(self):
        self.parent.quit()
        self.parent.destroy()

class Panel(tk.Tk):
    def __init__(self,  carac , *args, **kwargs):
        
        tk.Tk.__init__(self, *args, **kwargs)
        self.carac = carac
        
        self.mainFrame  =  MainFrame(self, carac)
        self.quitButton =  QuitButton(self)
        self.mainFrame.pack(side=tk.TOP, padx=8)
        self.quitButton.pack(side=tk.TOP, padx=8)

class Command(list):
    def __call__(self):
        return [func() for func in self]

def fullframe():
    #return r.rapid(None,None)
    return (slice(0,None), slice(0,None))

def output1(yx):
    return rapid.rapid(1, 20, yx)
    
class Data(object):
    on_image_change = Command()
    on_carac_polar_change = Command()
    on_carac_polar_illumination_change = Command()
    on_carac_polar_dit_change = Command()
    on_polar_change = Command()
    on_section_change = Command()
    on_freduce_change = Command()
    on_pixel_change = Command()

    on_refresh_ptcPlot = Command()
    on_refresh_flatPlot = Command()
    on_refresh_gainPlot = Command()
    
    dit   = None
    polar = None
    illumination = None
    flatPlot = None
    gainPlot = None
    ptcPlot  = None 
    config = {
        "ptc_on":"illumination", 
        "freduce":"mean",
        "pixel":0, 
        "sectionfunc":"fullframe",
        "output":0,
        "size":20,
        "yx":(0,0),
        "yx_io":rapid.grism.pos
    }
    choices = {
        "ptc_on":["illumination","dit","both"],
        "freduce":["mean", "median", "pixel"],
        #"sectionfunc":[fullframe, rapid.rapid, rapid.grism],
        #"sectionfuncargs":{ rapid.rapid:["output","size", "yx"] },
        "sectionfunc":["fullframe", "box", "grism"],        
        "output":[None]+list(range(8))
    }
    def __init__(self, carac, config=None):
        self.carac = carac
        self.selectPolar(np.max(carac.getPolar()))
        config = config or {}
        self.config = dict(self.config)
        self.config.update(config)
        if not "ptc_on" in config:
            # guess the default 
            if len( self.carac.getIlluminationSet() )<3:
                self.config["ptc_on"] = "dit"
            if len( self.carac.getDitSet() )<3:
                self.config["ptc_on"] = "illumination"
        self.setFreduce( self.config["freduce"])
        self.setSection()
        self.initFlatPlot()
        self.initGainPlot()
        self.initPtcPlot()
        
    def selectPolar(self, polar):
        self.carac_polar = self.carac.withPolar(polar)
        self.polar = polar
        
        
        if self.dit is None:
            self.dit = np.min(self.carac_polar.getDit())
        else:
            dits = self.carac_polar.getDit()
            self.dit = dits[np.argsort( np.abs(dits-self.dit) )[0]]        
        self.selectDit(self.dit)
        
        if self.illumination is None:
            self.illumination = np.min(self.carac_polar.getIllumination())
        else:
            illums = self.carac_polar.getIllumination()
            self.illumination = illums[np.argsort(np.abs(illums-self.illumination))[0]]
        
        self.selectIllumination( self.illumination )
        
        self.on_carac_polar_change()
        self.on_polar_change()                    

    def selectDit(self, dit):
        self.carac_polar_dit = self.carac_polar.withDit(dit)
        if self.illumination is not None:
            # reset polar to the closest one
            illums = self.carac_polar_dit.getIllumination()
            self.illumination = illums[np.argsort(np.abs(illums-self.illumination))[0]]
        
        
        if hasattr(self, "carac_polar_illumination"):            
            self.image = self.carac_polar_illumination.withDit(dit)[0]
        else:            
            self.image = self.carac_polar_dit[0]
        
        self.dit = dit
        
        self.on_carac_polar_dit_change()
        self.on_image_change()
        
        
    def selectIllumination(self, illumination):
        self.carac_polar_illumination = self.carac_polar.withIllumination(illumination)
        if self.dit is not None:
             # reset polar to the closest one
            dits = self.carac_polar_illumination.getDit()
            self.dit = dits[np.argsort( np.abs(dits-self.dit) )[0]]     
                
        self.image = self.carac_polar_dit.withIllumination(illumination)[0]
        self.illumination = illumination
        
        self.on_carac_polar_illumination_change()
        self.on_image_change()
    def getDetCarac(self):
        if self.config["ptc_on"] == "dit":
            return self.carac_polar_illumination.getDetCarac()
        if self.config["ptc_on"] == "illumination":
            return self.carac_polar_dit.getDetCarac()
        if self.config["ptc_on"] == "both":
            return self.carac_polar.getDetCarac()
        raise KeyError("ptc_on should be one of 'dit','illumination' or 'both' got %s"%self.config["ptc_on"] )
    def set_ptc_on(self,val):
        if not val in self.choices["ptc_on"]:
            raise KeyError("ptc_on should be one of %s got %s"%(self.choices["ptc_on"], val))
        self.config["ptc_on"] = val
    def get_ptc_on(self):
        return self.config["ptc_on"]


    def initFlatPlot(self):
        plot = FlatPlot()                
        #plot.new_axes( "img", figure="flat_img")
        #plot.new_axes( "hist", figure="flat_hist")
        self.flatPlot = plot
        self.on_image_change.append(self.refreshFlatPlot)
        
    def refreshFlatPlot(self):
        if not self.flatPlot: return 
        self.flatPlot.setData(self.image.Signal(), sectionplot=self.section)
        self.refreshImgPlotDefault(self.flatPlot)
        
        self.flatPlot.kw["allaxes.img"]["title"] = "p=%d, DIT=%0.5f, I=%f"%(self.polar, self.dit, self.illumination)
        self.on_refresh_flatPlot()
        

    def initGainPlot(self):
        plot = GainPlot()
        #plot.new_axes( "img", figure="gain_img")
        #plot.new_axes( "hist", figure="gain_hist")
        
        self.gainPlot = plot
        self.on_polar_change.append( self.refreshGainPlot )
        
    def refreshImgPlotDefault(self,plot):
        plot.img = _masknan(plot.img)
        plot.vmin = np.min(plot.img)
        plot.vmax = np.max(plot.img)
        
        kw =  plot.kw["hist"]        
        kw.setdefault("vmin" , plot.vmin)
        kw.setdefault("vmax" , plot.vmax)
        
        
    def refreshGainPlot(self):
        ptc = self.getDetCarac()
        self.gainPlot.setData( ptc.Gain(), sectionplot=self.section)
        self.refreshImgPlotDefault(self.gainPlot)
        
        
        
        
        dits   = list(set(ptc[-1].getData("DET DIT")))
        illums = list(set(ptc[-1].getData("ILLUMINATION")))
        if len(dits)>1:
            ditsleg = "%.4f->%.4f"%(min(dits), max(dits))
        else:
            ditsleg = "%.4f"%dits[0]
        if len(illums)>1:
            illumsleg = "%.4f->%.4f"%(min(illums), max(illums))
        else:
            illumsleg = "%s"%illums[0]
        
        self.gainPlot.kw["allaxes.img"]["title"] = "p=%d, DIT=%s, I=%s"%(self.polar, ditsleg, illumsleg)
        self.on_refresh_gainPlot()

    def initPtcPlot(self):
        self.ptcPlot = Ptc()
        #self.ptcPlot.new_axes("ptc", figure="ptc")
        self.on_polar_change.append(self.refreshPtcPlot)
        self.on_section_change.append(self.refreshPtcPlot)
        self.on_freduce_change.append(self.refreshPtcPlot)
        self.on_pixel_change.append(self.refreshPtcPlot)
        
    def refreshPtcPlot(self):
        kw = dict(            
            section = self.section
        )
        if self.freduce is None:
            kw["pixel"] = self.config["pixel"]
        else:
            kw["freduce"] = self.freduce
        
        if self.config["ptc_on"] == "dit":
            data = self.carac_polar_illumination
        elif self.config["ptc_on"] == "illumination":
            data = self.carac_polar_dit
        elif self.config["ptc_on"] == "both":
            data = self.carac_polar        
        self.ptcPlot.setData(data, **kw)
        self.on_refresh_ptcPlot()
        
        
    def refreshSectionPlot(self):
        if self.flatPlot:
            self.flatPlot.sectionplot = self.section
            self.on_refresh_flatPlot()
        if self.gainPlot:
            self.gainPlot.sectionplot = self.section
            self.on_refresh_gainPlot()
        
    def getFlatPlot(self):
        return self.flatPlot
        plot = self.image.Signal().plots.img(sectionplot=self.section)
        
        plot.kw["axes"]["title"] = "p=%d, DIT=%0.5f, I=%f"%(self.polar, self.dit, self.illumination)
        
        return plot
    def getGainPlot(self):
        return self.gainPlot    
    def getPtcPlot(self):
        return self.ptcPlot
    
    def getSectionPlot(self):
        if hasattr(self.section, "plot"):
            return self.section.plot
        return None
    def setSection(self):
        self.section= self.getSection( self.config["sectionfunc"])
        if self.getPixel()>=self.getSectionSize():
            self.setPixel(0)
        self.refreshSectionPlot()
        
        self.on_section_change()
        
    def setSectionFunc(self, sectionfunc):
        self.config["sectionfunc"] = sectionfunc
        self.setSection()

    def getSectionSize(self):
        if hasattr(self.section, "size"):
            return self.section.size
        # return the full frame size
        return self.image.getDataSize()
        return reduce( lambda x,y: x*y, self.image.getDataShape())
        
    def setFreduce(self,freduce):
        fred = freduce.lower()
        
        if fred == "mean":
            self.freduce = np.mean

        elif fred == "median":
            self.freduce = np.median

        elif fred == "pixel":
            self.freduce = None
        else:
            raise ValueError("freduce should be one of %s"%self.choices["freduce"])
        self.config["freduce"] = freduce
        
        self.on_freduce_change()
    def setPixel(self, pixel):
        self.config["pixel"] = max( min(pixel, self.getSectionSize()-1) , 0)        
        self.on_pixel_change()
        
    def getPixel(self):
        return self.config["pixel"]
        
    def getSection(self, sectionfunc):
        sf = sectionfunc.lower()
        if sf == "box":
            return rapid.rapid( self.config["output"], self.config["size"], self.config["yx"])                                                                                        
        if sf == "grism":
            return rapid.grism( self.config["yx_io"] )
        if sf == "free":
            return rapid.free( self.config["yx_io"] )
        
        return fullframe()
        
        
    def setSectionOutput(self, output):
        self.config["output"] = output
        self.setSection()
    def setSectionIOPos( self, yx):
        y,x = yx
        self.config["yx_io"]     = int(y),int(x)
        self.setSection()
    def setSectionBox(self, size, yx):
        y,x = yx
        self.config["yx"]     = int(y),int(x)
        self.config["size"]   = int(size)        
        self.setSection()
                    
        
        
class MainFrame(tk.Frame):
    def __init__(self, parent, carac, **kwargs):
        self.data = Data(carac)
        
        tk.Frame.__init__(self, parent, **kwargs)
        self.selecFrame  = SelecFrame(self, self.data)
        self.sectionFrame= SectionFrame(self, self.data)
        
        #self.chooseImageFrame = ChooseImageFrame(self)
        self.mainTabs = MainTabs(self, self.data)
        
        #################################################
        #self.chooseImageFrame.pack(side=tk.TOP, fill=tk.BOTH)
        self.selecFrame.pack(side=tk.TOP)
        self.sectionFrame.pack(side=tk.TOP)
        self.mainTabs.pack(side=tk.TOP)
        
        ##########################
        # refresh all plots
        self.data.refreshFlatPlot()
        self.data.refreshGainPlot()
        self.data.refreshPtcPlot()
        
        
class BaseFrame(tk.Frame):
    pass


class SelecDetParams(BaseFrame):
    pass

class DitMenu(BaseMenu):
    def __init__(self, parent, data, selected=0, command=None, **kwargs):
        self.data = data
        values = list(data.carac_polar_illumination.getDitSet())
        
        BaseMenu.__init__(self,parent, values,
                          selected=self.getSelected(data.dit,values, selected),
                          command=command or self.on_change, **kwargs)
    
    def on_change(self,dit_str):
        dit = float( dit_str )
        self.data.selectDit(dit)
    
    def refresh(self):
        return self._refresh( list(self.data.carac_polar_illumination.getDitSet()), self.data.dit) 
    
class IlluminationMenu(BaseMenu):
    def __init__(self, parent, data, selected=0, command=None, **kwargs):
        self.data = data
        values = list(data.carac_polar_dit.getIlluminationSet())        
        BaseMenu.__init__(self,parent, values,
                          selected=self.getSelected(data.illumination,values, selected),
                          command=command or self.on_change, **kwargs)
    def on_change(self,illumination_str):
        illumination = float( illumination_str )
        self.data.selectIllumination(illumination)
    
    def refresh(self):
        # Reset var and delete all old options
        return self._refresh( list(self.data.carac_polar_dit.getIlluminationSet()), self.data.illumination)
    
class PolarMenu(BaseMenu):
    def __init__(self, parent, data, selected=0, command=None, **kwargs):
        self.data = data
        values = list(data.carac.getPolarSet())
        
        BaseMenu.__init__(self,parent, data.carac.getPolarSet(),
                          selected=self.getSelected(data.polar, values, selected),
                          command=command or self.on_change, **kwargs)
    def on_change(self,polar_str):
        polar = float( polar_str )
        self.data.selectPolar(polar)
        
    def refresh(self):
        # Reset var and delete all old options
        return self._refresh(list(self.data.carac.getPolarSet()), self.data.polar)
        
        
class SelecFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):
        BaseFrame.__init__(self, parent, **kwargs)
        self.data    = data

        self.polarLabel = BaseLabel(self, text="Polar: ")
        self.polarMenu = PolarMenu( self, data)
        
        self.ditLabel = BaseLabel(self, text="DIT: ")
        self.ditMenu = DitMenu( self, data)
        
        self.illuminationLabel = BaseLabel(self, text="Illum: ")
        self.illuminationMenu = IlluminationMenu( self, data)
        

        self.polarLabel.pack(side=tk.LEFT)
        self.polarMenu.pack(side = tk.LEFT)
        
        self.ditLabel.pack(side=tk.LEFT)
        self.ditMenu.pack(side = tk.LEFT)
        self.illuminationLabel.pack(side=tk.LEFT)
        self.illuminationMenu.pack(side = tk.LEFT)
        
        self.data.on_carac_polar_illumination_change.append( self.ditMenu.refresh)
        self.data.on_carac_polar_dit_change.append(self.illuminationMenu.refresh)
        self.data.on_carac_polar_change.extend([self.ditMenu.refresh,self.illuminationMenu.refresh])
        
    def on_dit_change(self,dit):        
        self.main.flatFrame.set_image( self.main.carac.withDit(dit)[0] )
        



CASE_IOFRAME = 2
CASE_OUTPUT  = 1
CASE_OTHER   = 0 
class SectionFuncMenu(BaseMenu):
    def __init__(self, parent, data,command = None,  **kwargs):
        self.data = data
        values = data.choices["sectionfunc"]
        self.var = tk.StringVar()
        self.var.set( data.config["sectionfunc"] )
        
        BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs) 
    def on_change(self,sectionfunc):
        self.data.setSectionFunc(sectionfunc)

    def getCase(self):
        f = self.var.get().lower()

        
        if f=="box":
            return CASE_OUTPUT
        if f=="grism" or f=="free":    
            return CASE_IOFRAME
        
        return CASE_OTHER
    
        
class OutputMenu(BaseMenu):
    def __init__( self, parent, data, command=None, **kwargs):
        self.data = data
        values = data.choices["output"]
        self.var = tk.Variable()
        self.var.set( data.config["output"] )
        BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs)
        
    def on_change(self, output):
        self.data.setSectionOutput(output)

class FreduceMenu(BaseMenu):
    def __init__( self, parent, data, command=None, **kwargs):
        self.data = data
        values = data.choices["freduce"]
        self.var = tk.StringVar()
        self.var.set( data.config["freduce"] )
        BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs)
        
    def on_change(self, freduce):
        self.data.setFreduce(freduce)


class PixelFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):
        BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        self.freducePixel = TypeEntry(self, dtype=int, width=4, value=data.getPixel(),
                                      on_return=self.set_pixel
                                  )
        self.prevButton = BaseButton(self, text="<", command=self.prev_pixel)
        self.nextButton = BaseButton(self, text=">", command=self.next_pixel)
        self.freducePixel.pack(side=tk.LEFT)
        self.prevButton.pack(side=tk.LEFT)
        self.nextButton.pack(side=tk.LEFT)
                
        self.state_button( self.data.getPixel(),  0, self.data.getSectionSize()-1)
        self.data.on_pixel_change.append(self.refresh_pixel_val)
        
    def refresh_pixel_val(self):
        pixel = self.data.getPixel()
        self.freducePixel.setVal(pixel)
        self.state_button(pixel, 0, self.data.getSectionSize()-1)
    
    def set_pixel(self,event):
        self.data.setPixel(self.freducePixel.getVal())
            
    def state_button(self,pixel, mn, mx):
        self.state_prev_button(pixel, mn)
        self.state_next_button(pixel, mx)
    def state_prev_button(self, pixel, mn):
        if pixel<(mn+1):
            self.prevButton.config(state='disable')
        else:
            self.prevButton.config(state='normal')
    def state_next_button(self, pixel, mx):
        if pixel>(mx-1):
            self.nextButton.config(state='disable')
        else:
            self.nextButton.config(state='normal')
        
    def prev_pixel(self):
        self.data.setPixel( self.data.getPixel()-1)
    
    def next_pixel(self):
        self.data.setPixel( self.data.getPixel()+1)
        
class FreduceFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):
        BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        self.freduceLabel = BaseLabel(self, text="Freduce")
        self.freduceMenu  = FreduceMenu(self, data, command=self.on_freduce_change)
        self.freducePixel = PixelFrame(self, data)
        
        self.freduceMenu.pack(side=tk.LEFT)
        self.swap()
    def on_freduce_change(self, freduce):
        self.data.setFreduce(freduce)
        self.swap()
    
    def swap(self):
        freduce = self.data.config["freduce"].lower()

        if freduce=="pixel":
            self.freducePixel.pack(side=tk.LEFT)
        else:            
            self.freducePixel.pack_forget()
            

        
class SectionIOFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):            
        BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        y,x = self.data.config["yx_io"]
        self.x = LabelEntry( self, BaseLabel, {"text":"X"},
                             TypeEntry, {"dtype":int, "value":x,  "width":4,
                                         "on_return":self.on_change} )
        self.y = LabelEntry( self, BaseLabel, {"text":"Y"},
                             TypeEntry, {"dtype":int, "value":y, "width":4,
                                         "on_return":self.on_change} )
        
        self.x.pack( side=tk.LEFT)
        self.y.pack( side=tk.LEFT)
    def on_change(self,event):
        self.data.setSectionIOPos((self.y.getVal(), self.x.getVal()))
        
class SectionBoxFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):        
        BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        y,x = self.data.config["yx"]
        
        self.x = LabelEntry( self, BaseLabel, {"text":"X"},
                             TypeEntry, {"dtype":int, "value":x, "width":4,
                                         "on_return":self.on_change} )
        self.y = LabelEntry( self, BaseLabel, {"text":"Y"},
                             TypeEntry, {"dtype":int, "value":y, "width":4,
                                         "on_return":self.on_change} )
        
        self.output = OutputMenu(self, data)        
        
        self.size = LabelEntry( self, BaseLabel, {"text":"size"}, TypeEntry, {"dtype":int, "value":20, "on_return":self.on_change, "width":3, "allowNone":True} )
        
        self.output.pack( side=tk.LEFT ) 
        self.x.pack( side=tk.LEFT)
        self.y.pack( side=tk.LEFT)
        self.size.pack( side=tk.LEFT)
    def on_change(self, event):        
        self.data.setSectionBox(self.size.getVal(), (self.y.getVal(), self.x.getVal()))
        


class SectionFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):
        BaseFrame.__init__( self, parent , **kwargs)
        self.data = data
        self.sectionFuncMenu = SectionFuncMenu(self, data, command=self.func_change)
        
        self.sectionBoxFrame = SectionBoxFrame(self, data)
        self.sectionIOFrame  = SectionIOFrame(self, data)
        
        self.sectionFuncMenu.pack(side=tk.LEFT)
        #self.sectionIOFrame.pack(side=tk.LEFT)
        #self.sectionBoxFrame.pack(side=tk.LEFT)
        self.swap()
    def func_change(self,sectionfunc):
        self.data.setSectionFunc(sectionfunc)
        self.swap()
        
    def swap(self):
        case = self.sectionFuncMenu.getCase()
        
        if case==CASE_OUTPUT:
            self.sectionBoxFrame.pack(side=tk.LEFT)
            self.sectionIOFrame.pack_forget()
        elif case==CASE_IOFRAME:
            self.sectionIOFrame.pack(side=tk.LEFT)
            self.sectionBoxFrame.pack_forget()
        else:
            self.sectionBoxFrame.pack_forget()
            self.sectionIOFrame.pack_forget()
        
class BaseImageFrame(BaseFrame):
    
    def __init__(self, parent, img, pack=None, figure_label="", **kwargs):
        self.init_img(img)  
        BaseFrame.__init__(self, parent, **kwargs)
        self.plotFrame = tkplots.ImgHistFrame(self, img , figure_label=figure_label)
                
        pack = pack or self.pack_verticaly
        pack(self)

    @staticmethod
    def pack_verticaly(self):
        self.plotFrame.pack(side=tk.TOP)
    def init_img(self, img):
        self.img = img
        
    def set_img(self, img):        
        self.plotFrame.set_plot(img)
        self.init_img(img)
    
    def replot(self, img=None):      
        self.plotFrame.replot()
    
class GainFrame(BaseImageFrame):
    def __init__(self, parent, data, pack=None, kw=None, **kwargs):
        self.data = data
        
        img = self.data.getGainPlot()
                
        #img.kw_imshow.update()
        BaseImageFrame.__init__(self, parent, img, pack=pack,kw=kw, figure_label="gain", **kwargs)

        
        self.data.on_refresh_gainPlot.append( self.refresh )
        
    def refresh(self):
        self.replot()
        #self.set_img( self.data.getGainPlot() )
        
class FlatFrame(BaseImageFrame):
    def __init__(self, parent, data, pack=None, kw=None, **kwargs):
        self.data = data                
        self.kw = kw or {}
        img = self.data.getFlatPlot()
        
        
        BaseImageFrame.__init__(self, parent, img, pack=pack, kw=kw, figure_label="flat", **kwargs)
        
        self.data.on_refresh_flatPlot.append(self.refresh)
    def refresh(self):
        self.replot()
        #self.set_img( self.data.getFlatPlot() )
        
        
class PtcPlotFrame(BasePlotFrame):
    def __init__(self, parent, data, **kwargs):
        self.data = data
        plot = self.data.getPtcPlot()        
    
        plot.new_axes("ptc", figure="ptc")
        plot.kw["errorbar.axes"] = "ptc"
        plot.kw["plotfit.axes"] = "ptc"
        plot.kw["plot.axes"] = "ptc"
        
        BasePlotFrame.__init__(self, parent, plot.all, plot.get_axes("ptc").figure)
        
        self.data.on_refresh_ptcPlot.append(self.refresh)
        
    def refresh(self):
        return self.replot()
        

class PtcFrame(BaseFrame):
    def __init__(self, parent, data, **kwargs):
        BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        self.plotFrame = PtcPlotFrame(self, data)
        
        self.freduceFrame = FreduceFrame(self, data)

        self.plotFrame.pack(side=tk.TOP)
        self.freduceFrame.pack(side=tk.TOP)
    def refresh(self):
        return self.plotFrame.refresh()
    

class MaintabsTabs(ttk.Notebook):
    def __init__(self, parent ,data,  **kwargs):
        ttk.Notebook.__init__(self, parent, **kwargs)
        self.data = data
        self.flatTab    = ttk.Frame(self)
        self.gainTab    = ttk.Frame(self)
        self.ptcTab    = ttk.Frame(self)
        #self.ptcTab         = PtcTab(self)
        
        self.flatFrame = FlatFrame(self.flatTab, self.data)
        self.gainFrame = GainFrame(self.gainTab, self.data)
        self.ptcFrame =  PtcFrame(self.ptcTab, self.data)
        
        #################################################   
        self.add(self.flatTab  , text="Flat")
        self.add(self.gainTab , text="Gain")
        self.add(self.ptcTab , text="PTC")
        
        
        
        self.flatFrame.pack(side=tk.TOP)
        self.gainFrame.pack(side=tk.TOP)
        self.ptcFrame.pack(side=tk.TOP)
