#!/anaconda/bin/python
import detector as det
local = det.ImgListOpeneur(path="PIONIER*.fits", rootdir="/Users/guieu/DETDATA",
                           wrapper = det.CombinedImages
                       )
d = local.open(subdir="261-2014-PTC-P7100-1", path="PIONIER*000[01][05]*.fits")

ig = d.getGain().plots.img(figure="Gain")
ig.imshow(axes=(2,1,1), vmin=0, vmax=1)
ig.hist(axes=(2,1,1), bins=det.np.arange(0,1,50))

ifl = d.getFlux().plots.img(figure="Flux")
ifl.imshow(axes=(2,1,1), vmin=0, vmax=100)
ifl.hist(axes=(2,1,1), bins=det.np.arange(0,100,50))


