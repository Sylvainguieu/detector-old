import numpy as np
from detector import config

def score(obj, section, scorefunc, shape, dtype=np.float64, zeros=-99.99): 
    
    y_size, x_size = shape
    
    
    output = np.ndarray( (y_size, x_size), dtype=dtype)
    mask   = np.ones( (y_size, x_size), dtype=np.bool)
    output[...] = zeros
    
    
    sec0 = section((0,0))
    y, x = np.mgrid[0:shape[0],0:shape[1]]
    y, x = y[sec0], x[sec0]
    # determine the section dimention 
    y_sec_size, x_sec_size = np.max(y)+1, np.max(x)+1


    osize = config.outputsize
    if config.outputaxis == config.X:
        xboxes = zip( range(0,x_size,osize), range(osize,x_size+osize,osize))
        yboxes = [(0,y_size)]
    elif config.outputaxis == config.Y:
        yboxes = zip( range(0,y_size,osize), range(osize,y_size+osize,osize))
        xboxes = [(0,x_size)]
    else:
        raise ValueError("config.outputaxis '%s' not understood")
    
    return _score(obj, section, [(0,255)], zip( range(0,320,40), range(40,320+40,40)),  y_sec_size, x_sec_size, scorefunc, output, mask)
                  
    #return _score(obj, section, y_size, x_size, y_sec_size, x_sec_size, scorefunc, output,
    #              xboxes=range(0, 320, 40), xbox_size=40
    #)



def _score(obj, section, yboxes, xboxes, y_sec_size, x_sec_size,
           scorefunc,  output, mask):

    x_continue = True
    for x_start, x_end in xboxes:
        i = x_start
        while (i+x_sec_size)<=x_end:
            for y_start, y_end in yboxes:
                j = y_start
                while (j+y_sec_size)<=y_end:
                    sec = section((j,i))

                    output[j,i] = scorefunc(obj,sec)
                    mask[j,i] = False
                    j += 1
            print j,i
            i += 1
    return np.ma.masked_array(output, mask)
            
def _score_old(obj, section, y_size, x_size, y_sec_size, x_sec_size,
           scorefunc,  output, xboxes=[0], yboxes=[0], xbox_size=None, ybox_size=None):
    xbox_size = xbox_size or x_size
    ybox_size = ybox_size or y_size
    
    j, i =  0, 0
    jbox, ibox = 0, 0
    x_continue = True
    y_continue = True
    
    
    
    while x_continue:
        j = 0        
        y_continue = True
        while y_continue:
            sec = section((j,i))            
            if (j+y_sec_size)>=(y_size-1): y_continue = False
            
            if (i+x_sec_size)>=(x_size-1): x_continue = False
            output[j,i] = scorefunc(obj,sec)
            
            if ( (j-yboxes[jbox]+y_sec_size)>=(ybox_size)):
                jbox += 1
                j += ybox_size
            else:
                j += 1
        print j, i, jbox, ibox
        if ( (i-xboxes[ibox]+x_sec_size)>=(xbox_size-1)):
            ibox += 1
            i += xbox_size
        else:
            i += 1
    return output
