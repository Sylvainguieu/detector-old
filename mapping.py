from detector import fits, base 
import numpy as np
import datax

################################################################################
#
#   Class for Mapping 
#
################################################################################

# copy of pndrScope.i 
#mapABCD_H.pol = "Pnat";
# 
# mapABCD_H.win  = indgen(24);
# mapABCD_H.t1   = [1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3];
# mapABCD_H.t2   = [2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4];
# mapABCD_H.base = [1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,2,2,6,6,6,6];
# mapABCD_H.vis  = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
# 
# mapABCD_H.phi= [0,      1,   1.6,   0.6,
#                 0,      1,          
#                 0,      1,   1.6,   0.6,
#                 0,      1,  1.83,   0.83,
#                 0,      1,   1.6,   0.6,
#                              1.6,   0.6,
#                 0,      1,   1.6,   0.6] *(pi);
# 

def newMapping( t1, t2, base, phi, polar="Pnat", win=None):
    N = len(t1)
    if win is None:
        win = range(N)
    if not isinstance( polar, (list,np.ndarray)):
        polar = [polar]*N
    return Mappings( [ Mapping(t1[i], t2[i], base[i], phi[i], polar[i], win[i]) for i in range(N)])

class Mappings(list):    
    #def __init__(self, lst):
    # This is a list of mapping
    #   super(Mappings,self).__init__(lst)
     
    def __call__(self, tel=None, base=None, polar=None, win=None):
        if win is None:
            if tel   is not None: self =self.withTel(tel)
            if not len(self):
                raise ValueError("Nothing with tel %s"%tel)
            if base  is not None: self =self.withBase(base)
            if not len(self):
                raise ValueError("Nothing with tel %s, base %s"%(tel,base))
            if polar is not None: self =self.withPolar(polar)
            if not len(self):
                raise ValueError("Nothing with tel %s, base %s polar %s"%(tel,base, polar))
        elif win is not None:    
            if (tel is not None) or (polar is not None) or (base is not None):
                raise ValueError( "if win= keyword is not None, tel,base,polar keywords should be None")
            self = self.withWins(win)        
        return self.getIdx()
    
    def withTel(self, tel):
        return self.__class__([m for m in self if m.t1==tel or m.t2==tel])
    def withBase(self, base):
        return self.__class__([m for m in self if m.base==base])
    def withPolar(self, polar):
        return self.__class__([m for m in self if m.polar==polar])    
    def withWins(self, wins):
        return self.__class__([m for m in self if m.win in wins])

    def withWin(self, win):
        for m in self:
            if m.win == win:
                return m
        raise ValueError("the window %d does not exists")    
    def getT1(self):
        return [m.t1 for m in self]        
    def getT2(self):
        return [m.t2 for m in self]
    def getWin(self):
        return [m.win for m in self]        
    def getBase(self):
        return [m.base for m in self]    
    def getPhi(self):
        return [m.phi for m in self]
    def getPolar(self):
        return [m.polar for m in self]
   
    def getIdx(self):
        return datax.DataX(self.getWin(), axes=[base.WIN])  
    t1    = property(getT1)
    t2    = property(getT2)
    win   = property(getWin)
    base  = property(getBase)
    phi   = property(getPhi)
    polar = property(getPolar)
    idx   = property(getIdx)
    
class Mapping(object):
    def __init__(self, t1=0,t2=0,base=0,phi=0.0, polar="pNat", win=0):
        self.t1 = t1
        self.t2 = t2 
        self.phi = phi
        self.polar=polar
        self.win = win
        self.base = base
    def __repr__(self):
        return "(%d-%d  base=%d  win=%d  phi=%3.2f  polar=%s)"%(self.t1, self.t2, self.base, self.win,self.phi, self.polar)
        

    
mapABCD = newMapping( [1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3], 
                      [2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4], 
                      [1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,2,2,6,6,6,6], 
                      [t*np.pi for t in [0, 1,   1.6,   0.6,
                                         0, 1, 
                                         0, 1,   1.6,   0.6,
                                         0, 1,  1.83,   0.83,
                                         0, 1,   1.6,   0.6,
                                         1.6,   0.6 ,
                                         0,      1,   1.6,   0.6]]
                  )

mapABCD_pol =  newMapping( mapABCD.t1+mapABCD.t1,
                           mapABCD.t2+mapABCD.t2,
                           mapABCD.base+[b+6 for b in mapABCD.base],
                           mapABCD.phi+mapABCD.phi,
                           ["Pdown"]*24+["Pup"]*24
)

